Temporary header file '/tmp/eUxR7Qi4R6/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/lib/llvm-14/include/polly/Canonicalization.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/BlockGenerators.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/CodeGeneration.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/CodegenCleanup.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/IRBuilder.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/IslAst.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/IslExprBuilder.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/IslNodeBuilder.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/LoopGenerators.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/LoopGeneratorsGOMP.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/LoopGeneratorsKMP.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/PPCGCodeGeneration.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/PerfMonitor.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/RuntimeDebugBuilder.h"
  #include "/usr/lib/llvm-14/include/polly/CodeGen/Utils.h"
  #include "/usr/lib/llvm-14/include/polly/CodePreparation.h"
  #include "/usr/lib/llvm-14/include/polly/Config/config.h"
  #include "/usr/lib/llvm-14/include/polly/DeLICM.h"
  #include "/usr/lib/llvm-14/include/polly/DeadCodeElimination.h"
  #include "/usr/lib/llvm-14/include/polly/DependenceInfo.h"
  #include "/usr/lib/llvm-14/include/polly/FlattenAlgo.h"
  #include "/usr/lib/llvm-14/include/polly/FlattenSchedule.h"
  #include "/usr/lib/llvm-14/include/polly/ForwardOpTree.h"
  #include "/usr/lib/llvm-14/include/polly/JSONExporter.h"
  #include "/usr/lib/llvm-14/include/polly/LinkAllPasses.h"
  #include "/usr/lib/llvm-14/include/polly/ManualOptimizer.h"
  #include "/usr/lib/llvm-14/include/polly/MatmulOptimizer.h"
  #include "/usr/lib/llvm-14/include/polly/Options.h"
  #include "/usr/lib/llvm-14/include/polly/PolyhedralInfo.h"
  #include "/usr/lib/llvm-14/include/polly/PruneUnprofitable.h"
  #include "/usr/lib/llvm-14/include/polly/RegisterPasses.h"
  #include "/usr/lib/llvm-14/include/polly/ScheduleOptimizer.h"
  #include "/usr/lib/llvm-14/include/polly/ScheduleTreeTransform.h"
  #include "/usr/lib/llvm-14/include/polly/ScopBuilder.h"
  #include "/usr/lib/llvm-14/include/polly/ScopDetection.h"
  #include "/usr/lib/llvm-14/include/polly/ScopDetectionDiagnostic.h"
  #include "/usr/lib/llvm-14/include/polly/ScopInfo.h"
  #include "/usr/lib/llvm-14/include/polly/ScopPass.h"
  #include "/usr/lib/llvm-14/include/polly/Simplify.h"
  #include "/usr/lib/llvm-14/include/polly/Support/DumpFunctionPass.h"
  #include "/usr/lib/llvm-14/include/polly/Support/DumpModulePass.h"
  #include "/usr/lib/llvm-14/include/polly/Support/GICHelper.h"
  #include "/usr/lib/llvm-14/include/polly/Support/ISLOStream.h"
  #include "/usr/lib/llvm-14/include/polly/Support/ISLOperators.h"
  #include "/usr/lib/llvm-14/include/polly/Support/ISLTools.h"
  #include "/usr/lib/llvm-14/include/polly/Support/SCEVAffinator.h"
  #include "/usr/lib/llvm-14/include/polly/Support/SCEVValidator.h"
  #include "/usr/lib/llvm-14/include/polly/Support/ScopHelper.h"
  #include "/usr/lib/llvm-14/include/polly/Support/ScopLocation.h"
  #include "/usr/lib/llvm-14/include/polly/Support/VirtualInstruction.h"
  #include "/usr/lib/llvm-14/include/polly/ZoneAlgo.h"
  #include "/usr/lib/llvm-14/include/polly/isl/aff.h"
  #include "/usr/lib/llvm-14/include/polly/isl/aff_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/arg.h"
  #include "/usr/lib/llvm-14/include/polly/isl/ast.h"
  #include "/usr/lib/llvm-14/include/polly/isl/ast_build.h"
  #include "/usr/lib/llvm-14/include/polly/isl/ast_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/constraint.h"
  #include "/usr/lib/llvm-14/include/polly/isl/ctx.h"
  #include "/usr/lib/llvm-14/include/polly/isl/fixed_box.h"
  #include "/usr/lib/llvm-14/include/polly/isl/flow.h"
  #include "/usr/lib/llvm-14/include/polly/isl/hash.h"
  #include "/usr/lib/llvm-14/include/polly/isl/id.h"
  #include "/usr/lib/llvm-14/include/polly/isl/id_to_ast_expr.h"
  #include "/usr/lib/llvm-14/include/polly/isl/id_to_id.h"
  #include "/usr/lib/llvm-14/include/polly/isl/id_to_pw_aff.h"
  #include "/usr/lib/llvm-14/include/polly/isl/id_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/ilp.h"
  #include "/usr/lib/llvm-14/include/polly/isl/isl-noexceptions.h"
  #include "/usr/lib/llvm-14/include/polly/isl/list.h"
  #include "/usr/lib/llvm-14/include/polly/isl/local_space.h"
  #include "/usr/lib/llvm-14/include/polly/isl/lp.h"
  #include "/usr/lib/llvm-14/include/polly/isl/map.h"
  #include "/usr/lib/llvm-14/include/polly/isl/map_to_basic_set.h"
  #include "/usr/lib/llvm-14/include/polly/isl/map_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/mat.h"
  #include "/usr/lib/llvm-14/include/polly/isl/maybe.h"
  #include "/usr/lib/llvm-14/include/polly/isl/maybe_ast_expr.h"
  #include "/usr/lib/llvm-14/include/polly/isl/maybe_basic_set.h"
  #include "/usr/lib/llvm-14/include/polly/isl/maybe_id.h"
  #include "/usr/lib/llvm-14/include/polly/isl/maybe_pw_aff.h"
  #include "/usr/lib/llvm-14/include/polly/isl/multi.h"
  #include "/usr/lib/llvm-14/include/polly/isl/obj.h"
  #include "/usr/lib/llvm-14/include/polly/isl/options.h"
  #include "/usr/lib/llvm-14/include/polly/isl/point.h"
  #include "/usr/lib/llvm-14/include/polly/isl/polynomial.h"
  #include "/usr/lib/llvm-14/include/polly/isl/polynomial_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/printer.h"
  #include "/usr/lib/llvm-14/include/polly/isl/printer_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/schedule.h"
  #include "/usr/lib/llvm-14/include/polly/isl/schedule_node.h"
  #include "/usr/lib/llvm-14/include/polly/isl/schedule_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/set.h"
  #include "/usr/lib/llvm-14/include/polly/isl/set_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/space.h"
  #include "/usr/lib/llvm-14/include/polly/isl/space_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/stdint.h"
  #include "/usr/lib/llvm-14/include/polly/isl/stream.h"
  #include "/usr/lib/llvm-14/include/polly/isl/stride_info.h"
  #include "/usr/lib/llvm-14/include/polly/isl/union_map.h"
  #include "/usr/lib/llvm-14/include/polly/isl/union_map_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/union_set.h"
  #include "/usr/lib/llvm-14/include/polly/isl/union_set_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/val.h"
  #include "/usr/lib/llvm-14/include/polly/isl/val_gmp.h"
  #include "/usr/lib/llvm-14/include/polly/isl/val_type.h"
  #include "/usr/lib/llvm-14/include/polly/isl/vec.h"
  #include "/usr/lib/llvm-14/include/polly/isl/version.h"
  #include "/usr/lib/llvm-14/include/polly/isl/vertices.h"

  // add namespaces
  namespace isl{typedef int tmp_add_type_1;}
  isl::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace isl{namespace noexceptions{typedef int tmp_add_type_2;}}
  isl::noexceptions::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace llvm{typedef int tmp_add_type_3;}
  llvm::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace llvm{namespace APIntOps{typedef int tmp_add_type_4;}}
  llvm::APIntOps::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace llvm{namespace AttributeFuncs{typedef int tmp_add_type_5;}}
  llvm::AttributeFuncs::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace llvm{namespace BitmaskEnumDetail{typedef int tmp_add_type_6;}}
  llvm::BitmaskEnumDetail::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace llvm{namespace CallingConv{typedef int tmp_add_type_7;}}
  llvm::CallingConv::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace llvm{namespace cfg{typedef int tmp_add_type_8;}}
  llvm::cfg::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace llvm{namespace cl{typedef int tmp_add_type_9;}}
  llvm::cl::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace llvm{namespace cl{namespace detail{typedef int tmp_add_type_10;}}}
  llvm::cl::detail::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace llvm{namespace CodeGenOpt{typedef int tmp_add_type_11;}}
  llvm::CodeGenOpt::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace llvm{namespace CodeModel{typedef int tmp_add_type_12;}}
  llvm::CodeModel::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace llvm{namespace detail{typedef int tmp_add_type_13;}}
  llvm::detail::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace llvm{namespace DomTreeBuilder{typedef int tmp_add_type_14;}}
  llvm::DomTreeBuilder::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace llvm{namespace dwarf{typedef int tmp_add_type_15;}}
  llvm::dwarf::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace llvm{namespace fp{typedef int tmp_add_type_16;}}
  llvm::fp::tmp_add_type_16 tmp_add_func_16(){return 0;};
  namespace llvm{namespace hashing{typedef int tmp_add_type_17;}}
  llvm::hashing::tmp_add_type_17 tmp_add_func_17(){return 0;};
  namespace llvm{namespace hashing{namespace detail{typedef int tmp_add_type_18;}}}
  llvm::hashing::detail::tmp_add_type_18 tmp_add_func_18(){return 0;};
  namespace llvm{namespace InlineConstants{typedef int tmp_add_type_19;}}
  llvm::InlineConstants::tmp_add_type_19 tmp_add_func_19(){return 0;};
  namespace llvm{namespace Intrinsic{typedef int tmp_add_type_20;}}
  llvm::Intrinsic::tmp_add_type_20 tmp_add_func_20(){return 0;};
  namespace llvm{namespace legacy{typedef int tmp_add_type_21;}}
  llvm::legacy::tmp_add_type_21 tmp_add_func_21(){return 0;};
  namespace llvm{namespace mdconst{typedef int tmp_add_type_22;}}
  llvm::mdconst::tmp_add_type_22 tmp_add_func_22(){return 0;};
  namespace llvm{namespace mdconst{namespace detail{typedef int tmp_add_type_23;}}}
  llvm::mdconst::detail::tmp_add_type_23 tmp_add_func_23(){return 0;};
  namespace llvm{namespace numbers{typedef int tmp_add_type_24;}}
  llvm::numbers::tmp_add_type_24 tmp_add_func_24(){return 0;};
  namespace llvm{namespace ore{typedef int tmp_add_type_25;}}
  llvm::ore::tmp_add_type_25 tmp_add_func_25(){return 0;};
  namespace llvm{namespace pgo{typedef int tmp_add_type_26;}}
  llvm::pgo::tmp_add_type_26 tmp_add_func_26(){return 0;};
  namespace llvm{namespace PICLevel{typedef int tmp_add_type_27;}}
  llvm::PICLevel::tmp_add_type_27 tmp_add_func_27(){return 0;};
  namespace llvm{namespace PIELevel{typedef int tmp_add_type_28;}}
  llvm::PIELevel::tmp_add_type_28 tmp_add_func_28(){return 0;};
  namespace llvm{namespace Reloc{typedef int tmp_add_type_29;}}
  llvm::Reloc::tmp_add_type_29 tmp_add_func_29(){return 0;};
  namespace llvm{namespace remarks{typedef int tmp_add_type_30;}}
  llvm::remarks::tmp_add_type_30 tmp_add_func_30(){return 0;};
  namespace llvm{namespace sampleprof{typedef int tmp_add_type_31;}}
  llvm::sampleprof::tmp_add_type_31 tmp_add_func_31(){return 0;};
  namespace llvm{namespace support{typedef int tmp_add_type_32;}}
  llvm::support::tmp_add_type_32 tmp_add_func_32(){return 0;};
  namespace llvm{namespace support{namespace detail{typedef int tmp_add_type_33;}}}
  llvm::support::detail::tmp_add_type_33 tmp_add_func_33(){return 0;};
  namespace llvm{namespace support{namespace endian{typedef int tmp_add_type_34;}}}
  llvm::support::endian::tmp_add_type_34 tmp_add_func_34(){return 0;};
  namespace llvm{namespace SyncScope{typedef int tmp_add_type_35;}}
  llvm::SyncScope::tmp_add_type_35 tmp_add_func_35(){return 0;};
  namespace llvm{namespace sys{typedef int tmp_add_type_36;}}
  llvm::sys::tmp_add_type_36 tmp_add_func_36(){return 0;};
  namespace llvm{namespace sys{namespace fs{typedef int tmp_add_type_37;}}}
  llvm::sys::fs::tmp_add_type_37 tmp_add_func_37(){return 0;};
  namespace llvm{namespace TLSModel{typedef int tmp_add_type_38;}}
  llvm::TLSModel::tmp_add_type_38 tmp_add_func_38(){return 0;};
  namespace llvm{namespace vfs{typedef int tmp_add_type_39;}}
  llvm::vfs::tmp_add_type_39 tmp_add_func_39(){return 0;};
  namespace polly{typedef int tmp_add_type_40;}
  polly::tmp_add_type_40 tmp_add_func_40(){return 0;};
  namespace llvm{namespace __anondeb87dd9c511{typedef int tmp_add_type_41;}}
  llvm::__anondeb87dd9c511::tmp_add_type_41 tmp_add_func_41(){return 0;};
  namespace llvm{namespace adl_detail{typedef int tmp_add_type_42;}}
  llvm::adl_detail::tmp_add_type_42 tmp_add_func_42(){return 0;};
  namespace llvm{namespace bitfields_details{typedef int tmp_add_type_43;}}
  llvm::bitfields_details::tmp_add_type_43 tmp_add_func_43(){return 0;};
  namespace llvm{namespace hashbuilder_detail{typedef int tmp_add_type_44;}}
  llvm::hashbuilder_detail::tmp_add_type_44 tmp_add_func_44(){return 0;};
  namespace llvm{namespace ilist_detail{typedef int tmp_add_type_45;}}
  llvm::ilist_detail::tmp_add_type_45 tmp_add_func_45(){return 0;};
  namespace llvm{namespace optional_detail{typedef int tmp_add_type_46;}}
  llvm::optional_detail::tmp_add_type_46 tmp_add_func_46(){return 0;};
  namespace llvm{namespace pointer_union_detail{typedef int tmp_add_type_47;}}
  llvm::pointer_union_detail::tmp_add_type_47 tmp_add_func_47(){return 0;};
  namespace llvm{namespace trailing_objects_internal{typedef int tmp_add_type_48;}}
  llvm::trailing_objects_internal::tmp_add_type_48 tmp_add_func_48(){return 0;};

  // add classes
  _G_fpos64_t* tmp_add_class_0;
  _G_fpos_t* tmp_add_class_1;
  _IO_cookie_io_functions_t* tmp_add_class_2;
  isl_arg* tmp_add_class_3;
  isl_args* tmp_add_class_4;
  isl_hash_table* tmp_add_class_5;
  isl_hash_table_entry* tmp_add_class_6;
  isl_maybe_isl_ast_expr* tmp_add_class_7;
  isl_maybe_isl_basic_set* tmp_add_class_8;
  isl_maybe_isl_id* tmp_add_class_9;
  isl_maybe_isl_pw_aff* tmp_add_class_10;
  isl_obj* tmp_add_class_11;
  isl_obj_vtable* tmp_add_class_12;
  isl_stats* tmp_add_class_13;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/eUxR7Qi4R6/dump1.h"  -I/usr/lib/llvm-14/include/polly -I/usr/include/llvm-c-14 -I/usr/include/llvm-14 -I/usr/lib/llvm-14/include

