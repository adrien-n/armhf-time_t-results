Temporary header file '/tmp/kFMtZi44pU/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KDecoration2/kdecoration2/decoratedclient.h"
  #include "/usr/include/KDecoration2/kdecoration2/decoration.h"
  #include "/usr/include/KDecoration2/kdecoration2/decorationbutton.h"
  #include "/usr/include/KDecoration2/kdecoration2/decorationbuttongroup.h"
  #include "/usr/include/KDecoration2/kdecoration2/decorationdefines.h"
  #include "/usr/include/KDecoration2/kdecoration2/decorationsettings.h"
  #include "/usr/include/KDecoration2/kdecoration2/decorationshadow.h"
  #include "/usr/include/KDecoration2/kdecoration2/decorationthemeprovider.h"
  #include "/usr/include/KDecoration2/kdecoration2/kdecoration2_export.h"
  #include "/usr/include/KDecoration2/kdecoration2/private/decoratedclientprivate.h"
  #include "/usr/include/KDecoration2/kdecoration2/private/decorationbridge.h"
  #include "/usr/include/KDecoration2/kdecoration2/private/decorationsettingsprivate.h"
  #include "/usr/include/KDecoration2/kdecoration2/private/kdecoration2_private_export.h"
  #include "/usr/include/KF5/kdecoration2_version.h"

  // add namespaces
  namespace KDecoration2{typedef int tmp_add_type_1;}
  KDecoration2::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_2;}
  QAlgorithmsPrivate::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_3;}
  QColorConstants::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_4;}}
  QColorConstants::Svg::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace Qt{typedef int tmp_add_type_5;}
  Qt::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_6;}
  QtGlobalStatic::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_7;}
  QtMetaTypePrivate::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_8;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_9;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_10;}
  QtPrivate::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_11;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_12;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_13;}
  QtSharedPointer::tmp_add_type_13 tmp_add_func_13(){return 0;};

  // add classes
  QArrayData* tmp_add_class_0;
  QAtomicInt* tmp_add_class_1;
  QBrush* tmp_add_class_2;
  QBrushData* tmp_add_class_3;
  QByteArray* tmp_add_class_4;
  QByteArrayDataPtr* tmp_add_class_5;
  QByteRef* tmp_add_class_6;
  QChar* tmp_add_class_7;
  QCharRef* tmp_add_class_8;
  QColor* tmp_add_class_9;
  QConicalGradient* tmp_add_class_10;
  QDataStream* tmp_add_class_11;
  QFlag* tmp_add_class_12;
  QFont* tmp_add_class_13;
  QFontMetrics* tmp_add_class_14;
  QFontMetricsF* tmp_add_class_15;
  QGenericArgument* tmp_add_class_16;
  QGenericReturnArgument* tmp_add_class_17;
  QGradient* tmp_add_class_18;
  QHashData* tmp_add_class_19;
  QHashDummyValue* tmp_add_class_20;
  QIODevice* tmp_add_class_21;
  QIcon* tmp_add_class_22;
  QImage* tmp_add_class_23;
  QIncompatibleFlag* tmp_add_class_24;
  QInternal* tmp_add_class_25;
  QLatin1Char* tmp_add_class_26;
  QLatin1String* tmp_add_class_27;
  QLine* tmp_add_class_28;
  QLineF* tmp_add_class_29;
  QLinearGradient* tmp_add_class_30;
  QListData* tmp_add_class_31;
  QMargins* tmp_add_class_32;
  QMarginsF* tmp_add_class_33;
  QMatrix* tmp_add_class_34;
  QMessageLogContext* tmp_add_class_35;
  QMessageLogger* tmp_add_class_36;
  QMetaObject* tmp_add_class_37;
  QMetaType* tmp_add_class_38;
  QObject* tmp_add_class_39;
  QObjectData* tmp_add_class_40;
  QObjectUserData* tmp_add_class_41;
  QPaintDevice* tmp_add_class_42;
  QPalette* tmp_add_class_43;
  QPixelFormat* tmp_add_class_44;
  QPixmap* tmp_add_class_45;
  QPoint* tmp_add_class_46;
  QPointF* tmp_add_class_47;
  QPolygon* tmp_add_class_48;
  QPolygonF* tmp_add_class_49;
  QRadialGradient* tmp_add_class_50;
  QRect* tmp_add_class_51;
  QRectF* tmp_add_class_52;
  QRegExp* tmp_add_class_53;
  QRegion* tmp_add_class_54;
  QRgba64* tmp_add_class_55;
  QScopedPointerPodDeleter* tmp_add_class_56;
  QSharedData* tmp_add_class_57;
  QSignalBlocker* tmp_add_class_58;
  QSize* tmp_add_class_59;
  QSizeF* tmp_add_class_60;
  QString* tmp_add_class_61;
  QStringDataPtr* tmp_add_class_62;
  QStringList* tmp_add_class_63;
  QStringMatcher* tmp_add_class_64;
  QStringRef* tmp_add_class_65;
  QStringView* tmp_add_class_66;
  QSysInfo* tmp_add_class_67;
  QTransform* tmp_add_class_68;
  _G_fpos64_t* tmp_add_class_69;
  _G_fpos_t* tmp_add_class_70;
  _IO_cookie_io_functions_t* tmp_add_class_71;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/kFMtZi44pU/dump1.h"  -I/usr/include/KDecoration2 -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtGui -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

