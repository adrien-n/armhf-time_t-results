Temporary header file '/tmp/ky6Wgt995K/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/fplll/bkz.h"
  #include "/usr/include/fplll/bkz_param.h"
  #include "/usr/include/fplll/defs.h"
  #include "/usr/include/fplll/enum/enumerate.h"
  #include "/usr/include/fplll/enum/enumerate_base.h"
  #include "/usr/include/fplll/enum/enumerate_ext.h"
  #include "/usr/include/fplll/enum/enumerate_ext_api.h"
  #include "/usr/include/fplll/enum/evaluator.h"
  #include "/usr/include/fplll/fplll.h"
  #include "/usr/include/fplll/fplll_config.h"
  #include "/usr/include/fplll/gso.h"
  #include "/usr/include/fplll/gso_gram.h"
  #include "/usr/include/fplll/gso_interface.h"
  #include "/usr/include/fplll/hlll.h"
  #include "/usr/include/fplll/householder.h"
  #include "/usr/include/fplll/io/thread_pool.hpp"
  #include "/usr/include/fplll/lll.h"
  #include "/usr/include/fplll/nr/dpe.h"
  #include "/usr/include/fplll/nr/matrix.h"
  #include "/usr/include/fplll/nr/nr.h"
  #include "/usr/include/fplll/nr/numvect.h"
  #include "/usr/include/fplll/pruner/pruner.h"
  #include "/usr/include/fplll/svpcvp.h"
  #include "/usr/include/fplll/threadpool.h"
  #include "/usr/include/fplll/util.h"
  #include "/usr/include/fplll/wrapper.h"
  #include "/usr/include/fplll.h"

  // add namespaces
  namespace fplll{typedef int tmp_add_type_1;}
  fplll::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace thread_pool{typedef int tmp_add_type_2;}
  thread_pool::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace thread_pool{namespace detail{typedef int tmp_add_type_3;}}
  thread_pool::detail::tmp_add_type_3 tmp_add_func_3(){return 0;};

  // add classes
  _G_fpos64_t* tmp_add_class_0;
  _G_fpos_t* tmp_add_class_1;
  _IO_cookie_io_functions_t* tmp_add_class_2;
  itimerval* tmp_add_class_3;
  rlimit64* tmp_add_class_4;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/ky6Wgt995K/dump1.h"

