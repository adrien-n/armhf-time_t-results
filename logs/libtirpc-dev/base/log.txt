Temporary header file '/tmp/_OO7lXr58J/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/tirpc/netconfig.h"
  #include "/usr/include/tirpc/rpc/auth.h"
  #include "/usr/include/tirpc/rpc/auth_des.h"
  #include "/usr/include/tirpc/rpc/auth_gss.h"
  #include "/usr/include/tirpc/rpc/auth_unix.h"
  #include "/usr/include/tirpc/rpc/clnt.h"
  #include "/usr/include/tirpc/rpc/clnt_soc.h"
  #include "/usr/include/tirpc/rpc/clnt_stat.h"
  #include "/usr/include/tirpc/rpc/des.h"
  #include "/usr/include/tirpc/rpc/des_crypt.h"
  #include "/usr/include/tirpc/rpc/key_prot.h"
  #include "/usr/include/tirpc/rpc/nettype.h"
  #include "/usr/include/tirpc/rpc/pmap_clnt.h"
  #include "/usr/include/tirpc/rpc/pmap_prot.h"
  #include "/usr/include/tirpc/rpc/pmap_rmt.h"
  #include "/usr/include/tirpc/rpc/raw.h"
  #include "/usr/include/tirpc/rpc/rpc.h"
  #include "/usr/include/tirpc/rpc/rpc_com.h"
  #include "/usr/include/tirpc/rpc/rpc_msg.h"
  #include "/usr/include/tirpc/rpc/rpcb_clnt.h"
  #include "/usr/include/tirpc/rpc/rpcb_prot.h"
  #include "/usr/include/tirpc/rpc/rpcent.h"
  #include "/usr/include/tirpc/rpc/rpcsec_gss.h"
  #include "/usr/include/tirpc/rpc/svc.h"
  #include "/usr/include/tirpc/rpc/svc_auth.h"
  #include "/usr/include/tirpc/rpc/svc_auth_gss.h"
  #include "/usr/include/tirpc/rpc/svc_dg.h"
  #include "/usr/include/tirpc/rpc/svc_mt.h"
  #include "/usr/include/tirpc/rpc/svc_soc.h"
  #include "/usr/include/tirpc/rpc/types.h"
  #include "/usr/include/tirpc/rpcsvc/crypt.h"

  // add classes
  SVCAUTH* tmp_add_class_0;
  _G_fpos64_t* tmp_add_class_1;
  _G_fpos_t* tmp_add_class_2;
  _IO_cookie_io_functions_t* tmp_add_class_3;
  accepted_reply* tmp_add_class_4;
  authdes_cred* tmp_add_class_5;
  authdes_fullname* tmp_add_class_6;
  authdes_verf* tmp_add_class_7;
  authgss_private_data* tmp_add_class_8;
  authunix_parms* tmp_add_class_9;
  call_body* tmp_add_class_10;
  cmsghdr* tmp_add_class_11;
  cryptkeyarg* tmp_add_class_12;
  cryptkeyarg2* tmp_add_class_13;
  cryptkeyres* tmp_add_class_14;
  des_block* tmp_add_class_15;
  des_clnt_data* tmp_add_class_16;
  desargs* tmp_add_class_17;
  desparams* tmp_add_class_18;
  desresp* tmp_add_class_19;
  getcredres* tmp_add_class_20;
  group_filter* tmp_add_class_21;
  group_req* tmp_add_class_22;
  group_source_req* tmp_add_class_23;
  gss_OID_desc_struct* tmp_add_class_24;
  gss_OID_set_desc_struct* tmp_add_class_25;
  gss_buffer_desc_struct* tmp_add_class_26;
  gss_channel_bindings_struct* tmp_add_class_27;
  in6_addr* tmp_add_class_28;
  in6_pktinfo* tmp_add_class_29;
  in_addr* tmp_add_class_30;
  in_pktinfo* tmp_add_class_31;
  iovec* tmp_add_class_32;
  ip6_mtuinfo* tmp_add_class_33;
  ip_mreq* tmp_add_class_34;
  ip_mreq_source* tmp_add_class_35;
  ip_mreqn* tmp_add_class_36;
  ip_msfilter* tmp_add_class_37;
  ip_opts* tmp_add_class_38;
  ipv6_mreq* tmp_add_class_39;
  itimerval* tmp_add_class_40;
  key_netstarg* tmp_add_class_41;
  key_netstres* tmp_add_class_42;
  linger* tmp_add_class_43;
  mmsghdr* tmp_add_class_44;
  msghdr* tmp_add_class_45;
  netbuf* tmp_add_class_46;
  netconfig* tmp_add_class_47;
  netobj* tmp_add_class_48;
  opaque_auth* tmp_add_class_49;
  osockaddr* tmp_add_class_50;
  pmap* tmp_add_class_51;
  pmaplist* tmp_add_class_52;
  r_rpcb_rmtcallargs* tmp_add_class_53;
  r_rpcb_rmtcallres* tmp_add_class_54;
  rejected_reply* tmp_add_class_55;
  reply_body* tmp_add_class_56;
  rmtcallargs* tmp_add_class_57;
  rmtcallres* tmp_add_class_58;
  rp__list* tmp_add_class_59;
  rpc_err* tmp_add_class_60;
  rpc_gss_cred* tmp_add_class_61;
  rpc_gss_init_res* tmp_add_class_62;
  rpc_gss_sec* tmp_add_class_63;
  rpc_msg* tmp_add_class_64;
  rpc_timers* tmp_add_class_65;
  rpcb* tmp_add_class_66;
  rpcb_entry* tmp_add_class_67;
  rpcb_entry_list* tmp_add_class_68;
  rpcb_rmtcallargs* tmp_add_class_69;
  rpcb_rmtcallres* tmp_add_class_70;
  rpcb_stat* tmp_add_class_71;
  rpcbs_addrlist* tmp_add_class_72;
  rpcbs_rmtcalllist* tmp_add_class_73;
  sec_data* tmp_add_class_74;
  short_hand_verf* tmp_add_class_75;
  sockaddr* tmp_add_class_76;
  sockaddr_in* tmp_add_class_77;
  sockaddr_in6* tmp_add_class_78;
  sockaddr_storage* tmp_add_class_79;
  sockaddr_un* tmp_add_class_80;
  svc_dg_data* tmp_add_class_81;
  svc_req* tmp_add_class_82;
  t_bind* tmp_add_class_83;
  ucontext_t* tmp_add_class_84;
  ucred* tmp_add_class_85;
  unixcred* tmp_add_class_86;
  xdr_discrim* tmp_add_class_87;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/_OO7lXr58J/dump1.h"  -I/usr/include/tirpc

