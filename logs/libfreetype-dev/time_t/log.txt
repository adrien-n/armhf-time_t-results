Temporary header file '/tmp/Vw1mUPbNjq/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/freetype2/freetype/config/ftconfig.h"
  #include "/usr/include/freetype2/freetype/config/ftheader.h"
  #include "/usr/include/freetype2/freetype/config/ftoption.h"
  #include "/usr/include/freetype2/freetype/config/ftstdlib.h"
  #include "/usr/include/freetype2/freetype/config/integer-types.h"
  #include "/usr/include/freetype2/freetype/config/mac-support.h"
  #include "/usr/include/freetype2/freetype/config/public-macros.h"
  #include "/usr/include/freetype2/freetype/freetype.h"
  #include "/usr/include/freetype2/freetype/ftadvanc.h"
  #include "/usr/include/freetype2/freetype/ftbbox.h"
  #include "/usr/include/freetype2/freetype/ftbdf.h"
  #include "/usr/include/freetype2/freetype/ftbitmap.h"
  #include "/usr/include/freetype2/freetype/ftbzip2.h"
  #include "/usr/include/freetype2/freetype/ftcache.h"
  #include "/usr/include/freetype2/freetype/ftchapters.h"
  #include "/usr/include/freetype2/freetype/ftcid.h"
  #include "/usr/include/freetype2/freetype/ftcolor.h"
  #include "/usr/include/freetype2/freetype/ftdriver.h"
  #include "/usr/include/freetype2/freetype/fterrors.h"
  #include "/usr/include/freetype2/freetype/ftfntfmt.h"
  #include "/usr/include/freetype2/freetype/ftgasp.h"
  #include "/usr/include/freetype2/freetype/ftglyph.h"
  #include "/usr/include/freetype2/freetype/ftgxval.h"
  #include "/usr/include/freetype2/freetype/ftgzip.h"
  #include "/usr/include/freetype2/freetype/ftimage.h"
  #include "/usr/include/freetype2/freetype/ftincrem.h"
  #include "/usr/include/freetype2/freetype/ftlcdfil.h"
  #include "/usr/include/freetype2/freetype/ftlist.h"
  #include "/usr/include/freetype2/freetype/ftlogging.h"
  #include "/usr/include/freetype2/freetype/ftlzw.h"
  #include "/usr/include/freetype2/freetype/ftmm.h"
  #include "/usr/include/freetype2/freetype/ftmodapi.h"
  #include "/usr/include/freetype2/freetype/ftmoderr.h"
  #include "/usr/include/freetype2/freetype/ftotval.h"
  #include "/usr/include/freetype2/freetype/ftoutln.h"
  #include "/usr/include/freetype2/freetype/ftparams.h"
  #include "/usr/include/freetype2/freetype/ftpfr.h"
  #include "/usr/include/freetype2/freetype/ftrender.h"
  #include "/usr/include/freetype2/freetype/ftsizes.h"
  #include "/usr/include/freetype2/freetype/ftsnames.h"
  #include "/usr/include/freetype2/freetype/ftstroke.h"
  #include "/usr/include/freetype2/freetype/ftsynth.h"
  #include "/usr/include/freetype2/freetype/ftsystem.h"
  #include "/usr/include/freetype2/freetype/fttrigon.h"
  #include "/usr/include/freetype2/freetype/fttypes.h"
  #include "/usr/include/freetype2/freetype/ftwinfnt.h"
  #include "/usr/include/freetype2/freetype/otsvg.h"
  #include "/usr/include/freetype2/freetype/t1tables.h"
  #include "/usr/include/freetype2/freetype/ttnameid.h"
  #include "/usr/include/freetype2/freetype/tttables.h"
  #include "/usr/include/freetype2/freetype/tttags.h"
  #include "/usr/include/freetype2/ft2build.h"

  // add classes
  BDF_PropertyRec_* tmp_add_class_0;
  CID_FaceDictRec_* tmp_add_class_1;
  CID_FaceInfoRec_* tmp_add_class_2;
  FTC_ImageTypeRec_* tmp_add_class_3;
  FTC_SBitRec_* tmp_add_class_4;
  FTC_ScalerRec_* tmp_add_class_5;
  FT_Affine_23_* tmp_add_class_6;
  FT_BBox_* tmp_add_class_7;
  FT_BitmapGlyphRec_* tmp_add_class_8;
  FT_Bitmap_* tmp_add_class_9;
  FT_Bitmap_Size_* tmp_add_class_10;
  FT_COLR_Paint_* tmp_add_class_11;
  FT_CharMapRec_* tmp_add_class_12;
  FT_ClipBox_* tmp_add_class_13;
  FT_ColorIndex_* tmp_add_class_14;
  FT_ColorLine_* tmp_add_class_15;
  FT_ColorStopIterator_* tmp_add_class_16;
  FT_ColorStop_* tmp_add_class_17;
  FT_Color_* tmp_add_class_18;
  FT_Data_* tmp_add_class_19;
  FT_FaceRec_* tmp_add_class_20;
  FT_Generic_* tmp_add_class_21;
  FT_GlyphRec_* tmp_add_class_22;
  FT_GlyphSlotRec_* tmp_add_class_23;
  FT_Glyph_Class_* tmp_add_class_24;
  FT_Glyph_Metrics_* tmp_add_class_25;
  FT_Incremental_FuncsRec_* tmp_add_class_26;
  FT_Incremental_InterfaceRec_* tmp_add_class_27;
  FT_Incremental_MetricsRec_* tmp_add_class_28;
  FT_LayerIterator_* tmp_add_class_29;
  FT_ListNodeRec_* tmp_add_class_30;
  FT_ListRec_* tmp_add_class_31;
  FT_MM_Axis_* tmp_add_class_32;
  FT_MM_Var_* tmp_add_class_33;
  FT_Matrix_* tmp_add_class_34;
  FT_MemoryRec_* tmp_add_class_35;
  FT_Module_Class_* tmp_add_class_36;
  FT_Multi_Master_* tmp_add_class_37;
  FT_Opaque_Paint_* tmp_add_class_38;
  FT_Open_Args_* tmp_add_class_39;
  FT_OutlineGlyphRec_* tmp_add_class_40;
  FT_Outline_* tmp_add_class_41;
  FT_Outline_Funcs_* tmp_add_class_42;
  FT_PaintColrGlyph_* tmp_add_class_43;
  FT_PaintColrLayers_* tmp_add_class_44;
  FT_PaintComposite_* tmp_add_class_45;
  FT_PaintGlyph_* tmp_add_class_46;
  FT_PaintLinearGradient_* tmp_add_class_47;
  FT_PaintRadialGradient_* tmp_add_class_48;
  FT_PaintRotate_* tmp_add_class_49;
  FT_PaintScale_* tmp_add_class_50;
  FT_PaintSkew_* tmp_add_class_51;
  FT_PaintSolid_* tmp_add_class_52;
  FT_PaintSweepGradient_* tmp_add_class_53;
  FT_PaintTransform_* tmp_add_class_54;
  FT_PaintTranslate_* tmp_add_class_55;
  FT_Palette_Data_* tmp_add_class_56;
  FT_Parameter_* tmp_add_class_57;
  FT_Prop_GlyphToScriptMap_* tmp_add_class_58;
  FT_Prop_IncreaseXHeight_* tmp_add_class_59;
  FT_Raster_Funcs_* tmp_add_class_60;
  FT_Raster_Params_* tmp_add_class_61;
  FT_Renderer_Class_* tmp_add_class_62;
  FT_SVG_DocumentRec_* tmp_add_class_63;
  FT_SfntLangTag_* tmp_add_class_64;
  FT_SfntName_* tmp_add_class_65;
  FT_SizeRec_* tmp_add_class_66;
  FT_Size_Metrics_* tmp_add_class_67;
  FT_Size_RequestRec_* tmp_add_class_68;
  FT_Span_* tmp_add_class_69;
  FT_StreamDesc_* tmp_add_class_70;
  FT_StreamRec_* tmp_add_class_71;
  FT_SvgGlyphRec_* tmp_add_class_72;
  FT_UnitVector_* tmp_add_class_73;
  FT_Var_Axis_* tmp_add_class_74;
  FT_Var_Named_Style_* tmp_add_class_75;
  FT_Vector_* tmp_add_class_76;
  FT_WinFNT_HeaderRec_* tmp_add_class_77;
  PS_BlendRec_* tmp_add_class_78;
  PS_DesignMap_* tmp_add_class_79;
  PS_FontInfoRec_* tmp_add_class_80;
  PS_PrivateRec_* tmp_add_class_81;
  SVG_RendererHooks_* tmp_add_class_82;
  TT_Header_* tmp_add_class_83;
  TT_HoriHeader_* tmp_add_class_84;
  TT_MaxProfile_* tmp_add_class_85;
  TT_OS2_* tmp_add_class_86;
  TT_PCLT_* tmp_add_class_87;
  TT_Postscript_* tmp_add_class_88;
  TT_VertHeader_* tmp_add_class_89;
  _G_fpos64_t* tmp_add_class_90;
  _G_fpos_t* tmp_add_class_91;
  _IO_cookie_io_functions_t* tmp_add_class_92;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/Vw1mUPbNjq/dump1.h"  -I/usr/include/freetype2

