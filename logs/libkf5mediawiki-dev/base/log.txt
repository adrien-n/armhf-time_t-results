Temporary header file '/tmp/PIfq3TskIw/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KF5/MediaWiki/edit.h"
  #include "/usr/include/KF5/MediaWiki/generalinfo.h"
  #include "/usr/include/KF5/MediaWiki/image.h"
  #include "/usr/include/KF5/MediaWiki/imageinfo.h"
  #include "/usr/include/KF5/MediaWiki/job.h"
  #include "/usr/include/KF5/MediaWiki/login.h"
  #include "/usr/include/KF5/MediaWiki/logout.h"
  #include "/usr/include/KF5/MediaWiki/mediawiki.h"
  #include "/usr/include/KF5/MediaWiki/mediawiki_export.h"
  #include "/usr/include/KF5/MediaWiki/page.h"
  #include "/usr/include/KF5/MediaWiki/parse.h"
  #include "/usr/include/KF5/MediaWiki/protection.h"
  #include "/usr/include/KF5/MediaWiki/queryimageinfo.h"
  #include "/usr/include/KF5/MediaWiki/queryimages.h"
  #include "/usr/include/KF5/MediaWiki/queryinfo.h"
  #include "/usr/include/KF5/MediaWiki/queryrevision.h"
  #include "/usr/include/KF5/MediaWiki/querysiteinfogeneral.h"
  #include "/usr/include/KF5/MediaWiki/querysiteinfousergroups.h"
  #include "/usr/include/KF5/MediaWiki/revision.h"
  #include "/usr/include/KF5/MediaWiki/upload.h"
  #include "/usr/include/KF5/MediaWiki/usergroup.h"
  #include "/usr/include/KF5/mediawiki_version.h"

  // add namespaces
  namespace dtlsopenssl{typedef int tmp_add_type_1;}
  dtlsopenssl::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace mediawiki{typedef int tmp_add_type_2;}
  mediawiki::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_3;}
  QAlgorithmsPrivate::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QSsl{typedef int tmp_add_type_4;}
  QSsl::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace Qt{typedef int tmp_add_type_5;}
  Qt::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QTextStreamFunctions{typedef int tmp_add_type_6;}
  QTextStreamFunctions::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_7;}
  QtGlobalStatic::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_8;}
  QtMetaTypePrivate::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_9;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_10;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_11;}
  QtPrivate::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_12;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_13;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_14;}
  QtSharedPointer::tmp_add_type_14 tmp_add_func_14(){return 0;};

  // add classes
  KJob* tmp_add_class_0;
  QAbstractSocket* tmp_add_class_1;
  QArrayData* tmp_add_class_2;
  QAssociativeIterable* tmp_add_class_3;
  QAtomicInt* tmp_add_class_4;
  QByteArray* tmp_add_class_5;
  QByteArrayDataPtr* tmp_add_class_6;
  QByteRef* tmp_add_class_7;
  QChar* tmp_add_class_8;
  QCharRef* tmp_add_class_9;
  QContiguousCacheData* tmp_add_class_10;
  QCryptographicHash* tmp_add_class_11;
  QDate* tmp_add_class_12;
  QDateTime* tmp_add_class_13;
  QDebug* tmp_add_class_14;
  QDebugStateSaver* tmp_add_class_15;
  QFlag* tmp_add_class_16;
  QGenericArgument* tmp_add_class_17;
  QGenericReturnArgument* tmp_add_class_18;
  QHashData* tmp_add_class_19;
  QHashDummyValue* tmp_add_class_20;
  QIODevice* tmp_add_class_21;
  QIncompatibleFlag* tmp_add_class_22;
  QInternal* tmp_add_class_23;
  QLatin1Char* tmp_add_class_24;
  QLatin1String* tmp_add_class_25;
  QListData* tmp_add_class_26;
  QLocale* tmp_add_class_27;
  QMapDataBase* tmp_add_class_28;
  QMapNodeBase* tmp_add_class_29;
  QMessageLogContext* tmp_add_class_30;
  QMessageLogger* tmp_add_class_31;
  QMetaObject* tmp_add_class_32;
  QMetaType* tmp_add_class_33;
  QNetworkAccessManager* tmp_add_class_34;
  QNetworkCookieJar* tmp_add_class_35;
  QNetworkRequest* tmp_add_class_36;
  QNoDebug* tmp_add_class_37;
  QObject* tmp_add_class_38;
  QObjectData* tmp_add_class_39;
  QObjectUserData* tmp_add_class_40;
  QRegExp* tmp_add_class_41;
  QScopedPointerPodDeleter* tmp_add_class_42;
  QSequentialIterable* tmp_add_class_43;
  QSharedData* tmp_add_class_44;
  QSignalBlocker* tmp_add_class_45;
  QSslCertificate* tmp_add_class_46;
  QSslConfiguration* tmp_add_class_47;
  QSslError* tmp_add_class_48;
  QSslPreSharedKeyAuthenticator* tmp_add_class_49;
  QSslSocket* tmp_add_class_50;
  QString* tmp_add_class_51;
  QStringDataPtr* tmp_add_class_52;
  QStringList* tmp_add_class_53;
  QStringMatcher* tmp_add_class_54;
  QStringRef* tmp_add_class_55;
  QStringView* tmp_add_class_56;
  QSysInfo* tmp_add_class_57;
  QTcpSocket* tmp_add_class_58;
  QTextStream* tmp_add_class_59;
  QTextStreamManipulator* tmp_add_class_60;
  QTime* tmp_add_class_61;
  QUrl* tmp_add_class_62;
  QVariant* tmp_add_class_63;
  QVariantComparisonHelper* tmp_add_class_64;
  _G_fpos64_t* tmp_add_class_65;
  _G_fpos_t* tmp_add_class_66;
  _IO_cookie_io_functions_t* tmp_add_class_67;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/PIfq3TskIw/dump1.h"  -I/usr/include/KF5/KCoreAddons -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtNetwork -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

