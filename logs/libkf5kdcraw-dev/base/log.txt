Temporary header file '/tmp/R2ihnSGnwN/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KF5/KDCRAW/kdcraw/dcrawinfocontainer.h"
  #include "/usr/include/KF5/KDCRAW/kdcraw/kdcraw.h"
  #include "/usr/include/KF5/KDCRAW/kdcraw/libkdcraw_export.h"
  #include "/usr/include/KF5/KDCRAW/kdcraw/rawdecodingsettings.h"
  #include "/usr/include/KF5/KDCRAW/kdcraw/rawfiles.h"
  #include "/usr/include/KF5/libkdcraw_version.h"

  // add namespaces
  namespace KDcrawIface{typedef int tmp_add_type_1;}
  KDcrawIface::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_2;}
  QAlgorithmsPrivate::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_3;}
  QColorConstants::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_4;}}
  QColorConstants::Svg::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace Qt{typedef int tmp_add_type_5;}
  Qt::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QTextStreamFunctions{typedef int tmp_add_type_6;}
  QTextStreamFunctions::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_7;}
  QtGlobalStatic::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_8;}
  QtMetaTypePrivate::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_9;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_10;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_11;}
  QtPrivate::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_12;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_13;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_14;}
  QtSharedPointer::tmp_add_type_14 tmp_add_func_14(){return 0;};

  // add classes
  QArrayData* tmp_add_class_0;
  QAssociativeIterable* tmp_add_class_1;
  QAtomicInt* tmp_add_class_2;
  QBuffer* tmp_add_class_3;
  QByteArray* tmp_add_class_4;
  QByteArrayDataPtr* tmp_add_class_5;
  QByteRef* tmp_add_class_6;
  QChar* tmp_add_class_7;
  QCharRef* tmp_add_class_8;
  QColor* tmp_add_class_9;
  QContiguousCacheData* tmp_add_class_10;
  QDataStream* tmp_add_class_11;
  QDate* tmp_add_class_12;
  QDateTime* tmp_add_class_13;
  QDebug* tmp_add_class_14;
  QDebugStateSaver* tmp_add_class_15;
  QFlag* tmp_add_class_16;
  QGenericArgument* tmp_add_class_17;
  QGenericReturnArgument* tmp_add_class_18;
  QHashData* tmp_add_class_19;
  QHashDummyValue* tmp_add_class_20;
  QIODevice* tmp_add_class_21;
  QImage* tmp_add_class_22;
  QIncompatibleFlag* tmp_add_class_23;
  QInternal* tmp_add_class_24;
  QLatin1Char* tmp_add_class_25;
  QLatin1String* tmp_add_class_26;
  QLine* tmp_add_class_27;
  QLineF* tmp_add_class_28;
  QListData* tmp_add_class_29;
  QLocale* tmp_add_class_30;
  QMapDataBase* tmp_add_class_31;
  QMapNodeBase* tmp_add_class_32;
  QMargins* tmp_add_class_33;
  QMarginsF* tmp_add_class_34;
  QMatrix* tmp_add_class_35;
  QMessageLogContext* tmp_add_class_36;
  QMessageLogger* tmp_add_class_37;
  QMetaObject* tmp_add_class_38;
  QMetaType* tmp_add_class_39;
  QNoDebug* tmp_add_class_40;
  QObject* tmp_add_class_41;
  QObjectData* tmp_add_class_42;
  QObjectUserData* tmp_add_class_43;
  QPaintDevice* tmp_add_class_44;
  QPixelFormat* tmp_add_class_45;
  QPoint* tmp_add_class_46;
  QPointF* tmp_add_class_47;
  QPolygon* tmp_add_class_48;
  QPolygonF* tmp_add_class_49;
  QRect* tmp_add_class_50;
  QRectF* tmp_add_class_51;
  QRegExp* tmp_add_class_52;
  QRegion* tmp_add_class_53;
  QRgba64* tmp_add_class_54;
  QScopedPointerPodDeleter* tmp_add_class_55;
  QSequentialIterable* tmp_add_class_56;
  QSharedData* tmp_add_class_57;
  QSignalBlocker* tmp_add_class_58;
  QSize* tmp_add_class_59;
  QSizeF* tmp_add_class_60;
  QString* tmp_add_class_61;
  QStringDataPtr* tmp_add_class_62;
  QStringList* tmp_add_class_63;
  QStringMatcher* tmp_add_class_64;
  QStringRef* tmp_add_class_65;
  QStringView* tmp_add_class_66;
  QSysInfo* tmp_add_class_67;
  QTextStream* tmp_add_class_68;
  QTextStreamManipulator* tmp_add_class_69;
  QTime* tmp_add_class_70;
  QTransform* tmp_add_class_71;
  QVariant* tmp_add_class_72;
  QVariantComparisonHelper* tmp_add_class_73;
  _G_fpos64_t* tmp_add_class_74;
  _G_fpos_t* tmp_add_class_75;
  _IO_cookie_io_functions_t* tmp_add_class_76;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/R2ihnSGnwN/dump1.h"  -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtGui -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

