Temporary header file '/tmp/FUvJemnftW/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/qt5-ukui/animation-helper.h"
  #include "/usr/include/qt5-ukui/animator-iface.h"
  #include "/usr/include/qt5-ukui/animator-plugin-iface.h"
  #include "/usr/include/qt5-ukui/application-style-settings.h"
  #include "/usr/include/qt5-ukui/black-list.h"
  #include "/usr/include/qt5-ukui/highlight-effect.h"
  #include "/usr/include/qt5-ukui/internal-style.h"
  #include "/usr/include/qt5-ukui/libqt5-ukui-style_global.h"
  #include "/usr/include/qt5-ukui/moc_predefs.h"
  #include "/usr/include/qt5-ukui/mps-style.h"
  #include "/usr/include/qt5-ukui/ukui-scrollbar-default-interaction-animator.h"
  #include "/usr/include/qt5-ukui/ukui-style-settings.h"
  #include "/usr/include/qt5-ukui/ukui-tabwidget-animator-iface.h"
  #include "/usr/include/qt5-ukui/ukui-tabwidget-animator-plugin-iface.h"
  #include "/usr/include/qt5-ukui/ukui-tabwidget-default-slide-animator-factory.h"
  #include "/usr/include/qt5-ukui/ukui-tabwidget-default-slide-animator.h"
  #include "/usr/include/qt5-ukui/ukui-two-finger-slide-gesture.h"
  #include "/usr/include/qt5-ukui/ukui-two-finger-zoom-gesture.h"

  // add namespaces
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_1;}
  QAlgorithmsPrivate::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_2;}
  QColorConstants::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_3;}}
  QColorConstants::Svg::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace Qt{typedef int tmp_add_type_4;}
  Qt::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_5;}
  QtGlobalStatic::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_6;}
  QtMetaTypePrivate::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_7;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_8;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_9;}
  QtPrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_10;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_11;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_12;}
  QtSharedPointer::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace UKUI{typedef int tmp_add_type_13;}
  UKUI::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace UKUI{namespace ScrollBar{typedef int tmp_add_type_14;}}
  UKUI::ScrollBar::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace UKUI{namespace TabWidget{typedef int tmp_add_type_15;}}
  UKUI::TabWidget::tmp_add_type_15 tmp_add_func_15(){return 0;};

  // add classes
  AnimationHelper* tmp_add_class_0;
  AnimatorIface* tmp_add_class_1;
  ApplicationStyleSettings* tmp_add_class_2;
  HighLightEffect* tmp_add_class_3;
  InternalStyle* tmp_add_class_4;
  MPSStyle* tmp_add_class_5;
  QAbstractAnimation* tmp_add_class_6;
  QAbstractItemModel* tmp_add_class_7;
  QAbstractListModel* tmp_add_class_8;
  QAbstractSlider* tmp_add_class_9;
  QAbstractSpinBox* tmp_add_class_10;
  QAbstractTableModel* tmp_add_class_11;
  QActionEvent* tmp_add_class_12;
  QAnimationDriver* tmp_add_class_13;
  QAnimationGroup* tmp_add_class_14;
  QApplicationStateChangeEvent* tmp_add_class_15;
  QArrayData* tmp_add_class_16;
  QAssociativeIterable* tmp_add_class_17;
  QAtomicInt* tmp_add_class_18;
  QBrush* tmp_add_class_19;
  QBrushData* tmp_add_class_20;
  QByteArray* tmp_add_class_21;
  QByteArrayDataPtr* tmp_add_class_22;
  QByteRef* tmp_add_class_23;
  QChar* tmp_add_class_24;
  QCharRef* tmp_add_class_25;
  QChildEvent* tmp_add_class_26;
  QCloseEvent* tmp_add_class_27;
  QColor* tmp_add_class_28;
  QCommonStyle* tmp_add_class_29;
  QConicalGradient* tmp_add_class_30;
  QContextMenuEvent* tmp_add_class_31;
  QCursor* tmp_add_class_32;
  QDataStream* tmp_add_class_33;
  QDate* tmp_add_class_34;
  QDateTime* tmp_add_class_35;
  QDeferredDeleteEvent* tmp_add_class_36;
  QDoubleValidator* tmp_add_class_37;
  QDragEnterEvent* tmp_add_class_38;
  QDragLeaveEvent* tmp_add_class_39;
  QDragMoveEvent* tmp_add_class_40;
  QDropEvent* tmp_add_class_41;
  QDynamicPropertyChangeEvent* tmp_add_class_42;
  QEasingCurve* tmp_add_class_43;
  QEnterEvent* tmp_add_class_44;
  QEvent* tmp_add_class_45;
  QExposeEvent* tmp_add_class_46;
  QFile* tmp_add_class_47;
  QFileDevice* tmp_add_class_48;
  QFileOpenEvent* tmp_add_class_49;
  QFlag* tmp_add_class_50;
  QFocusEvent* tmp_add_class_51;
  QFont* tmp_add_class_52;
  QFontInfo* tmp_add_class_53;
  QFontMetrics* tmp_add_class_54;
  QFontMetricsF* tmp_add_class_55;
  QFrame* tmp_add_class_56;
  QGSettings* tmp_add_class_57;
  QGenericArgument* tmp_add_class_58;
  QGenericReturnArgument* tmp_add_class_59;
  QGesture* tmp_add_class_60;
  QGestureEvent* tmp_add_class_61;
  QGestureRecognizer* tmp_add_class_62;
  QGradient* tmp_add_class_63;
  QHashData* tmp_add_class_64;
  QHashDummyValue* tmp_add_class_65;
  QHelpEvent* tmp_add_class_66;
  QHideEvent* tmp_add_class_67;
  QHoverEvent* tmp_add_class_68;
  QIODevice* tmp_add_class_69;
  QIcon* tmp_add_class_70;
  QIconDragEvent* tmp_add_class_71;
  QImage* tmp_add_class_72;
  QIncompatibleFlag* tmp_add_class_73;
  QInputEvent* tmp_add_class_74;
  QInputMethodEvent* tmp_add_class_75;
  QInputMethodQueryEvent* tmp_add_class_76;
  QIntValidator* tmp_add_class_77;
  QInternal* tmp_add_class_78;
  QKeyEvent* tmp_add_class_79;
  QKeySequence* tmp_add_class_80;
  QLatin1Char* tmp_add_class_81;
  QLatin1String* tmp_add_class_82;
  QLine* tmp_add_class_83;
  QLineF* tmp_add_class_84;
  QLinearGradient* tmp_add_class_85;
  QListData* tmp_add_class_86;
  QLocale* tmp_add_class_87;
  QMapDataBase* tmp_add_class_88;
  QMapNodeBase* tmp_add_class_89;
  QMargins* tmp_add_class_90;
  QMarginsF* tmp_add_class_91;
  QMatrix* tmp_add_class_92;
  QMessageLogContext* tmp_add_class_93;
  QMessageLogger* tmp_add_class_94;
  QMetaClassInfo* tmp_add_class_95;
  QMetaEnum* tmp_add_class_96;
  QMetaMethod* tmp_add_class_97;
  QMetaObject* tmp_add_class_98;
  QMetaProperty* tmp_add_class_99;
  QMetaType* tmp_add_class_100;
  QModelIndex* tmp_add_class_101;
  QMouseEvent* tmp_add_class_102;
  QMoveEvent* tmp_add_class_103;
  QNativeGestureEvent* tmp_add_class_104;
  QObject* tmp_add_class_105;
  QObjectData* tmp_add_class_106;
  QObjectUserData* tmp_add_class_107;
  QPaintDevice* tmp_add_class_108;
  QPaintEvent* tmp_add_class_109;
  QPalette* tmp_add_class_110;
  QPanGesture* tmp_add_class_111;
  QParallelAnimationGroup* tmp_add_class_112;
  QPersistentModelIndex* tmp_add_class_113;
  QPinchGesture* tmp_add_class_114;
  QPixelFormat* tmp_add_class_115;
  QPixmap* tmp_add_class_116;
  QPlatformSurfaceEvent* tmp_add_class_117;
  QPoint* tmp_add_class_118;
  QPointF* tmp_add_class_119;
  QPointingDeviceUniqueId* tmp_add_class_120;
  QPolygon* tmp_add_class_121;
  QPolygonF* tmp_add_class_122;
  QProxyStyle* tmp_add_class_123;
  QRadialGradient* tmp_add_class_124;
  QRect* tmp_add_class_125;
  QRectF* tmp_add_class_126;
  QRegExp* tmp_add_class_127;
  QRegExpValidator* tmp_add_class_128;
  QRegion* tmp_add_class_129;
  QRegularExpression* tmp_add_class_130;
  QRegularExpressionMatch* tmp_add_class_131;
  QRegularExpressionMatchIterator* tmp_add_class_132;
  QRegularExpressionValidator* tmp_add_class_133;
  QResizeEvent* tmp_add_class_134;
  QRgba64* tmp_add_class_135;
  QRubberBand* tmp_add_class_136;
  QScopedPointerPodDeleter* tmp_add_class_137;
  QScreenOrientationChangeEvent* tmp_add_class_138;
  QScrollEvent* tmp_add_class_139;
  QScrollPrepareEvent* tmp_add_class_140;
  QSequentialIterable* tmp_add_class_141;
  QSettings* tmp_add_class_142;
  QSharedData* tmp_add_class_143;
  QShortcutEvent* tmp_add_class_144;
  QShowEvent* tmp_add_class_145;
  QSignalBlocker* tmp_add_class_146;
  QSize* tmp_add_class_147;
  QSizeF* tmp_add_class_148;
  QSizePolicy* tmp_add_class_149;
  QSlider* tmp_add_class_150;
  QStackedWidget* tmp_add_class_151;
  QStatusTipEvent* tmp_add_class_152;
  QString* tmp_add_class_153;
  QStringDataPtr* tmp_add_class_154;
  QStringList* tmp_add_class_155;
  QStringMatcher* tmp_add_class_156;
  QStringRef* tmp_add_class_157;
  QStringView* tmp_add_class_158;
  QStyle* tmp_add_class_159;
  QStyleHintReturn* tmp_add_class_160;
  QStyleHintReturnMask* tmp_add_class_161;
  QStyleHintReturnVariant* tmp_add_class_162;
  QStyleOption* tmp_add_class_163;
  QStyleOptionButton* tmp_add_class_164;
  QStyleOptionComboBox* tmp_add_class_165;
  QStyleOptionComplex* tmp_add_class_166;
  QStyleOptionDockWidget* tmp_add_class_167;
  QStyleOptionFocusRect* tmp_add_class_168;
  QStyleOptionFrame* tmp_add_class_169;
  QStyleOptionGraphicsItem* tmp_add_class_170;
  QStyleOptionGroupBox* tmp_add_class_171;
  QStyleOptionHeader* tmp_add_class_172;
  QStyleOptionMenuItem* tmp_add_class_173;
  QStyleOptionProgressBar* tmp_add_class_174;
  QStyleOptionRubberBand* tmp_add_class_175;
  QStyleOptionSizeGrip* tmp_add_class_176;
  QStyleOptionSlider* tmp_add_class_177;
  QStyleOptionSpinBox* tmp_add_class_178;
  QStyleOptionTab* tmp_add_class_179;
  QStyleOptionTabBarBase* tmp_add_class_180;
  QStyleOptionTabV4* tmp_add_class_181;
  QStyleOptionTabWidgetFrame* tmp_add_class_182;
  QStyleOptionTitleBar* tmp_add_class_183;
  QStyleOptionToolBar* tmp_add_class_184;
  QStyleOptionToolBox* tmp_add_class_185;
  QStyleOptionToolButton* tmp_add_class_186;
  QStyleOptionViewItem* tmp_add_class_187;
  QSwipeGesture* tmp_add_class_188;
  QSysInfo* tmp_add_class_189;
  QTabBar* tmp_add_class_190;
  QTabWidget* tmp_add_class_191;
  QTabletEvent* tmp_add_class_192;
  QTapAndHoldGesture* tmp_add_class_193;
  QTapGesture* tmp_add_class_194;
  QTime* tmp_add_class_195;
  QTimerEvent* tmp_add_class_196;
  QToolBarChangeEvent* tmp_add_class_197;
  QTouchDevice* tmp_add_class_198;
  QTouchEvent* tmp_add_class_199;
  QTransform* tmp_add_class_200;
  QUrl* tmp_add_class_201;
  QValidator* tmp_add_class_202;
  QVariant* tmp_add_class_203;
  QVariantAnimation* tmp_add_class_204;
  QVariantComparisonHelper* tmp_add_class_205;
  QVector2D* tmp_add_class_206;
  QWhatsThisClickedEvent* tmp_add_class_207;
  QWheelEvent* tmp_add_class_208;
  QWidget* tmp_add_class_209;
  QWidgetData* tmp_add_class_210;
  QWindowStateChangeEvent* tmp_add_class_211;
  UKUIAnimatorPluginIface* tmp_add_class_212;
  UKUIStyleSettings* tmp_add_class_213;
  UKUITabWidgetAnimatorIface* tmp_add_class_214;
  UKUITabWidgetAnimatorPluginIface* tmp_add_class_215;
  _G_fpos64_t* tmp_add_class_216;
  _G_fpos_t* tmp_add_class_217;
  _IO_cookie_io_functions_t* tmp_add_class_218;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/FUvJemnftW/dump1.h"  -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtWidgets -I/usr/include/arm-linux-gnueabihf/qt5/QtGui -I/usr/include/arm-linux-gnueabihf/qt5/QtCore -I/usr/include/arm-linux-gnueabihf/qt5/QGSettings

