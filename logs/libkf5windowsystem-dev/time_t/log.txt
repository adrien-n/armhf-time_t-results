Temporary header file '/tmp/1f2O7aSWmD/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KF5/KWindowSystem/config-kwindowsystem.h"
  #include "/usr/include/KF5/KWindowSystem/fixx11h.h"
  #include "/usr/include/KF5/KWindowSystem/kkeyserver.h"
  #include "/usr/include/KF5/KWindowSystem/kkeyserver_x11.h"
  #include "/usr/include/KF5/KWindowSystem/kmanagerselection.h"
  #include "/usr/include/KF5/KWindowSystem/kselectionowner.h"
  #include "/usr/include/KF5/KWindowSystem/kselectionwatcher.h"
  #include "/usr/include/KF5/KWindowSystem/kstartupinfo.h"
  #include "/usr/include/KF5/KWindowSystem/kusertimestamp.h"
  #include "/usr/include/KF5/KWindowSystem/kwindoweffects.h"
  #include "/usr/include/KF5/KWindowSystem/kwindowinfo.h"
  #include "/usr/include/KF5/KWindowSystem/kwindowshadow.h"
  #include "/usr/include/KF5/KWindowSystem/kwindowsystem.h"
  #include "/usr/include/KF5/KWindowSystem/kwindowsystem_export.h"
  #include "/usr/include/KF5/KWindowSystem/kwindowsystem_version.h"
  #include "/usr/include/KF5/KWindowSystem/kx11extras.h"
  #include "/usr/include/KF5/KWindowSystem/kxmessages.h"
  #include "/usr/include/KF5/KWindowSystem/netwm.h"
  #include "/usr/include/KF5/KWindowSystem/netwm_def.h"
  #include "/usr/include/KF5/KWindowSystem/private/kwindoweffects_p.h"
  #include "/usr/include/KF5/KWindowSystem/private/kwindowinfo_p.h"
  #include "/usr/include/KF5/KWindowSystem/private/kwindowshadow_p.h"
  #include "/usr/include/KF5/KWindowSystem/private/kwindowsystem_p.h"
  #include "/usr/include/KF5/KWindowSystem/private/kwindowsystemplugininterface_p.h"

  // add namespaces
  namespace KKeyServer{typedef int tmp_add_type_1;}
  KKeyServer::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace KUserTimestamp{typedef int tmp_add_type_2;}
  KUserTimestamp::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace KWindowEffects{typedef int tmp_add_type_3;}
  KWindowEffects::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_4;}
  QAlgorithmsPrivate::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_5;}
  QColorConstants::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_6;}}
  QColorConstants::Svg::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace Qt{typedef int tmp_add_type_7;}
  Qt::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_8;}
  QtGlobalStatic::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_9;}
  QtMetaTypePrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_10;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_11;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_12;}
  QtPrivate::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_13;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_14;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_15;}
  QtSharedPointer::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace X{typedef int tmp_add_type_16;}
  X::tmp_add_type_16 tmp_add_func_16(){return 0;};

  // add classes
  KSelectionOwner* tmp_add_class_0;
  KSelectionWatcher* tmp_add_class_1;
  KStartupInfo* tmp_add_class_2;
  KStartupInfoData* tmp_add_class_3;
  KStartupInfoId* tmp_add_class_4;
  KWindowEffectsPrivate* tmp_add_class_5;
  KWindowEffectsPrivateV2* tmp_add_class_6;
  KWindowInfo* tmp_add_class_7;
  KWindowInfoPrivate* tmp_add_class_8;
  KWindowInfoPrivateAppMenuExtension* tmp_add_class_9;
  KWindowInfoPrivateDesktopFileNameExtension* tmp_add_class_10;
  KWindowInfoPrivateGtkApplicationIdExtension* tmp_add_class_11;
  KWindowInfoPrivatePidExtension* tmp_add_class_12;
  KWindowShadow* tmp_add_class_13;
  KWindowShadowPrivate* tmp_add_class_14;
  KWindowShadowTile* tmp_add_class_15;
  KWindowShadowTilePrivate* tmp_add_class_16;
  KWindowSystem* tmp_add_class_17;
  KWindowSystemPluginInterface* tmp_add_class_18;
  KWindowSystemPrivate* tmp_add_class_19;
  KWindowSystemPrivateV2* tmp_add_class_20;
  KX11Extras* tmp_add_class_21;
  KXMessages* tmp_add_class_22;
  NET* tmp_add_class_23;
  NETExtendedStrut* tmp_add_class_24;
  NETFullscreenMonitors* tmp_add_class_25;
  NETIcon* tmp_add_class_26;
  NETPoint* tmp_add_class_27;
  NETRect* tmp_add_class_28;
  NETRootInfo* tmp_add_class_29;
  NETSize* tmp_add_class_30;
  NETStrut* tmp_add_class_31;
  NETWinInfo* tmp_add_class_32;
  QArrayData* tmp_add_class_33;
  QAtomicInt* tmp_add_class_34;
  QByteArray* tmp_add_class_35;
  QByteArrayDataPtr* tmp_add_class_36;
  QByteRef* tmp_add_class_37;
  QChar* tmp_add_class_38;
  QCharRef* tmp_add_class_39;
  QChildEvent* tmp_add_class_40;
  QColor* tmp_add_class_41;
  QCursor* tmp_add_class_42;
  QDataStream* tmp_add_class_43;
  QDeferredDeleteEvent* tmp_add_class_44;
  QDynamicPropertyChangeEvent* tmp_add_class_45;
  QEvent* tmp_add_class_46;
  QFlag* tmp_add_class_47;
  QGenericArgument* tmp_add_class_48;
  QGenericReturnArgument* tmp_add_class_49;
  QHashData* tmp_add_class_50;
  QHashDummyValue* tmp_add_class_51;
  QIODevice* tmp_add_class_52;
  QIcon* tmp_add_class_53;
  QImage* tmp_add_class_54;
  QIncompatibleFlag* tmp_add_class_55;
  QInternal* tmp_add_class_56;
  QLatin1Char* tmp_add_class_57;
  QLatin1String* tmp_add_class_58;
  QLine* tmp_add_class_59;
  QLineF* tmp_add_class_60;
  QListData* tmp_add_class_61;
  QMargins* tmp_add_class_62;
  QMarginsF* tmp_add_class_63;
  QMatrix* tmp_add_class_64;
  QMessageLogContext* tmp_add_class_65;
  QMessageLogger* tmp_add_class_66;
  QMetaObject* tmp_add_class_67;
  QMetaType* tmp_add_class_68;
  QObject* tmp_add_class_69;
  QObjectData* tmp_add_class_70;
  QObjectUserData* tmp_add_class_71;
  QPaintDevice* tmp_add_class_72;
  QPixelFormat* tmp_add_class_73;
  QPixmap* tmp_add_class_74;
  QPoint* tmp_add_class_75;
  QPointF* tmp_add_class_76;
  QPolygon* tmp_add_class_77;
  QPolygonF* tmp_add_class_78;
  QRect* tmp_add_class_79;
  QRectF* tmp_add_class_80;
  QRegExp* tmp_add_class_81;
  QRegion* tmp_add_class_82;
  QRgba64* tmp_add_class_83;
  QScopedPointerPodDeleter* tmp_add_class_84;
  QSharedData* tmp_add_class_85;
  QSignalBlocker* tmp_add_class_86;
  QSize* tmp_add_class_87;
  QSizeF* tmp_add_class_88;
  QString* tmp_add_class_89;
  QStringDataPtr* tmp_add_class_90;
  QStringList* tmp_add_class_91;
  QStringMatcher* tmp_add_class_92;
  QStringRef* tmp_add_class_93;
  QStringView* tmp_add_class_94;
  QSurface* tmp_add_class_95;
  QSurfaceFormat* tmp_add_class_96;
  QSysInfo* tmp_add_class_97;
  QTimerEvent* tmp_add_class_98;
  QTransform* tmp_add_class_99;
  QWindow* tmp_add_class_100;
  _G_fpos64_t* tmp_add_class_101;
  _G_fpos_t* tmp_add_class_102;
  _IO_cookie_io_functions_t* tmp_add_class_103;
  _XEvent* tmp_add_class_104;
  _XExtData* tmp_add_class_105;
  _XIMHotKeyTrigger* tmp_add_class_106;
  _XIMHotKeyTriggers* tmp_add_class_107;
  _XIMPreeditCaretCallbackStruct* tmp_add_class_108;
  _XIMPreeditDrawCallbackStruct* tmp_add_class_109;
  _XIMPreeditStateNotifyCallbackStruct* tmp_add_class_110;
  _XIMStatusDrawCallbackStruct* tmp_add_class_111;
  _XIMStringConversionCallbackStruct* tmp_add_class_112;
  _XIMStringConversionText* tmp_add_class_113;
  _XIMText* tmp_add_class_114;
  _XImage* tmp_add_class_115;
  iovec* tmp_add_class_116;
  xcb_alloc_color_cells_cookie_t* tmp_add_class_117;
  xcb_alloc_color_cells_reply_t* tmp_add_class_118;
  xcb_alloc_color_cells_request_t* tmp_add_class_119;
  xcb_alloc_color_cookie_t* tmp_add_class_120;
  xcb_alloc_color_planes_cookie_t* tmp_add_class_121;
  xcb_alloc_color_planes_reply_t* tmp_add_class_122;
  xcb_alloc_color_planes_request_t* tmp_add_class_123;
  xcb_alloc_color_reply_t* tmp_add_class_124;
  xcb_alloc_color_request_t* tmp_add_class_125;
  xcb_alloc_named_color_cookie_t* tmp_add_class_126;
  xcb_alloc_named_color_reply_t* tmp_add_class_127;
  xcb_alloc_named_color_request_t* tmp_add_class_128;
  xcb_allow_events_request_t* tmp_add_class_129;
  xcb_arc_iterator_t* tmp_add_class_130;
  xcb_arc_t* tmp_add_class_131;
  xcb_atom_iterator_t* tmp_add_class_132;
  xcb_auth_info_t* tmp_add_class_133;
  xcb_bell_request_t* tmp_add_class_134;
  xcb_bool32_iterator_t* tmp_add_class_135;
  xcb_button_iterator_t* tmp_add_class_136;
  xcb_button_press_event_t* tmp_add_class_137;
  xcb_change_active_pointer_grab_request_t* tmp_add_class_138;
  xcb_change_gc_request_t* tmp_add_class_139;
  xcb_change_gc_value_list_t* tmp_add_class_140;
  xcb_change_hosts_request_t* tmp_add_class_141;
  xcb_change_keyboard_control_request_t* tmp_add_class_142;
  xcb_change_keyboard_control_value_list_t* tmp_add_class_143;
  xcb_change_keyboard_mapping_request_t* tmp_add_class_144;
  xcb_change_pointer_control_request_t* tmp_add_class_145;
  xcb_change_property_request_t* tmp_add_class_146;
  xcb_change_save_set_request_t* tmp_add_class_147;
  xcb_change_window_attributes_request_t* tmp_add_class_148;
  xcb_change_window_attributes_value_list_t* tmp_add_class_149;
  xcb_char2b_iterator_t* tmp_add_class_150;
  xcb_char2b_t* tmp_add_class_151;
  xcb_charinfo_iterator_t* tmp_add_class_152;
  xcb_charinfo_t* tmp_add_class_153;
  xcb_circulate_notify_event_t* tmp_add_class_154;
  xcb_circulate_window_request_t* tmp_add_class_155;
  xcb_clear_area_request_t* tmp_add_class_156;
  xcb_client_message_data_iterator_t* tmp_add_class_157;
  xcb_client_message_data_t* tmp_add_class_158;
  xcb_client_message_event_t* tmp_add_class_159;
  xcb_close_font_request_t* tmp_add_class_160;
  xcb_coloritem_iterator_t* tmp_add_class_161;
  xcb_coloritem_t* tmp_add_class_162;
  xcb_colormap_iterator_t* tmp_add_class_163;
  xcb_colormap_notify_event_t* tmp_add_class_164;
  xcb_configure_notify_event_t* tmp_add_class_165;
  xcb_configure_request_event_t* tmp_add_class_166;
  xcb_configure_window_request_t* tmp_add_class_167;
  xcb_configure_window_value_list_t* tmp_add_class_168;
  xcb_convert_selection_request_t* tmp_add_class_169;
  xcb_copy_area_request_t* tmp_add_class_170;
  xcb_copy_colormap_and_free_request_t* tmp_add_class_171;
  xcb_copy_gc_request_t* tmp_add_class_172;
  xcb_copy_plane_request_t* tmp_add_class_173;
  xcb_create_colormap_request_t* tmp_add_class_174;
  xcb_create_cursor_request_t* tmp_add_class_175;
  xcb_create_gc_request_t* tmp_add_class_176;
  xcb_create_gc_value_list_t* tmp_add_class_177;
  xcb_create_glyph_cursor_request_t* tmp_add_class_178;
  xcb_create_notify_event_t* tmp_add_class_179;
  xcb_create_pixmap_request_t* tmp_add_class_180;
  xcb_create_window_request_t* tmp_add_class_181;
  xcb_create_window_value_list_t* tmp_add_class_182;
  xcb_cursor_iterator_t* tmp_add_class_183;
  xcb_delete_property_request_t* tmp_add_class_184;
  xcb_depth_iterator_t* tmp_add_class_185;
  xcb_depth_t* tmp_add_class_186;
  xcb_destroy_notify_event_t* tmp_add_class_187;
  xcb_destroy_subwindows_request_t* tmp_add_class_188;
  xcb_destroy_window_request_t* tmp_add_class_189;
  xcb_drawable_iterator_t* tmp_add_class_190;
  xcb_enter_notify_event_t* tmp_add_class_191;
  xcb_expose_event_t* tmp_add_class_192;
  xcb_fill_poly_request_t* tmp_add_class_193;
  xcb_focus_in_event_t* tmp_add_class_194;
  xcb_font_iterator_t* tmp_add_class_195;
  xcb_fontable_iterator_t* tmp_add_class_196;
  xcb_fontprop_iterator_t* tmp_add_class_197;
  xcb_fontprop_t* tmp_add_class_198;
  xcb_force_screen_saver_request_t* tmp_add_class_199;
  xcb_format_iterator_t* tmp_add_class_200;
  xcb_format_t* tmp_add_class_201;
  xcb_free_colormap_request_t* tmp_add_class_202;
  xcb_free_colors_request_t* tmp_add_class_203;
  xcb_free_cursor_request_t* tmp_add_class_204;
  xcb_free_gc_request_t* tmp_add_class_205;
  xcb_free_pixmap_request_t* tmp_add_class_206;
  xcb_gcontext_iterator_t* tmp_add_class_207;
  xcb_ge_generic_event_t* tmp_add_class_208;
  xcb_get_atom_name_cookie_t* tmp_add_class_209;
  xcb_get_atom_name_reply_t* tmp_add_class_210;
  xcb_get_atom_name_request_t* tmp_add_class_211;
  xcb_get_font_path_cookie_t* tmp_add_class_212;
  xcb_get_font_path_reply_t* tmp_add_class_213;
  xcb_get_font_path_request_t* tmp_add_class_214;
  xcb_get_geometry_cookie_t* tmp_add_class_215;
  xcb_get_geometry_reply_t* tmp_add_class_216;
  xcb_get_geometry_request_t* tmp_add_class_217;
  xcb_get_image_cookie_t* tmp_add_class_218;
  xcb_get_image_reply_t* tmp_add_class_219;
  xcb_get_image_request_t* tmp_add_class_220;
  xcb_get_input_focus_cookie_t* tmp_add_class_221;
  xcb_get_input_focus_reply_t* tmp_add_class_222;
  xcb_get_input_focus_request_t* tmp_add_class_223;
  xcb_get_keyboard_control_cookie_t* tmp_add_class_224;
  xcb_get_keyboard_control_reply_t* tmp_add_class_225;
  xcb_get_keyboard_control_request_t* tmp_add_class_226;
  xcb_get_keyboard_mapping_cookie_t* tmp_add_class_227;
  xcb_get_keyboard_mapping_reply_t* tmp_add_class_228;
  xcb_get_keyboard_mapping_request_t* tmp_add_class_229;
  xcb_get_modifier_mapping_cookie_t* tmp_add_class_230;
  xcb_get_modifier_mapping_reply_t* tmp_add_class_231;
  xcb_get_modifier_mapping_request_t* tmp_add_class_232;
  xcb_get_motion_events_cookie_t* tmp_add_class_233;
  xcb_get_motion_events_reply_t* tmp_add_class_234;
  xcb_get_motion_events_request_t* tmp_add_class_235;
  xcb_get_pointer_control_cookie_t* tmp_add_class_236;
  xcb_get_pointer_control_reply_t* tmp_add_class_237;
  xcb_get_pointer_control_request_t* tmp_add_class_238;
  xcb_get_pointer_mapping_cookie_t* tmp_add_class_239;
  xcb_get_pointer_mapping_reply_t* tmp_add_class_240;
  xcb_get_pointer_mapping_request_t* tmp_add_class_241;
  xcb_get_property_cookie_t* tmp_add_class_242;
  xcb_get_property_reply_t* tmp_add_class_243;
  xcb_get_property_request_t* tmp_add_class_244;
  xcb_get_screen_saver_cookie_t* tmp_add_class_245;
  xcb_get_screen_saver_reply_t* tmp_add_class_246;
  xcb_get_screen_saver_request_t* tmp_add_class_247;
  xcb_get_selection_owner_cookie_t* tmp_add_class_248;
  xcb_get_selection_owner_reply_t* tmp_add_class_249;
  xcb_get_selection_owner_request_t* tmp_add_class_250;
  xcb_get_window_attributes_cookie_t* tmp_add_class_251;
  xcb_get_window_attributes_reply_t* tmp_add_class_252;
  xcb_get_window_attributes_request_t* tmp_add_class_253;
  xcb_grab_button_request_t* tmp_add_class_254;
  xcb_grab_key_request_t* tmp_add_class_255;
  xcb_grab_keyboard_cookie_t* tmp_add_class_256;
  xcb_grab_keyboard_reply_t* tmp_add_class_257;
  xcb_grab_keyboard_request_t* tmp_add_class_258;
  xcb_grab_pointer_cookie_t* tmp_add_class_259;
  xcb_grab_pointer_reply_t* tmp_add_class_260;
  xcb_grab_pointer_request_t* tmp_add_class_261;
  xcb_grab_server_request_t* tmp_add_class_262;
  xcb_graphics_exposure_event_t* tmp_add_class_263;
  xcb_gravity_notify_event_t* tmp_add_class_264;
  xcb_host_iterator_t* tmp_add_class_265;
  xcb_host_t* tmp_add_class_266;
  xcb_image_text_16_request_t* tmp_add_class_267;
  xcb_image_text_8_request_t* tmp_add_class_268;
  xcb_install_colormap_request_t* tmp_add_class_269;
  xcb_intern_atom_cookie_t* tmp_add_class_270;
  xcb_intern_atom_reply_t* tmp_add_class_271;
  xcb_intern_atom_request_t* tmp_add_class_272;
  xcb_key_press_event_t* tmp_add_class_273;
  xcb_keycode32_iterator_t* tmp_add_class_274;
  xcb_keycode_iterator_t* tmp_add_class_275;
  xcb_keymap_notify_event_t* tmp_add_class_276;
  xcb_keysym_iterator_t* tmp_add_class_277;
  xcb_kill_client_request_t* tmp_add_class_278;
  xcb_list_extensions_cookie_t* tmp_add_class_279;
  xcb_list_extensions_reply_t* tmp_add_class_280;
  xcb_list_extensions_request_t* tmp_add_class_281;
  xcb_list_fonts_cookie_t* tmp_add_class_282;
  xcb_list_fonts_reply_t* tmp_add_class_283;
  xcb_list_fonts_request_t* tmp_add_class_284;
  xcb_list_fonts_with_info_cookie_t* tmp_add_class_285;
  xcb_list_fonts_with_info_reply_t* tmp_add_class_286;
  xcb_list_fonts_with_info_request_t* tmp_add_class_287;
  xcb_list_hosts_cookie_t* tmp_add_class_288;
  xcb_list_hosts_reply_t* tmp_add_class_289;
  xcb_list_hosts_request_t* tmp_add_class_290;
  xcb_list_installed_colormaps_cookie_t* tmp_add_class_291;
  xcb_list_installed_colormaps_reply_t* tmp_add_class_292;
  xcb_list_installed_colormaps_request_t* tmp_add_class_293;
  xcb_list_properties_cookie_t* tmp_add_class_294;
  xcb_list_properties_reply_t* tmp_add_class_295;
  xcb_list_properties_request_t* tmp_add_class_296;
  xcb_lookup_color_cookie_t* tmp_add_class_297;
  xcb_lookup_color_reply_t* tmp_add_class_298;
  xcb_lookup_color_request_t* tmp_add_class_299;
  xcb_map_notify_event_t* tmp_add_class_300;
  xcb_map_request_event_t* tmp_add_class_301;
  xcb_map_subwindows_request_t* tmp_add_class_302;
  xcb_map_window_request_t* tmp_add_class_303;
  xcb_mapping_notify_event_t* tmp_add_class_304;
  xcb_motion_notify_event_t* tmp_add_class_305;
  xcb_no_exposure_event_t* tmp_add_class_306;
  xcb_no_operation_request_t* tmp_add_class_307;
  xcb_open_font_request_t* tmp_add_class_308;
  xcb_pixmap_iterator_t* tmp_add_class_309;
  xcb_point_iterator_t* tmp_add_class_310;
  xcb_point_t* tmp_add_class_311;
  xcb_poly_arc_request_t* tmp_add_class_312;
  xcb_poly_fill_arc_request_t* tmp_add_class_313;
  xcb_poly_fill_rectangle_request_t* tmp_add_class_314;
  xcb_poly_line_request_t* tmp_add_class_315;
  xcb_poly_point_request_t* tmp_add_class_316;
  xcb_poly_rectangle_request_t* tmp_add_class_317;
  xcb_poly_segment_request_t* tmp_add_class_318;
  xcb_poly_text_16_request_t* tmp_add_class_319;
  xcb_poly_text_8_request_t* tmp_add_class_320;
  xcb_property_notify_event_t* tmp_add_class_321;
  xcb_put_image_request_t* tmp_add_class_322;
  xcb_query_best_size_cookie_t* tmp_add_class_323;
  xcb_query_best_size_reply_t* tmp_add_class_324;
  xcb_query_best_size_request_t* tmp_add_class_325;
  xcb_query_colors_cookie_t* tmp_add_class_326;
  xcb_query_colors_reply_t* tmp_add_class_327;
  xcb_query_colors_request_t* tmp_add_class_328;
  xcb_query_extension_cookie_t* tmp_add_class_329;
  xcb_query_extension_reply_t* tmp_add_class_330;
  xcb_query_extension_request_t* tmp_add_class_331;
  xcb_query_font_cookie_t* tmp_add_class_332;
  xcb_query_font_reply_t* tmp_add_class_333;
  xcb_query_font_request_t* tmp_add_class_334;
  xcb_query_keymap_cookie_t* tmp_add_class_335;
  xcb_query_keymap_reply_t* tmp_add_class_336;
  xcb_query_keymap_request_t* tmp_add_class_337;
  xcb_query_pointer_cookie_t* tmp_add_class_338;
  xcb_query_pointer_reply_t* tmp_add_class_339;
  xcb_query_pointer_request_t* tmp_add_class_340;
  xcb_query_text_extents_cookie_t* tmp_add_class_341;
  xcb_query_text_extents_reply_t* tmp_add_class_342;
  xcb_query_text_extents_request_t* tmp_add_class_343;
  xcb_query_tree_cookie_t* tmp_add_class_344;
  xcb_query_tree_reply_t* tmp_add_class_345;
  xcb_query_tree_request_t* tmp_add_class_346;
  xcb_recolor_cursor_request_t* tmp_add_class_347;
  xcb_rectangle_iterator_t* tmp_add_class_348;
  xcb_rectangle_t* tmp_add_class_349;
  xcb_reparent_notify_event_t* tmp_add_class_350;
  xcb_reparent_window_request_t* tmp_add_class_351;
  xcb_request_error_t* tmp_add_class_352;
  xcb_resize_request_event_t* tmp_add_class_353;
  xcb_rgb_iterator_t* tmp_add_class_354;
  xcb_rgb_t* tmp_add_class_355;
  xcb_rotate_properties_request_t* tmp_add_class_356;
  xcb_screen_iterator_t* tmp_add_class_357;
  xcb_screen_t* tmp_add_class_358;
  xcb_segment_iterator_t* tmp_add_class_359;
  xcb_segment_t* tmp_add_class_360;
  xcb_selection_clear_event_t* tmp_add_class_361;
  xcb_selection_notify_event_t* tmp_add_class_362;
  xcb_selection_request_event_t* tmp_add_class_363;
  xcb_send_event_request_t* tmp_add_class_364;
  xcb_set_access_control_request_t* tmp_add_class_365;
  xcb_set_clip_rectangles_request_t* tmp_add_class_366;
  xcb_set_close_down_mode_request_t* tmp_add_class_367;
  xcb_set_dashes_request_t* tmp_add_class_368;
  xcb_set_font_path_request_t* tmp_add_class_369;
  xcb_set_input_focus_request_t* tmp_add_class_370;
  xcb_set_modifier_mapping_cookie_t* tmp_add_class_371;
  xcb_set_modifier_mapping_reply_t* tmp_add_class_372;
  xcb_set_modifier_mapping_request_t* tmp_add_class_373;
  xcb_set_pointer_mapping_cookie_t* tmp_add_class_374;
  xcb_set_pointer_mapping_reply_t* tmp_add_class_375;
  xcb_set_pointer_mapping_request_t* tmp_add_class_376;
  xcb_set_screen_saver_request_t* tmp_add_class_377;
  xcb_set_selection_owner_request_t* tmp_add_class_378;
  xcb_setup_authenticate_iterator_t* tmp_add_class_379;
  xcb_setup_authenticate_t* tmp_add_class_380;
  xcb_setup_failed_iterator_t* tmp_add_class_381;
  xcb_setup_failed_t* tmp_add_class_382;
  xcb_setup_iterator_t* tmp_add_class_383;
  xcb_setup_request_iterator_t* tmp_add_class_384;
  xcb_setup_request_t* tmp_add_class_385;
  xcb_setup_t* tmp_add_class_386;
  xcb_store_colors_request_t* tmp_add_class_387;
  xcb_store_named_color_request_t* tmp_add_class_388;
  xcb_str_iterator_t* tmp_add_class_389;
  xcb_str_t* tmp_add_class_390;
  xcb_timecoord_iterator_t* tmp_add_class_391;
  xcb_timecoord_t* tmp_add_class_392;
  xcb_timestamp_iterator_t* tmp_add_class_393;
  xcb_translate_coordinates_cookie_t* tmp_add_class_394;
  xcb_translate_coordinates_reply_t* tmp_add_class_395;
  xcb_translate_coordinates_request_t* tmp_add_class_396;
  xcb_ungrab_button_request_t* tmp_add_class_397;
  xcb_ungrab_key_request_t* tmp_add_class_398;
  xcb_ungrab_keyboard_request_t* tmp_add_class_399;
  xcb_ungrab_pointer_request_t* tmp_add_class_400;
  xcb_ungrab_server_request_t* tmp_add_class_401;
  xcb_uninstall_colormap_request_t* tmp_add_class_402;
  xcb_unmap_notify_event_t* tmp_add_class_403;
  xcb_unmap_subwindows_request_t* tmp_add_class_404;
  xcb_unmap_window_request_t* tmp_add_class_405;
  xcb_value_error_t* tmp_add_class_406;
  xcb_visibility_notify_event_t* tmp_add_class_407;
  xcb_visualid_iterator_t* tmp_add_class_408;
  xcb_visualtype_iterator_t* tmp_add_class_409;
  xcb_visualtype_t* tmp_add_class_410;
  xcb_warp_pointer_request_t* tmp_add_class_411;
  xcb_window_iterator_t* tmp_add_class_412;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/1f2O7aSWmD/dump1.h"  -I/usr/include/KF5/KWindowSystem -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtGui -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

