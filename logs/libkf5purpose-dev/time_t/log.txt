Temporary header file '/tmp/4AW3Dz0G2J/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KF5/purpose/purpose/alternativesmodel.h"
  #include "/usr/include/KF5/purpose/purpose/configuration.h"
  #include "/usr/include/KF5/purpose/purpose/job.h"
  #include "/usr/include/KF5/purpose/purpose/pluginbase.h"
  #include "/usr/include/KF5/purpose/purpose/purpose_export.h"
  #include "/usr/include/KF5/purpose/purpose_version.h"
  #include "/usr/include/KF5/purposewidgets/purpose/menu.h"
  #include "/usr/include/KF5/purposewidgets/purpose/purposewidgets_export.h"
  #include "/usr/include/KF5/purposewidgets/purposewidgets/menu.h"

  // add namespaces
  namespace Purpose{typedef int tmp_add_type_1;}
  Purpose::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_2;}
  QAlgorithmsPrivate::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_3;}
  QColorConstants::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_4;}}
  QColorConstants::Svg::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QJsonPrivate{typedef int tmp_add_type_5;}
  QJsonPrivate::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace Qt{typedef int tmp_add_type_6;}
  Qt::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QTextStreamFunctions{typedef int tmp_add_type_7;}
  QTextStreamFunctions::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_8;}
  QtGlobalStatic::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_9;}
  QtMetaTypePrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_10;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_11;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_12;}
  QtPrivate::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_13;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_14;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_15;}
  QtSharedPointer::tmp_add_type_15 tmp_add_func_15(){return 0;};

  // add classes
  KJob* tmp_add_class_0;
  QAbstractItemModel* tmp_add_class_1;
  QAbstractListModel* tmp_add_class_2;
  QAbstractTableModel* tmp_add_class_3;
  QAction* tmp_add_class_4;
  QActionGroup* tmp_add_class_5;
  QArrayData* tmp_add_class_6;
  QAssociativeIterable* tmp_add_class_7;
  QAtomicInt* tmp_add_class_8;
  QBrush* tmp_add_class_9;
  QBrushData* tmp_add_class_10;
  QByteArray* tmp_add_class_11;
  QByteArrayDataPtr* tmp_add_class_12;
  QByteRef* tmp_add_class_13;
  QCborError* tmp_add_class_14;
  QCborParserError* tmp_add_class_15;
  QCborValue* tmp_add_class_16;
  QCborValueRef* tmp_add_class_17;
  QChar* tmp_add_class_18;
  QCharRef* tmp_add_class_19;
  QColor* tmp_add_class_20;
  QConicalGradient* tmp_add_class_21;
  QContiguousCacheData* tmp_add_class_22;
  QCursor* tmp_add_class_23;
  QDataStream* tmp_add_class_24;
  QDate* tmp_add_class_25;
  QDateTime* tmp_add_class_26;
  QDebug* tmp_add_class_27;
  QDebugStateSaver* tmp_add_class_28;
  QFlag* tmp_add_class_29;
  QFont* tmp_add_class_30;
  QFontInfo* tmp_add_class_31;
  QFontMetrics* tmp_add_class_32;
  QFontMetricsF* tmp_add_class_33;
  QGenericArgument* tmp_add_class_34;
  QGenericReturnArgument* tmp_add_class_35;
  QGradient* tmp_add_class_36;
  QHashData* tmp_add_class_37;
  QHashDummyValue* tmp_add_class_38;
  QIODevice* tmp_add_class_39;
  QIcon* tmp_add_class_40;
  QImage* tmp_add_class_41;
  QIncompatibleFlag* tmp_add_class_42;
  QInternal* tmp_add_class_43;
  QJsonArray* tmp_add_class_44;
  QJsonObject* tmp_add_class_45;
  QJsonValue* tmp_add_class_46;
  QJsonValuePtr* tmp_add_class_47;
  QJsonValueRef* tmp_add_class_48;
  QJsonValueRefPtr* tmp_add_class_49;
  QKeySequence* tmp_add_class_50;
  QLatin1Char* tmp_add_class_51;
  QLatin1String* tmp_add_class_52;
  QLine* tmp_add_class_53;
  QLineF* tmp_add_class_54;
  QLinearGradient* tmp_add_class_55;
  QListData* tmp_add_class_56;
  QLocale* tmp_add_class_57;
  QMapDataBase* tmp_add_class_58;
  QMapNodeBase* tmp_add_class_59;
  QMargins* tmp_add_class_60;
  QMarginsF* tmp_add_class_61;
  QMatrix* tmp_add_class_62;
  QMenu* tmp_add_class_63;
  QMessageLogContext* tmp_add_class_64;
  QMessageLogger* tmp_add_class_65;
  QMetaObject* tmp_add_class_66;
  QMetaType* tmp_add_class_67;
  QMimeData* tmp_add_class_68;
  QModelIndex* tmp_add_class_69;
  QNoDebug* tmp_add_class_70;
  QObject* tmp_add_class_71;
  QObjectData* tmp_add_class_72;
  QObjectUserData* tmp_add_class_73;
  QPaintDevice* tmp_add_class_74;
  QPalette* tmp_add_class_75;
  QPersistentModelIndex* tmp_add_class_76;
  QPixelFormat* tmp_add_class_77;
  QPixmap* tmp_add_class_78;
  QPoint* tmp_add_class_79;
  QPointF* tmp_add_class_80;
  QPolygon* tmp_add_class_81;
  QPolygonF* tmp_add_class_82;
  QRadialGradient* tmp_add_class_83;
  QRect* tmp_add_class_84;
  QRectF* tmp_add_class_85;
  QRegExp* tmp_add_class_86;
  QRegion* tmp_add_class_87;
  QRegularExpression* tmp_add_class_88;
  QRegularExpressionMatch* tmp_add_class_89;
  QRegularExpressionMatchIterator* tmp_add_class_90;
  QRgba64* tmp_add_class_91;
  QScopedPointerPodDeleter* tmp_add_class_92;
  QSequentialIterable* tmp_add_class_93;
  QSharedData* tmp_add_class_94;
  QSignalBlocker* tmp_add_class_95;
  QSize* tmp_add_class_96;
  QSizeF* tmp_add_class_97;
  QSizePolicy* tmp_add_class_98;
  QString* tmp_add_class_99;
  QStringDataPtr* tmp_add_class_100;
  QStringList* tmp_add_class_101;
  QStringMatcher* tmp_add_class_102;
  QStringRef* tmp_add_class_103;
  QStringView* tmp_add_class_104;
  QSysInfo* tmp_add_class_105;
  QTextStream* tmp_add_class_106;
  QTextStreamManipulator* tmp_add_class_107;
  QTime* tmp_add_class_108;
  QTransform* tmp_add_class_109;
  QUrl* tmp_add_class_110;
  QUuid* tmp_add_class_111;
  QVariant* tmp_add_class_112;
  QVariantComparisonHelper* tmp_add_class_113;
  QWidget* tmp_add_class_114;
  QWidgetData* tmp_add_class_115;
  _G_fpos64_t* tmp_add_class_116;
  _G_fpos_t* tmp_add_class_117;
  _IO_cookie_io_functions_t* tmp_add_class_118;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/4AW3Dz0G2J/dump1.h"  -I/usr/include/KF5/purpose -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/KF5/purposewidgets -I/usr/include/KF5/KCoreAddons -I/usr/include/arm-linux-gnueabihf/qt5/QtWidgets -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

