Temporary header file '/tmp/2uzGmyVARm/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KF5/KGlobalAccel/kglobalaccel.h"
  #include "/usr/include/KF5/KGlobalAccel/kglobalaccel_export.h"
  #include "/usr/include/KF5/KGlobalAccel/kglobalaccel_version.h"
  #include "/usr/include/KF5/KGlobalAccel/kglobalshortcutinfo.h"
  #include "/usr/include/KF5/KGlobalAccel/private/kf5globalaccelprivate_export.h"
  #include "/usr/include/KF5/KGlobalAccel/private/kglobalaccel_interface.h"
  #include "/usr/include/KF5/KGlobalAccel/private/kglobalacceld.h"

  // add namespaces
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_1;}
  QAlgorithmsPrivate::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QDBus{typedef int tmp_add_type_2;}
  QDBus::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QDBusPendingReplyTypes{typedef int tmp_add_type_3;}
  QDBusPendingReplyTypes::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QJsonPrivate{typedef int tmp_add_type_4;}
  QJsonPrivate::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace Qt{typedef int tmp_add_type_5;}
  Qt::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QTextStreamFunctions{typedef int tmp_add_type_6;}
  QTextStreamFunctions::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_7;}
  QtGlobalStatic::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_8;}
  QtMetaTypePrivate::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_9;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_10;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_11;}
  QtPrivate::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_12;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_13;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_14;}
  QtSharedPointer::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace QtStringBuilder{typedef int tmp_add_type_15;}
  QtStringBuilder::tmp_add_type_15 tmp_add_func_15(){return 0;};

  // add classes
  KGlobalAccel* tmp_add_class_0;
  KGlobalAccelD* tmp_add_class_1;
  KGlobalAccelInterface* tmp_add_class_2;
  KGlobalAccelInterfaceV2* tmp_add_class_3;
  KGlobalShortcutInfo* tmp_add_class_4;
  QAbstractAnimation* tmp_add_class_5;
  QAbstractConcatenable* tmp_add_class_6;
  QAbstractEventDispatcher* tmp_add_class_7;
  QAbstractItemModel* tmp_add_class_8;
  QAbstractListModel* tmp_add_class_9;
  QAbstractNativeEventFilter* tmp_add_class_10;
  QAbstractProxyModel* tmp_add_class_11;
  QAbstractState* tmp_add_class_12;
  QAbstractTableModel* tmp_add_class_13;
  QAbstractTransition* tmp_add_class_14;
  QAnimationDriver* tmp_add_class_15;
  QAnimationGroup* tmp_add_class_16;
  QArrayData* tmp_add_class_17;
  QAssociativeIterable* tmp_add_class_18;
  QAtomicInt* tmp_add_class_19;
  QBasicMutex* tmp_add_class_20;
  QBasicTimer* tmp_add_class_21;
  QBitArray* tmp_add_class_22;
  QBitRef* tmp_add_class_23;
  QBuffer* tmp_add_class_24;
  QByteArray* tmp_add_class_25;
  QByteArrayDataPtr* tmp_add_class_26;
  QByteArrayMatcher* tmp_add_class_27;
  QByteRef* tmp_add_class_28;
  QCalendar* tmp_add_class_29;
  QCborArray* tmp_add_class_30;
  QCborError* tmp_add_class_31;
  QCborMap* tmp_add_class_32;
  QCborParserError* tmp_add_class_33;
  QCborStreamReader* tmp_add_class_34;
  QCborStreamWriter* tmp_add_class_35;
  QCborValue* tmp_add_class_36;
  QCborValueRef* tmp_add_class_37;
  QChar* tmp_add_class_38;
  QCharRef* tmp_add_class_39;
  QChildEvent* tmp_add_class_40;
  QCollator* tmp_add_class_41;
  QCollatorSortKey* tmp_add_class_42;
  QCommandLineOption* tmp_add_class_43;
  QCommandLineParser* tmp_add_class_44;
  QConcatenateTablesProxyModel* tmp_add_class_45;
  QContiguousCacheData* tmp_add_class_46;
  QCoreApplication* tmp_add_class_47;
  QCryptographicHash* tmp_add_class_48;
  QDBusAbstractAdaptor* tmp_add_class_49;
  QDBusAbstractInterface* tmp_add_class_50;
  QDBusAbstractInterfaceBase* tmp_add_class_51;
  QDBusArgument* tmp_add_class_52;
  QDBusConnection* tmp_add_class_53;
  QDBusConnectionInterface* tmp_add_class_54;
  QDBusContext* tmp_add_class_55;
  QDBusError* tmp_add_class_56;
  QDBusInterface* tmp_add_class_57;
  QDBusMessage* tmp_add_class_58;
  QDBusMetaType* tmp_add_class_59;
  QDBusObjectPath* tmp_add_class_60;
  QDBusPendingCall* tmp_add_class_61;
  QDBusPendingCallWatcher* tmp_add_class_62;
  QDBusPendingReplyData* tmp_add_class_63;
  QDBusServer* tmp_add_class_64;
  QDBusServiceWatcher* tmp_add_class_65;
  QDBusSignature* tmp_add_class_66;
  QDBusUnixFileDescriptor* tmp_add_class_67;
  QDBusVariant* tmp_add_class_68;
  QDBusVirtualObject* tmp_add_class_69;
  QDataStream* tmp_add_class_70;
  QDate* tmp_add_class_71;
  QDateTime* tmp_add_class_72;
  QDeadlineTimer* tmp_add_class_73;
  QDebug* tmp_add_class_74;
  QDebugStateSaver* tmp_add_class_75;
  QDeferredDeleteEvent* tmp_add_class_76;
  QDir* tmp_add_class_77;
  QDirIterator* tmp_add_class_78;
  QDynamicPropertyChangeEvent* tmp_add_class_79;
  QEasingCurve* tmp_add_class_80;
  QElapsedTimer* tmp_add_class_81;
  QEvent* tmp_add_class_82;
  QEventLoop* tmp_add_class_83;
  QEventLoopLocker* tmp_add_class_84;
  QEventTransition* tmp_add_class_85;
  QException* tmp_add_class_86;
  QFactoryInterface* tmp_add_class_87;
  QFile* tmp_add_class_88;
  QFileDevice* tmp_add_class_89;
  QFileInfo* tmp_add_class_90;
  QFileSelector* tmp_add_class_91;
  QFileSystemWatcher* tmp_add_class_92;
  QFinalState* tmp_add_class_93;
  QFlag* tmp_add_class_94;
  QFutureInterfaceBase* tmp_add_class_95;
  QFutureWatcherBase* tmp_add_class_96;
  QGenericArgument* tmp_add_class_97;
  QGenericReturnArgument* tmp_add_class_98;
  QHashData* tmp_add_class_99;
  QHashDummyValue* tmp_add_class_100;
  QHistoryState* tmp_add_class_101;
  QIODevice* tmp_add_class_102;
  QIdentityProxyModel* tmp_add_class_103;
  QIncompatibleFlag* tmp_add_class_104;
  QInternal* tmp_add_class_105;
  QItemSelection* tmp_add_class_106;
  QItemSelectionModel* tmp_add_class_107;
  QItemSelectionRange* tmp_add_class_108;
  QJsonArray* tmp_add_class_109;
  QJsonDocument* tmp_add_class_110;
  QJsonObject* tmp_add_class_111;
  QJsonParseError* tmp_add_class_112;
  QJsonValue* tmp_add_class_113;
  QJsonValuePtr* tmp_add_class_114;
  QJsonValueRef* tmp_add_class_115;
  QJsonValueRefPtr* tmp_add_class_116;
  QKeySequence* tmp_add_class_117;
  QLatin1Char* tmp_add_class_118;
  QLatin1String* tmp_add_class_119;
  QLibrary* tmp_add_class_120;
  QLibraryInfo* tmp_add_class_121;
  QLine* tmp_add_class_122;
  QLineF* tmp_add_class_123;
  QLinkedListData* tmp_add_class_124;
  QListData* tmp_add_class_125;
  QLocale* tmp_add_class_126;
  QLockFile* tmp_add_class_127;
  QLoggingCategory* tmp_add_class_128;
  QMapDataBase* tmp_add_class_129;
  QMapNodeBase* tmp_add_class_130;
  QMargins* tmp_add_class_131;
  QMarginsF* tmp_add_class_132;
  QMessageAuthenticationCode* tmp_add_class_133;
  QMessageLogContext* tmp_add_class_134;
  QMessageLogger* tmp_add_class_135;
  QMetaClassInfo* tmp_add_class_136;
  QMetaEnum* tmp_add_class_137;
  QMetaMethod* tmp_add_class_138;
  QMetaObject* tmp_add_class_139;
  QMetaProperty* tmp_add_class_140;
  QMetaType* tmp_add_class_141;
  QMimeData* tmp_add_class_142;
  QMimeDatabase* tmp_add_class_143;
  QMimeType* tmp_add_class_144;
  QModelIndex* tmp_add_class_145;
  QMutex* tmp_add_class_146;
  QMutexLocker* tmp_add_class_147;
  QNoDebug* tmp_add_class_148;
  QObject* tmp_add_class_149;
  QObjectCleanupHandler* tmp_add_class_150;
  QObjectData* tmp_add_class_151;
  QObjectUserData* tmp_add_class_152;
  QOperatingSystemVersion* tmp_add_class_153;
  QParallelAnimationGroup* tmp_add_class_154;
  QPauseAnimation* tmp_add_class_155;
  QPersistentModelIndex* tmp_add_class_156;
  QPluginLoader* tmp_add_class_157;
  QPoint* tmp_add_class_158;
  QPointF* tmp_add_class_159;
  QProcess* tmp_add_class_160;
  QProcessEnvironment* tmp_add_class_161;
  QPropertyAnimation* tmp_add_class_162;
  QRandomGenerator* tmp_add_class_163;
  QRandomGenerator64* tmp_add_class_164;
  QReadLocker* tmp_add_class_165;
  QReadWriteLock* tmp_add_class_166;
  QRect* tmp_add_class_167;
  QRectF* tmp_add_class_168;
  QRecursiveMutex* tmp_add_class_169;
  QRegExp* tmp_add_class_170;
  QRegularExpression* tmp_add_class_171;
  QRegularExpressionMatch* tmp_add_class_172;
  QRegularExpressionMatchIterator* tmp_add_class_173;
  QResource* tmp_add_class_174;
  QRunnable* tmp_add_class_175;
  QSaveFile* tmp_add_class_176;
  QScopedPointerPodDeleter* tmp_add_class_177;
  QSemaphore* tmp_add_class_178;
  QSemaphoreReleaser* tmp_add_class_179;
  QSequentialAnimationGroup* tmp_add_class_180;
  QSequentialIterable* tmp_add_class_181;
  QSettings* tmp_add_class_182;
  QSharedData* tmp_add_class_183;
  QSharedMemory* tmp_add_class_184;
  QSignalBlocker* tmp_add_class_185;
  QSignalMapper* tmp_add_class_186;
  QSignalTransition* tmp_add_class_187;
  QSize* tmp_add_class_188;
  QSizeF* tmp_add_class_189;
  QSocketDescriptor* tmp_add_class_190;
  QSocketNotifier* tmp_add_class_191;
  QSortFilterProxyModel* tmp_add_class_192;
  QStandardPaths* tmp_add_class_193;
  QState* tmp_add_class_194;
  QStateMachine* tmp_add_class_195;
  QStaticByteArrayMatcherBase* tmp_add_class_196;
  QStaticPlugin* tmp_add_class_197;
  QStorageInfo* tmp_add_class_198;
  QString* tmp_add_class_199;
  QStringDataPtr* tmp_add_class_200;
  QStringList* tmp_add_class_201;
  QStringListModel* tmp_add_class_202;
  QStringMatcher* tmp_add_class_203;
  QStringRef* tmp_add_class_204;
  QStringView* tmp_add_class_205;
  QSysInfo* tmp_add_class_206;
  QSystemSemaphore* tmp_add_class_207;
  QTemporaryDir* tmp_add_class_208;
  QTemporaryFile* tmp_add_class_209;
  QTextBoundaryFinder* tmp_add_class_210;
  QTextCodec* tmp_add_class_211;
  QTextDecoder* tmp_add_class_212;
  QTextEncoder* tmp_add_class_213;
  QTextStream* tmp_add_class_214;
  QTextStreamManipulator* tmp_add_class_215;
  QThread* tmp_add_class_216;
  QThreadPool* tmp_add_class_217;
  QThreadStorageData* tmp_add_class_218;
  QTime* tmp_add_class_219;
  QTimeLine* tmp_add_class_220;
  QTimeZone* tmp_add_class_221;
  QTimer* tmp_add_class_222;
  QTimerEvent* tmp_add_class_223;
  QTranslator* tmp_add_class_224;
  QTransposeProxyModel* tmp_add_class_225;
  QUnhandledException* tmp_add_class_226;
  QUrl* tmp_add_class_227;
  QUrlQuery* tmp_add_class_228;
  QUuid* tmp_add_class_229;
  QVariant* tmp_add_class_230;
  QVariantAnimation* tmp_add_class_231;
  QVariantComparisonHelper* tmp_add_class_232;
  QVersionNumber* tmp_add_class_233;
  QWaitCondition* tmp_add_class_234;
  QWriteLocker* tmp_add_class_235;
  QXmlStreamAttribute* tmp_add_class_236;
  QXmlStreamAttributes* tmp_add_class_237;
  QXmlStreamEntityDeclaration* tmp_add_class_238;
  QXmlStreamEntityResolver* tmp_add_class_239;
  QXmlStreamNamespaceDeclaration* tmp_add_class_240;
  QXmlStreamNotationDeclaration* tmp_add_class_241;
  QXmlStreamReader* tmp_add_class_242;
  QXmlStreamStringRef* tmp_add_class_243;
  QXmlStreamWriter* tmp_add_class_244;
  _G_fpos64_t* tmp_add_class_245;
  _G_fpos_t* tmp_add_class_246;
  _IO_cookie_io_functions_t* tmp_add_class_247;
  qfloat16* tmp_add_class_248;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/2uzGmyVARm/dump1.h"  -I/usr/include/KF5/KGlobalAccel -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtGui -I/usr/include/arm-linux-gnueabihf/qt5/QtDBus -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

