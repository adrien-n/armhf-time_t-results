Temporary header file '/tmp/MvePT7COyO/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/apertium/a.h"
  #include "/usr/include/apertium/align.h"
  #include "/usr/include/apertium/analysis.h"
  #include "/usr/include/apertium/apertium_re.h"
  #include "/usr/include/apertium/caps_compiler.h"
  #include "/usr/include/apertium/caps_restorer.h"
  #include "/usr/include/apertium/collection.h"
  #include "/usr/include/apertium/constant_manager.h"
  #include "/usr/include/apertium/endian_double_util.h"
  #include "/usr/include/apertium/exception.h"
  #include "/usr/include/apertium/exception_type.h"
  #include "/usr/include/apertium/feature_vec.h"
  #include "/usr/include/apertium/feature_vec_averager.h"
  #include "/usr/include/apertium/file_morpho_stream.h"
  #include "/usr/include/apertium/file_tagger.h"
  #include "/usr/include/apertium/hmm.h"
  #include "/usr/include/apertium/i.h"
  #include "/usr/include/apertium/interchunk.h"
  #include "/usr/include/apertium/interchunk_word.h"
  #include "/usr/include/apertium/latex_accentsmap.h"
  #include "/usr/include/apertium/lemma.h"
  #include "/usr/include/apertium/lexical_unit.h"
  #include "/usr/include/apertium/linebreak.h"
  #include "/usr/include/apertium/lswpost.h"
  #include "/usr/include/apertium/morpheme.h"
  #include "/usr/include/apertium/morpho_stream.h"
  #include "/usr/include/apertium/mtx_reader.h"
  #include "/usr/include/apertium/optional.h"
  #include "/usr/include/apertium/perceptron_spec.h"
  #include "/usr/include/apertium/perceptron_tagger.h"
  #include "/usr/include/apertium/postchunk.h"
  #include "/usr/include/apertium/pretransfer.h"
  #include "/usr/include/apertium/sentence_stream.h"
  #include "/usr/include/apertium/shell_utils.h"
  #include "/usr/include/apertium/stream.h"
  #include "/usr/include/apertium/stream_tagger.h"
  #include "/usr/include/apertium/streamed_type.h"
  #include "/usr/include/apertium/string_to_wostream.h"
  #include "/usr/include/apertium/tagger_data.h"
  #include "/usr/include/apertium/tagger_data_hmm.h"
  #include "/usr/include/apertium/tagger_data_lsw.h"
  #include "/usr/include/apertium/tagger_data_percep_coarse_tags.h"
  #include "/usr/include/apertium/tagger_flags.h"
  #include "/usr/include/apertium/tagger_utils.h"
  #include "/usr/include/apertium/tagger_word.h"
  #include "/usr/include/apertium/tmx_align_parameters.h"
  #include "/usr/include/apertium/tmx_aligner_tool.h"
  #include "/usr/include/apertium/tmx_alignment.h"
  #include "/usr/include/apertium/tmx_arguments_parser.h"
  #include "/usr/include/apertium/tmx_book_to_matrix.h"
  #include "/usr/include/apertium/tmx_builder.h"
  #include "/usr/include/apertium/tmx_dic_tree.h"
  #include "/usr/include/apertium/tmx_dictionary.h"
  #include "/usr/include/apertium/tmx_quasi_diagonal.h"
  #include "/usr/include/apertium/tmx_serialize_impl.h"
  #include "/usr/include/apertium/tmx_strings_and_streams.h"
  #include "/usr/include/apertium/tmx_trail_postprocessors.h"
  #include "/usr/include/apertium/tmx_translate.h"
  #include "/usr/include/apertium/tmx_words.h"
  #include "/usr/include/apertium/transfer.h"
  #include "/usr/include/apertium/transfer_base.h"
  #include "/usr/include/apertium/transfer_data.h"
  #include "/usr/include/apertium/transfer_instr.h"
  #include "/usr/include/apertium/transfer_mult.h"
  #include "/usr/include/apertium/transfer_regex.h"
  #include "/usr/include/apertium/transfer_token.h"
  #include "/usr/include/apertium/transfer_word.h"
  #include "/usr/include/apertium/trx_reader.h"
  #include "/usr/include/apertium/tsx_reader.h"
  #include "/usr/include/apertium/ttag.h"
  #include "/usr/include/apertium/unigram_tagger.h"
  #include "/usr/include/apertium/unlocked_cstdio.h"
  #include "/usr/include/apertium/utils.h"
  #include "/usr/include/apertium/xml_reader.h"

  // add namespaces
  namespace Apertium{typedef int tmp_add_type_1;}
  Apertium::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace Apertium{namespace Exception{typedef int tmp_add_type_2;}}
  Apertium::Exception::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace Apertium{namespace Exception{namespace Analysis{typedef int tmp_add_type_3;}}}
  Apertium::Exception::Analysis::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace Apertium{namespace Exception{namespace Deserialiser{typedef int tmp_add_type_4;}}}
  Apertium::Exception::Deserialiser::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace Apertium{namespace Exception{namespace LexicalUnit{typedef int tmp_add_type_5;}}}
  Apertium::Exception::LexicalUnit::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace Apertium{namespace Exception{namespace Morpheme{typedef int tmp_add_type_6;}}}
  Apertium::Exception::Morpheme::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace Apertium{namespace Exception{namespace Optional{typedef int tmp_add_type_7;}}}
  Apertium::Exception::Optional::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace Apertium{namespace Exception{namespace PerceptronTagger{typedef int tmp_add_type_8;}}}
  Apertium::Exception::PerceptronTagger::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace Apertium{namespace Exception{namespace Serialiser{typedef int tmp_add_type_9;}}}
  Apertium::Exception::Serialiser::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace Apertium{namespace Exception{namespace Shell{typedef int tmp_add_type_10;}}}
  Apertium::Exception::Shell::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace Apertium{namespace Exception{namespace Stream{typedef int tmp_add_type_11;}}}
  Apertium::Exception::Stream::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace Apertium{namespace Exception{namespace Tag{typedef int tmp_add_type_12;}}}
  Apertium::Exception::Tag::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace Apertium{namespace SentenceStream{typedef int tmp_add_type_13;}}
  Apertium::SentenceStream::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace Apertium{namespace ShellUtils{typedef int tmp_add_type_14;}}
  Apertium::ShellUtils::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace detail{typedef int tmp_add_type_15;}
  detail::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace TMXAligner{typedef int tmp_add_type_16;}
  TMXAligner::tmp_add_type_16 tmp_add_func_16(){return 0;};
  namespace utf8{typedef int tmp_add_type_17;}
  utf8::tmp_add_type_17 tmp_add_func_17(){return 0;};
  namespace utf8{namespace internal{typedef int tmp_add_type_18;}}
  utf8::internal::tmp_add_type_18 tmp_add_func_18(){return 0;};
  namespace utf8{namespace unchecked{typedef int tmp_add_type_19;}}
  utf8::unchecked::tmp_add_type_19 tmp_add_func_19(){return 0;};
  namespace Apertium{namespace Exception{namespace apertium_tagger{typedef int tmp_add_type_20;}}}
  Apertium::Exception::apertium_tagger::tmp_add_type_20 tmp_add_func_20(){return 0;};
  namespace Apertium{namespace Exception{namespace wchar_t_ExceptionType{typedef int tmp_add_type_21;}}}
  Apertium::Exception::wchar_t_ExceptionType::tmp_add_type_21 tmp_add_func_21(){return 0;};
  namespace icu_72{typedef int tmp_add_type_22;}
  icu_72::tmp_add_type_22 tmp_add_func_22(){return 0;};
  namespace tagger_utils{typedef int tmp_add_type_23;}
  tagger_utils::tmp_add_type_23 tmp_add_func_23(){return 0;};

  // add classes
  AccentsMap* tmp_add_class_0;
  AlignParameters* tmp_add_class_1;
  Alphabet* tmp_add_class_2;
  AnyData* tmp_add_class_3;
  ApertiumRE* tmp_add_class_4;
  Arguments* tmp_add_class_5;
  CapsCompiler* tmp_add_class_6;
  CapsRestorer* tmp_add_class_7;
  Collection* tmp_add_class_8;
  ConstantManager* tmp_add_class_9;
  Dest* tmp_add_class_10;
  EndianDoubleUtil* tmp_add_class_11;
  FSTProcessor* tmp_add_class_12;
  FileMorphoStream* tmp_add_class_13;
  HMM* tmp_add_class_14;
  InputFile* tmp_add_class_15;
  Interchunk* tmp_add_class_16;
  InterchunkWord* tmp_add_class_17;
  LSWPoST* tmp_add_class_18;
  MatchExe* tmp_add_class_19;
  MatchNode* tmp_add_class_20;
  MatchState* tmp_add_class_21;
  MorphoStream* tmp_add_class_22;
  Node* tmp_add_class_23;
  PatternList* tmp_add_class_24;
  Postchunk* tmp_add_class_25;
  SortedVector* tmp_add_class_26;
  State* tmp_add_class_27;
  StringUtils* tmp_add_class_28;
  TEnforceAfterRule* tmp_add_class_29;
  TForbidRule* tmp_add_class_30;
  TMXBuilder* tmp_add_class_31;
  TRXReader* tmp_add_class_32;
  TSXReader* tmp_add_class_33;
  TaggerData* tmp_add_class_34;
  TaggerDataHMM* tmp_add_class_35;
  TaggerDataLSW* tmp_add_class_36;
  TaggerDataPercepCoarseTags* tmp_add_class_37;
  TaggerWord* tmp_add_class_38;
  TransExe* tmp_add_class_39;
  Transducer* tmp_add_class_40;
  Transfer* tmp_add_class_41;
  TransferBase* tmp_add_class_42;
  TransferData* tmp_add_class_43;
  TransferInstr* tmp_add_class_44;
  TransferMult* tmp_add_class_45;
  TransferToken* tmp_add_class_46;
  TransferWord* tmp_add_class_47;
  UFieldPosition* tmp_add_class_48;
  UParseError* tmp_add_class_49;
  UReplaceableCallbacks* tmp_add_class_50;
  USerializedSet* tmp_add_class_51;
  UText* tmp_add_class_52;
  UTextFuncs* tmp_add_class_53;
  UTransPosition* tmp_add_class_54;
  XMLParseUtil* tmp_add_class_55;
  XMLReader* tmp_add_class_56;
  _G_fpos64_t* tmp_add_class_57;
  _G_fpos_t* tmp_add_class_58;
  _IO_cookie_io_functions_t* tmp_add_class_59;
  _uconv_t* tmp_add_class_60;
  _xlinkHandler* tmp_add_class_61;
  _xmlAttr* tmp_add_class_62;
  _xmlAttribute* tmp_add_class_63;
  _xmlBuffer* tmp_add_class_64;
  _xmlCharEncodingHandler* tmp_add_class_65;
  _xmlDOMWrapCtxt* tmp_add_class_66;
  _xmlDoc* tmp_add_class_67;
  _xmlDtd* tmp_add_class_68;
  _xmlElement* tmp_add_class_69;
  _xmlElementContent* tmp_add_class_70;
  _xmlEntity* tmp_add_class_71;
  _xmlEnumeration* tmp_add_class_72;
  _xmlError* tmp_add_class_73;
  _xmlGlobalState* tmp_add_class_74;
  _xmlID* tmp_add_class_75;
  _xmlNode* tmp_add_class_76;
  _xmlNotation* tmp_add_class_77;
  _xmlNs* tmp_add_class_78;
  _xmlOutputBuffer* tmp_add_class_79;
  _xmlParserCtxt* tmp_add_class_80;
  _xmlParserInput* tmp_add_class_81;
  _xmlParserInputBuffer* tmp_add_class_82;
  _xmlParserNodeInfo* tmp_add_class_83;
  _xmlParserNodeInfoSeq* tmp_add_class_84;
  _xmlRef* tmp_add_class_85;
  _xmlSAXHandler* tmp_add_class_86;
  _xmlSAXHandlerV1* tmp_add_class_87;
  _xmlSAXLocator* tmp_add_class_88;
  _xmlValidCtxt* tmp_add_class_89;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/MvePT7COyO/dump1.h"  -I/usr/include/libxml2

