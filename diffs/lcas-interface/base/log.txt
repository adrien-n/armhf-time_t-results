Temporary header file '/tmp/LOz4zN8ZZo/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/lcas/lcas.h"
  #include "/usr/include/lcas/lcas_defines.h"
  #include "/usr/include/lcas/lcas_log.h"
  #include "/usr/include/lcas/lcas_modules.h"
  #include "/usr/include/lcas/lcas_pem.h"
  #include "/usr/include/lcas/lcas_types.h"
  #include "/usr/include/lcas/lcas_utils.h"

  // add classes
  ASN1_ENCODING_st* tmp_add_class_0;
  BIO_sock_info_u* tmp_add_class_1;
  BIT_STRING_BITNAME_st* tmp_add_class_2;
  Netscape_certificate_sequence* tmp_add_class_3;
  Netscape_spkac_st* tmp_add_class_4;
  Netscape_spki_st* tmp_add_class_5;
  PBE2PARAM_st* tmp_add_class_6;
  PBEPARAM_st* tmp_add_class_7;
  PBKDF2PARAM_st* tmp_add_class_8;
  PKCS7_CTX_st* tmp_add_class_9;
  SCRYPT_PARAMS_st* tmp_add_class_10;
  SHA256state_st* tmp_add_class_11;
  SHA512state_st* tmp_add_class_12;
  SHAstate_st* tmp_add_class_13;
  X509_algor_st* tmp_add_class_14;
  X509_info_st* tmp_add_class_15;
  X509_val_st* tmp_add_class_16;
  _G_fpos64_t* tmp_add_class_17;
  _G_fpos_t* tmp_add_class_18;
  _IO_cookie_io_functions_t* tmp_add_class_19;
  addrinfo* tmp_add_class_20;
  asn1_string_st* tmp_add_class_21;
  asn1_string_table_st* tmp_add_class_22;
  asn1_type_st* tmp_add_class_23;
  buf_mem_st* tmp_add_class_24;
  cmsghdr* tmp_add_class_25;
  conf_method_st* tmp_add_class_26;
  conf_st* tmp_add_class_27;
  crypto_ex_data_st* tmp_add_class_28;
  crypto_threadid_st* tmp_add_class_29;
  evp_cipher_info_st* tmp_add_class_30;
  f_owner_ex* tmp_add_class_31;
  file_handle* tmp_add_class_32;
  flock64* tmp_add_class_33;
  gaicb* tmp_add_class_34;
  globus_args_option_descriptor_s* tmp_add_class_35;
  globus_args_option_instance_s* tmp_add_class_36;
  globus_barrier_s* tmp_add_class_37;
  globus_list* tmp_add_class_38;
  globus_logging_module_s* tmp_add_class_39;
  globus_module_descriptor_s* tmp_add_class_40;
  globus_object_s* tmp_add_class_41;
  globus_object_type_s* tmp_add_class_42;
  globus_options_entry_s* tmp_add_class_43;
  globus_priority_q_s* tmp_add_class_44;
  globus_state_extension_handle_s* tmp_add_class_45;
  globus_validate_int_parms_s* tmp_add_class_46;
  group_filter* tmp_add_class_47;
  group_req* tmp_add_class_48;
  group_source_req* tmp_add_class_49;
  gss_OID_desc_struct* tmp_add_class_50;
  gss_OID_set_desc_struct* tmp_add_class_51;
  gss_buffer_desc_struct* tmp_add_class_52;
  gss_buffer_set_desc_struct* tmp_add_class_53;
  gss_channel_bindings_struct* tmp_add_class_54;
  hostent* tmp_add_class_55;
  in6_addr* tmp_add_class_56;
  in6_pktinfo* tmp_add_class_57;
  in_addr* tmp_add_class_58;
  in_pktinfo* tmp_add_class_59;
  iovec* tmp_add_class_60;
  ip6_mtuinfo* tmp_add_class_61;
  ip_mreq* tmp_add_class_62;
  ip_mreq_source* tmp_add_class_63;
  ip_mreqn* tmp_add_class_64;
  ip_msfilter* tmp_add_class_65;
  ip_opts* tmp_add_class_66;
  ipv6_mreq* tmp_add_class_67;
  itimerval* tmp_add_class_68;
  lcas_cred_id_s* tmp_add_class_69;
  lhash_st_CONF_VALUE* tmp_add_class_70;
  lhash_st_OPENSSL_CSTRING* tmp_add_class_71;
  lhash_st_OPENSSL_STRING* tmp_add_class_72;
  linger* tmp_add_class_73;
  mmsghdr* tmp_add_class_74;
  msghdr* tmp_add_class_75;
  netent* tmp_add_class_76;
  obj_name_st* tmp_add_class_77;
  osockaddr* tmp_add_class_78;
  ossl_algorithm_st* tmp_add_class_79;
  ossl_dispatch_st* tmp_add_class_80;
  ossl_item_st* tmp_add_class_81;
  ossl_param_st* tmp_add_class_82;
  passwd* tmp_add_class_83;
  pkcs7_digest_st* tmp_add_class_84;
  pkcs7_enc_content_st* tmp_add_class_85;
  pkcs7_encrypted_st* tmp_add_class_86;
  pkcs7_enveloped_st* tmp_add_class_87;
  pkcs7_issuer_and_serial_st* tmp_add_class_88;
  pkcs7_recip_info_st* tmp_add_class_89;
  pkcs7_signed_st* tmp_add_class_90;
  pkcs7_signedandenveloped_st* tmp_add_class_91;
  pkcs7_signer_info_st* tmp_add_class_92;
  pkcs7_st* tmp_add_class_93;
  private_key_st* tmp_add_class_94;
  protoent* tmp_add_class_95;
  rpcent* tmp_add_class_96;
  rsa_oaep_params_st* tmp_add_class_97;
  rsa_pss_params_st* tmp_add_class_98;
  servent* tmp_add_class_99;
  sockaddr* tmp_add_class_100;
  sockaddr_in* tmp_add_class_101;
  sockaddr_in6* tmp_add_class_102;
  sockaddr_storage* tmp_add_class_103;
  statx_timestamp* tmp_add_class_104;
  ucontext_t* tmp_add_class_105;
  ucred* tmp_add_class_106;
  x509_trust_st* tmp_add_class_107;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/LOz4zN8ZZo/dump1.h"  -I/usr/include/globus

