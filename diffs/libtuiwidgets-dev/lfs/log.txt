Temporary header file '/tmp/Lp86J9su55/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/Tui/Misc/AbstractTableModelTrackBy.h"
  #include "/usr/include/Tui/Misc/SurrogateEscape.h"
  #include "/usr/include/Tui/ZBasicDefaultWidgetManager.h"
  #include "/usr/include/Tui/ZBasicWindowFacet.h"
  #include "/usr/include/Tui/ZButton.h"
  #include "/usr/include/Tui/ZCheckBox.h"
  #include "/usr/include/Tui/ZColor.h"
  #include "/usr/include/Tui/ZCommandManager.h"
  #include "/usr/include/Tui/ZCommandNotifier.h"
  #include "/usr/include/Tui/ZCommon.h"
  #include "/usr/include/Tui/ZDefaultWidgetManager.h"
  #include "/usr/include/Tui/ZDialog.h"
  #include "/usr/include/Tui/ZEvent.h"
  #include "/usr/include/Tui/ZFormatRange.h"
  #include "/usr/include/Tui/ZHBoxLayout.h"
  #include "/usr/include/Tui/ZImage.h"
  #include "/usr/include/Tui/ZInputBox.h"
  #include "/usr/include/Tui/ZLabel.h"
  #include "/usr/include/Tui/ZLayout.h"
  #include "/usr/include/Tui/ZLayoutItem.h"
  #include "/usr/include/Tui/ZListView.h"
  #include "/usr/include/Tui/ZMenu.h"
  #include "/usr/include/Tui/ZMenuItem.h"
  #include "/usr/include/Tui/ZMenubar.h"
  #include "/usr/include/Tui/ZMoFunc_p.h"
  #include "/usr/include/Tui/ZPainter.h"
  #include "/usr/include/Tui/ZPalette.h"
  #include "/usr/include/Tui/ZRadioButton.h"
  #include "/usr/include/Tui/ZRoot.h"
  #include "/usr/include/Tui/ZShortcut.h"
  #include "/usr/include/Tui/ZSimpleFileLogger.h"
  #include "/usr/include/Tui/ZSimpleStringLogger.h"
  #include "/usr/include/Tui/ZStyledTextLine.h"
  #include "/usr/include/Tui/ZSymbol.h"
  #include "/usr/include/Tui/ZTerminal.h"
  #include "/usr/include/Tui/ZTest.h"
  #include "/usr/include/Tui/ZTextLayout.h"
  #include "/usr/include/Tui/ZTextLine.h"
  #include "/usr/include/Tui/ZTextMetrics.h"
  #include "/usr/include/Tui/ZTextOption.h"
  #include "/usr/include/Tui/ZTextStyle.h"
  #include "/usr/include/Tui/ZVBoxLayout.h"
  #include "/usr/include/Tui/ZValuePtr.h"
  #include "/usr/include/Tui/ZWidget.h"
  #include "/usr/include/Tui/ZWindow.h"
  #include "/usr/include/Tui/ZWindowContainer.h"
  #include "/usr/include/Tui/ZWindowFacet.h"
  #include "/usr/include/Tui/ZWindowLayout.h"
  #include "/usr/include/Tui/tuiwidgets_internal.h"

  // add namespaces
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_1;}
  QAlgorithmsPrivate::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace Qt{typedef int tmp_add_type_2;}
  Qt::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_3;}
  QtGlobalStatic::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_4;}
  QtMetaTypePrivate::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_5;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_6;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_7;}
  QtPrivate::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_8;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_9;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_10;}
  QtSharedPointer::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace Tui{typedef int tmp_add_type_11;}
  Tui::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace Tui{namespace v0{typedef int tmp_add_type_12;}}
  Tui::v0::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace Tui{namespace v0{namespace Colors{typedef int tmp_add_type_13;}}}
  Tui::v0::Colors::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace Tui{namespace v0{namespace Misc{typedef int tmp_add_type_14;}}}
  Tui::v0::Misc::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace Tui{namespace v0{namespace Misc{namespace SurrogateEscape{typedef int tmp_add_type_15;}}}}
  Tui::v0::Misc::SurrogateEscape::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace Tui{namespace v0{namespace Private{typedef int tmp_add_type_16;}}}
  Tui::v0::Private::tmp_add_type_16 tmp_add_func_16(){return 0;};
  namespace Tui{namespace v0{namespace ZEventType{typedef int tmp_add_type_17;}}}
  Tui::v0::ZEventType::tmp_add_type_17 tmp_add_func_17(){return 0;};
  namespace Tui{namespace v0{namespace ZSimpleFileLogger{typedef int tmp_add_type_18;}}}
  Tui::v0::ZSimpleFileLogger::tmp_add_type_18 tmp_add_func_18(){return 0;};
  namespace Tui{namespace v0{namespace ZSimpleStringLogger{typedef int tmp_add_type_19;}}}
  Tui::v0::ZSimpleStringLogger::tmp_add_type_19 tmp_add_func_19(){return 0;};
  namespace Tui{namespace v0{namespace ZTest{typedef int tmp_add_type_20;}}}
  Tui::v0::ZTest::tmp_add_type_20 tmp_add_func_20(){return 0;};

  // add classes
  QAbstractItemModel* tmp_add_class_0;
  QAbstractListModel* tmp_add_class_1;
  QAbstractTableModel* tmp_add_class_2;
  QArrayData* tmp_add_class_3;
  QAssociativeIterable* tmp_add_class_4;
  QAtomicInt* tmp_add_class_5;
  QByteArray* tmp_add_class_6;
  QByteArrayDataPtr* tmp_add_class_7;
  QByteRef* tmp_add_class_8;
  QChar* tmp_add_class_9;
  QCharRef* tmp_add_class_10;
  QChildEvent* tmp_add_class_11;
  QDeferredDeleteEvent* tmp_add_class_12;
  QDynamicPropertyChangeEvent* tmp_add_class_13;
  QEvent* tmp_add_class_14;
  QFlag* tmp_add_class_15;
  QGenericArgument* tmp_add_class_16;
  QGenericReturnArgument* tmp_add_class_17;
  QHashData* tmp_add_class_18;
  QHashDummyValue* tmp_add_class_19;
  QIncompatibleFlag* tmp_add_class_20;
  QInternal* tmp_add_class_21;
  QItemSelection* tmp_add_class_22;
  QItemSelectionModel* tmp_add_class_23;
  QItemSelectionRange* tmp_add_class_24;
  QLatin1Char* tmp_add_class_25;
  QLatin1String* tmp_add_class_26;
  QListData* tmp_add_class_27;
  QMapDataBase* tmp_add_class_28;
  QMapNodeBase* tmp_add_class_29;
  QMargins* tmp_add_class_30;
  QMarginsF* tmp_add_class_31;
  QMessageLogContext* tmp_add_class_32;
  QMessageLogger* tmp_add_class_33;
  QMetaObject* tmp_add_class_34;
  QMetaType* tmp_add_class_35;
  QModelIndex* tmp_add_class_36;
  QObject* tmp_add_class_37;
  QObjectData* tmp_add_class_38;
  QObjectUserData* tmp_add_class_39;
  QPersistentModelIndex* tmp_add_class_40;
  QPoint* tmp_add_class_41;
  QPointF* tmp_add_class_42;
  QRect* tmp_add_class_43;
  QRectF* tmp_add_class_44;
  QRegExp* tmp_add_class_45;
  QScopedPointerPodDeleter* tmp_add_class_46;
  QSequentialIterable* tmp_add_class_47;
  QSharedData* tmp_add_class_48;
  QSignalBlocker* tmp_add_class_49;
  QSize* tmp_add_class_50;
  QSizeF* tmp_add_class_51;
  QString* tmp_add_class_52;
  QStringDataPtr* tmp_add_class_53;
  QStringList* tmp_add_class_54;
  QStringMatcher* tmp_add_class_55;
  QStringRef* tmp_add_class_56;
  QStringView* tmp_add_class_57;
  QSysInfo* tmp_add_class_58;
  QTimerEvent* tmp_add_class_59;
  QVariant* tmp_add_class_60;
  QVariantComparisonHelper* tmp_add_class_61;
  _G_fpos64_t* tmp_add_class_62;
  _G_fpos_t* tmp_add_class_63;
  _IO_cookie_io_functions_t* tmp_add_class_64;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/Lp86J9su55/dump1.h"  -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

