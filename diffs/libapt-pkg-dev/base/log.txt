Temporary header file '/tmp/5pNj7GwZTH/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/apt-pkg/acquire-item.h"
  #include "/usr/include/apt-pkg/acquire-method.h"
  #include "/usr/include/apt-pkg/acquire-worker.h"
  #include "/usr/include/apt-pkg/acquire.h"
  #include "/usr/include/apt-pkg/algorithms.h"
  #include "/usr/include/apt-pkg/aptconfiguration.h"
  #include "/usr/include/apt-pkg/arfile.h"
  #include "/usr/include/apt-pkg/cachefile.h"
  #include "/usr/include/apt-pkg/cachefilter.h"
  #include "/usr/include/apt-pkg/cacheiterators.h"
  #include "/usr/include/apt-pkg/cacheset.h"
  #include "/usr/include/apt-pkg/cdrom.h"
  #include "/usr/include/apt-pkg/cdromutl.h"
  #include "/usr/include/apt-pkg/clean.h"
  #include "/usr/include/apt-pkg/cmndline.h"
  #include "/usr/include/apt-pkg/configuration.h"
  #include "/usr/include/apt-pkg/debfile.h"
  #include "/usr/include/apt-pkg/debindexfile.h"
  #include "/usr/include/apt-pkg/deblistparser.h"
  #include "/usr/include/apt-pkg/debmetaindex.h"
  #include "/usr/include/apt-pkg/debrecords.h"
  #include "/usr/include/apt-pkg/debsystem.h"
  #include "/usr/include/apt-pkg/debversion.h"
  #include "/usr/include/apt-pkg/depcache.h"
  #include "/usr/include/apt-pkg/dirstream.h"
  #include "/usr/include/apt-pkg/dpkgpm.h"
  #include "/usr/include/apt-pkg/edsp.h"
  #include "/usr/include/apt-pkg/edspindexfile.h"
  #include "/usr/include/apt-pkg/edsplistparser.h"
  #include "/usr/include/apt-pkg/edspsystem.h"
  #include "/usr/include/apt-pkg/error.h"
  #include "/usr/include/apt-pkg/extracttar.h"
  #include "/usr/include/apt-pkg/fileutl.h"
  #include "/usr/include/apt-pkg/gpgv.h"
  #include "/usr/include/apt-pkg/hashes.h"
  #include "/usr/include/apt-pkg/indexcopy.h"
  #include "/usr/include/apt-pkg/indexfile.h"
  #include "/usr/include/apt-pkg/init.h"
  #include "/usr/include/apt-pkg/install-progress.h"
  #include "/usr/include/apt-pkg/macros.h"
  #include "/usr/include/apt-pkg/metaindex.h"
  #include "/usr/include/apt-pkg/mmap.h"
  #include "/usr/include/apt-pkg/netrc.h"
  #include "/usr/include/apt-pkg/orderlist.h"
  #include "/usr/include/apt-pkg/packagemanager.h"
  #include "/usr/include/apt-pkg/pkgcache.h"
  #include "/usr/include/apt-pkg/pkgcachegen.h"
  #include "/usr/include/apt-pkg/pkgrecords.h"
  #include "/usr/include/apt-pkg/pkgsystem.h"
  #include "/usr/include/apt-pkg/policy.h"
  #include "/usr/include/apt-pkg/prettyprinters.h"
  #include "/usr/include/apt-pkg/progress.h"
  #include "/usr/include/apt-pkg/proxy.h"
  #include "/usr/include/apt-pkg/sourcelist.h"
  #include "/usr/include/apt-pkg/srcrecords.h"
  #include "/usr/include/apt-pkg/srvrec.h"
  #include "/usr/include/apt-pkg/statechanges.h"
  #include "/usr/include/apt-pkg/string_view.h"
  #include "/usr/include/apt-pkg/strutl.h"
  #include "/usr/include/apt-pkg/tagfile.h"
  #include "/usr/include/apt-pkg/update.h"
  #include "/usr/include/apt-pkg/upgrade.h"
  #include "/usr/include/apt-pkg/version.h"
  #include "/usr/include/apt-pkg/versionmatch.h"
  #include "/usr/include/apt-pkg/weakptr.h"

  // add namespaces
  namespace APT{typedef int tmp_add_type_1;}
  APT::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace APT{namespace CacheFilter{typedef int tmp_add_type_2;}}
  APT::CacheFilter::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace APT{namespace Configuration{typedef int tmp_add_type_3;}}
  APT::Configuration::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace APT{namespace KernelAutoRemoveHelper{typedef int tmp_add_type_4;}}
  APT::KernelAutoRemoveHelper::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace APT{namespace Progress{typedef int tmp_add_type_5;}}
  APT::Progress::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace APT{namespace String{typedef int tmp_add_type_6;}}
  APT::String::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace APT{namespace Upgrade{typedef int tmp_add_type_7;}}
  APT::Upgrade::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace EDSP{typedef int tmp_add_type_8;}
  EDSP::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace EDSP{namespace Request{typedef int tmp_add_type_9;}}
  EDSP::Request::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace EIPP{typedef int tmp_add_type_10;}
  EIPP::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace EIPP{namespace Request{typedef int tmp_add_type_11;}}
  EIPP::Request::tmp_add_type_11 tmp_add_func_11(){return 0;};

  // add classes
  ARArchive* tmp_add_class_0;
  CdromDevice* tmp_add_class_1;
  CommandLine* tmp_add_class_2;
  Configuration* tmp_add_class_3;
  DiffInfo* tmp_add_class_4;
  DynamicMMap* tmp_add_class_5;
  ExtractTar* tmp_add_class_6;
  FileFd* tmp_add_class_7;
  GlobalError* tmp_add_class_8;
  HashString* tmp_add_class_9;
  HashStringList* tmp_add_class_10;
  Hashes* tmp_add_class_11;
  IndexCopy* tmp_add_class_12;
  IndexTarget* tmp_add_class_13;
  MMap* tmp_add_class_14;
  OpProgress* tmp_add_class_15;
  OpTextProgress* tmp_add_class_16;
  PackageCopy* tmp_add_class_17;
  RxChoiceList* tmp_add_class_18;
  SigVerify* tmp_add_class_19;
  SourceCopy* tmp_add_class_20;
  SrvRec* tmp_add_class_21;
  TranslationsCopy* tmp_add_class_22;
  URI* tmp_add_class_23;
  WeakPointable* tmp_add_class_24;
  _G_fpos64_t* tmp_add_class_25;
  _G_fpos_t* tmp_add_class_26;
  _IO_cookie_io_functions_t* tmp_add_class_27;
  debDebFile* tmp_add_class_28;
  debDebFileParser* tmp_add_class_29;
  debDebFileRecordParser* tmp_add_class_30;
  debDebPkgFileIndex* tmp_add_class_31;
  debDebianSourceDirIndex* tmp_add_class_32;
  debDscFileIndex* tmp_add_class_33;
  debListParser* tmp_add_class_34;
  debPackagesIndex* tmp_add_class_35;
  debRecordParser* tmp_add_class_36;
  debRecordParserBase* tmp_add_class_37;
  debReleaseIndex* tmp_add_class_38;
  debSourcesIndex* tmp_add_class_39;
  debStatusIndex* tmp_add_class_40;
  debStatusListParser* tmp_add_class_41;
  debStringPackageIndex* tmp_add_class_42;
  debSystem* tmp_add_class_43;
  debTranslationsIndex* tmp_add_class_44;
  debTranslationsParser* tmp_add_class_45;
  debVersioningSystem* tmp_add_class_46;
  edspIndex* tmp_add_class_47;
  edspLikeIndex* tmp_add_class_48;
  edspLikeListParser* tmp_add_class_49;
  edspLikeSystem* tmp_add_class_50;
  edspListParser* tmp_add_class_51;
  edspSystem* tmp_add_class_52;
  eippIndex* tmp_add_class_53;
  eippListParser* tmp_add_class_54;
  eippSystem* tmp_add_class_55;
  itimerval* tmp_add_class_56;
  metaIndex* tmp_add_class_57;
  ns_tcp_tsig_state* tmp_add_class_58;
  ns_tsig_key* tmp_add_class_59;
  pkgAcqArchive* tmp_add_class_60;
  pkgAcqAuxFile* tmp_add_class_61;
  pkgAcqBaseIndex* tmp_add_class_62;
  pkgAcqChangelog* tmp_add_class_63;
  pkgAcqDiffIndex* tmp_add_class_64;
  pkgAcqFile* tmp_add_class_65;
  pkgAcqIndex* tmp_add_class_66;
  pkgAcqIndexDiffs* tmp_add_class_67;
  pkgAcqIndexMergeDiffs* tmp_add_class_68;
  pkgAcqMetaBase* tmp_add_class_69;
  pkgAcqMetaClearSig* tmp_add_class_70;
  pkgAcqMetaIndex* tmp_add_class_71;
  pkgAcqMetaSig* tmp_add_class_72;
  pkgAcqMethod* tmp_add_class_73;
  pkgAcqTransactionItem* tmp_add_class_74;
  pkgAcquire* tmp_add_class_75;
  pkgAcquireStatus* tmp_add_class_76;
  pkgArchiveCleaner* tmp_add_class_77;
  pkgCache* tmp_add_class_78;
  pkgCacheFile* tmp_add_class_79;
  pkgCacheGenerator* tmp_add_class_80;
  pkgCacheListParser* tmp_add_class_81;
  pkgCdrom* tmp_add_class_82;
  pkgCdromStatus* tmp_add_class_83;
  pkgDPkgPM* tmp_add_class_84;
  pkgDebianIndexFile* tmp_add_class_85;
  pkgDebianIndexRealFile* tmp_add_class_86;
  pkgDebianIndexTargetFile* tmp_add_class_87;
  pkgDepCache* tmp_add_class_88;
  pkgDirStream* tmp_add_class_89;
  pkgIndexFile* tmp_add_class_90;
  pkgOrderList* tmp_add_class_91;
  pkgPackageManager* tmp_add_class_92;
  pkgPolicy* tmp_add_class_93;
  pkgProblemResolver* tmp_add_class_94;
  pkgRecords* tmp_add_class_95;
  pkgSimulate* tmp_add_class_96;
  pkgSourceList* tmp_add_class_97;
  pkgSrcRecords* tmp_add_class_98;
  pkgSystem* tmp_add_class_99;
  pkgTagFile* tmp_add_class_100;
  pkgTagSection* tmp_add_class_101;
  pkgUdevCdromDevices* tmp_add_class_102;
  pkgVersionMatch* tmp_add_class_103;
  pkgVersioningSystem* tmp_add_class_104;
  re_pattern_buffer* tmp_add_class_105;
  re_registers* tmp_add_class_106;
  statx_timestamp* tmp_add_class_107;
  ucontext_t* tmp_add_class_108;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/5pNj7GwZTH/dump1.h"

