Temporary header file '/tmp/hR0pI0rzj1/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/bignum.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/bits.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/bits_inline.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/char_euc_jp.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/char_none.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/char_sjis.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/char_utf_8.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/charset.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/class.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/code.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/collection.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/compare.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/config.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/config_threads.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/endian.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/exception.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/extend.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/extern.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/float.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/gloc.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/hash.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/int64.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/load.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/module.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/number.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/parameter.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/port.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/prof.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/pthread.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/reader.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/regexp.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/scmconst.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/static.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/string.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/symbol.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/system.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/treemap.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/uvector.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/vector.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/vm.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/vminsn.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/weak.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gauche/writer.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_allocator.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_config_macros.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_cpp.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_inline.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_mark.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_pthread_redirects.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_tiny_fl.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_typed.h"
  #include "/usr/lib/gauche-0.97/0.9.10/include/gc_version.h"

  // add classes
  GC_false_type* tmp_add_class_0;
  GC_ms_entry* tmp_add_class_1;
  GC_prof_stats_s* tmp_add_class_2;
  GC_stack_base* tmp_add_class_3;
  GC_true_type* tmp_add_class_4;
  ScmAutoloadRec* tmp_add_class_5;
  ScmBignumRec* tmp_add_class_6;
  ScmBitvectorRec* tmp_add_class_7;
  ScmBoxRec* tmp_add_class_8;
  ScmCStackRec* tmp_add_class_9;
  ScmCharSetRec* tmp_add_class_10;
  ScmClassRec* tmp_add_class_11;
  ScmClassStaticSlotSpecRec* tmp_add_class_12;
  ScmClosureRec* tmp_add_class_13;
  ScmComparatorRec* tmp_add_class_14;
  ScmCompileErrorMixinRec* tmp_add_class_15;
  ScmCompiledCodeRec* tmp_add_class_16;
  ScmCompnumRec* tmp_add_class_17;
  ScmCompoundConditionRec* tmp_add_class_18;
  ScmContFrameRec* tmp_add_class_19;
  ScmDStringChainRec* tmp_add_class_20;
  ScmDStringChunkRec* tmp_add_class_21;
  ScmDStringRec* tmp_add_class_22;
  ScmDictEntryRec* tmp_add_class_23;
  ScmEnvFrameRec* tmp_add_class_24;
  ScmEvalPacketRec* tmp_add_class_25;
  ScmExtendedPairRec* tmp_add_class_26;
  ScmFilenameErrorMixinRec* tmp_add_class_27;
  ScmFlonumRec* tmp_add_class_28;
  ScmForeignPointerRec* tmp_add_class_29;
  ScmGenericRec* tmp_add_class_30;
  ScmGlocRec* tmp_add_class_31;
  ScmHashCoreRec* tmp_add_class_32;
  ScmHashEntryRec* tmp_add_class_33;
  ScmHashIterRec* tmp_add_class_34;
  ScmHashTableRec* tmp_add_class_35;
  ScmHeaderRec* tmp_add_class_36;
  ScmInstanceRec* tmp_add_class_37;
  ScmLoadConditionMixinRec* tmp_add_class_38;
  ScmLoadPacketRec* tmp_add_class_39;
  ScmMVBoxRec* tmp_add_class_40;
  ScmMessageConditionRec* tmp_add_class_41;
  ScmMethodRec* tmp_add_class_42;
  ScmModuleRec* tmp_add_class_43;
  ScmNextMethodRec* tmp_add_class_44;
  ScmNumberFormatRec* tmp_add_class_45;
  ScmPairRec* tmp_add_class_46;
  ScmParameterLocRec* tmp_add_class_47;
  ScmPortBufferRec* tmp_add_class_48;
  ScmPortErrorRec* tmp_add_class_49;
  ScmPortInputStringRec* tmp_add_class_50;
  ScmPortRec* tmp_add_class_51;
  ScmPortVTableRec* tmp_add_class_52;
  ScmProcedureRec* tmp_add_class_53;
  ScmProfCountRec* tmp_add_class_54;
  ScmProfSampleRec* tmp_add_class_55;
  ScmPromiseRec* tmp_add_class_56;
  ScmRatnumRec* tmp_add_class_57;
  ScmReadErrorRec* tmp_add_class_58;
  ScmReadReferenceRec* tmp_add_class_59;
  ScmRegMatchRec* tmp_add_class_60;
  ScmRegexpRec* tmp_add_class_61;
  ScmSignalQueueRec* tmp_add_class_62;
  ScmSlotAccessorRec* tmp_add_class_63;
  ScmStringBodyRec* tmp_add_class_64;
  ScmStringRec* tmp_add_class_65;
  ScmSubrRec* tmp_add_class_66;
  ScmSymbolRec* tmp_add_class_67;
  ScmSyntacticClosureRec* tmp_add_class_68;
  ScmSysFdsetRec* tmp_add_class_69;
  ScmSysGroupRec* tmp_add_class_70;
  ScmSysPasswdRec* tmp_add_class_71;
  ScmSysSigsetRec* tmp_add_class_72;
  ScmSysStatRec* tmp_add_class_73;
  ScmSysTmRec* tmp_add_class_74;
  ScmSystemErrorRec* tmp_add_class_75;
  ScmThreadExceptionRec* tmp_add_class_76;
  ScmTimeRec* tmp_add_class_77;
  ScmTreeCoreRec* tmp_add_class_78;
  ScmTreeIterRec* tmp_add_class_79;
  ScmTreeMapRec* tmp_add_class_80;
  ScmUVectorRec* tmp_add_class_81;
  ScmUnhandledSignalErrorRec* tmp_add_class_82;
  ScmVMProfilerRec* tmp_add_class_83;
  ScmVMRec* tmp_add_class_84;
  ScmVMStatRec* tmp_add_class_85;
  ScmVectorRec* tmp_add_class_86;
  ScmWeakHashIterRec* tmp_add_class_87;
  ScmWeakHashTableRec* tmp_add_class_88;
  ScmWeakVectorRec* tmp_add_class_89;
  _G_fpos64_t* tmp_add_class_90;
  _G_fpos_t* tmp_add_class_91;
  _IO_cookie_io_functions_t* tmp_add_class_92;
  dl_find_object* tmp_add_class_93;
  gc* tmp_add_class_94;
  gc_cleanup* tmp_add_class_95;
  itimerval* tmp_add_class_96;
  statx_timestamp* tmp_add_class_97;
  ucontext_t* tmp_add_class_98;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/hR0pI0rzj1/dump1.h"  -I/usr/lib/gauche-0.97/0.9.10/include

