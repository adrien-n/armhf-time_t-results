Temporary header file '/tmp/wkLEs8fGyW/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/coin/CoinHelperFunctions.hpp"
  #include "/usr/include/coin/ClpSimplex.hpp"
  #include "/usr/include/coin/OsiSolverInterface.hpp"
  #include "/usr/include/coin/OsiClpSolverInterface.hpp"
  #include "/usr/include/coin/CbcOrClpParam.hpp"
  #include "/usr/include/coin/ClpCholeskyBase.hpp"
  #include "/usr/include/coin/ClpCholeskyDense.hpp"
  #include "/usr/include/coin/ClpCholeskyPardiso.hpp"
  #include "/usr/include/coin/ClpConfig.h"
  #include "/usr/include/coin/ClpConstraint.hpp"
  #include "/usr/include/coin/ClpConstraintLinear.hpp"
  #include "/usr/include/coin/ClpConstraintQuadratic.hpp"
  #include "/usr/include/coin/ClpDualRowDantzig.hpp"
  #include "/usr/include/coin/ClpDualRowPivot.hpp"
  #include "/usr/include/coin/ClpDualRowSteepest.hpp"
  #include "/usr/include/coin/ClpDummyMatrix.hpp"
  #include "/usr/include/coin/ClpDynamicExampleMatrix.hpp"
  #include "/usr/include/coin/ClpDynamicMatrix.hpp"
  #include "/usr/include/coin/ClpEventHandler.hpp"
  #include "/usr/include/coin/ClpFactorization.hpp"
  #include "/usr/include/coin/ClpGubDynamicMatrix.hpp"
  #include "/usr/include/coin/ClpGubMatrix.hpp"
  #include "/usr/include/coin/ClpInterior.hpp"
  #include "/usr/include/coin/ClpLinearObjective.hpp"
  #include "/usr/include/coin/ClpMatrixBase.hpp"
  #include "/usr/include/coin/ClpMessage.hpp"
  #include "/usr/include/coin/ClpModel.hpp"
  #include "/usr/include/coin/ClpNetworkMatrix.hpp"
  #include "/usr/include/coin/ClpNode.hpp"
  #include "/usr/include/coin/ClpNonLinearCost.hpp"
  #include "/usr/include/coin/ClpObjective.hpp"
  #include "/usr/include/coin/ClpPEDualRowDantzig.hpp"
  #include "/usr/include/coin/ClpPEDualRowSteepest.hpp"
  #include "/usr/include/coin/ClpPEPrimalColumnDantzig.hpp"
  #include "/usr/include/coin/ClpPEPrimalColumnSteepest.hpp"
  #include "/usr/include/coin/ClpPESimplex.hpp"
  #include "/usr/include/coin/ClpPackedMatrix.hpp"
  #include "/usr/include/coin/ClpParameters.hpp"
  #include "/usr/include/coin/ClpPdcoBase.hpp"
  #include "/usr/include/coin/ClpPlusMinusOneMatrix.hpp"
  #include "/usr/include/coin/ClpPresolve.hpp"
  #include "/usr/include/coin/ClpPrimalColumnDantzig.hpp"
  #include "/usr/include/coin/ClpPrimalColumnPivot.hpp"
  #include "/usr/include/coin/ClpPrimalColumnSteepest.hpp"
  #include "/usr/include/coin/ClpQuadraticObjective.hpp"
  #include "/usr/include/coin/ClpSimplexDual.hpp"
  #include "/usr/include/coin/ClpSimplexNonlinear.hpp"
  #include "/usr/include/coin/ClpSimplexOther.hpp"
  #include "/usr/include/coin/ClpSimplexPrimal.hpp"
  #include "/usr/include/coin/ClpSolve.hpp"
  #include "/usr/include/coin/Clp_C_Interface.h"
  #include "/usr/include/coin/Idiot.hpp"

  // add classes
  CbcOrClpParam* tmp_add_class_0;
  ClpCholeskyBase* tmp_add_class_1;
  ClpCholeskyDense* tmp_add_class_2;
  ClpConstraint* tmp_add_class_3;
  ClpConstraintLinear* tmp_add_class_4;
  ClpConstraintQuadratic* tmp_add_class_5;
  ClpDataSave* tmp_add_class_6;
  ClpDisasterHandler* tmp_add_class_7;
  ClpDualRowDantzig* tmp_add_class_8;
  ClpDualRowPivot* tmp_add_class_9;
  ClpDualRowSteepest* tmp_add_class_10;
  ClpDummyMatrix* tmp_add_class_11;
  ClpDynamicExampleMatrix* tmp_add_class_12;
  ClpDynamicMatrix* tmp_add_class_13;
  ClpEventHandler* tmp_add_class_14;
  ClpFactorization* tmp_add_class_15;
  ClpGubDynamicMatrix* tmp_add_class_16;
  ClpGubMatrix* tmp_add_class_17;
  ClpHashValue* tmp_add_class_18;
  ClpInterior* tmp_add_class_19;
  ClpLinearObjective* tmp_add_class_20;
  ClpMatrixBase* tmp_add_class_21;
  ClpMessage* tmp_add_class_22;
  ClpModel* tmp_add_class_23;
  ClpNetworkMatrix* tmp_add_class_24;
  ClpNode* tmp_add_class_25;
  ClpNodeStuff* tmp_add_class_26;
  ClpNonLinearCost* tmp_add_class_27;
  ClpObjective* tmp_add_class_28;
  ClpPEDualRowDantzig* tmp_add_class_29;
  ClpPEDualRowSteepest* tmp_add_class_30;
  ClpPEPrimalColumnDantzig* tmp_add_class_31;
  ClpPEPrimalColumnSteepest* tmp_add_class_32;
  ClpPESimplex* tmp_add_class_33;
  ClpPackedMatrix* tmp_add_class_34;
  ClpPackedMatrix2* tmp_add_class_35;
  ClpPackedMatrix3* tmp_add_class_36;
  ClpPdcoBase* tmp_add_class_37;
  ClpPlusMinusOneMatrix* tmp_add_class_38;
  ClpPresolve* tmp_add_class_39;
  ClpPrimalColumnDantzig* tmp_add_class_40;
  ClpPrimalColumnPivot* tmp_add_class_41;
  ClpPrimalColumnSteepest* tmp_add_class_42;
  ClpQuadraticObjective* tmp_add_class_43;
  ClpSimplex* tmp_add_class_44;
  ClpSimplexDual* tmp_add_class_45;
  ClpSimplexNonlinear* tmp_add_class_46;
  ClpSimplexOther* tmp_add_class_47;
  ClpSimplexPrimal* tmp_add_class_48;
  ClpSimplexProgress* tmp_add_class_49;
  ClpSolve* tmp_add_class_50;
  CoinArbitraryArrayWithLength* tmp_add_class_51;
  CoinArrayWithLength* tmp_add_class_52;
  CoinBigIndexArrayWithLength* tmp_add_class_53;
  CoinDenseFactorization* tmp_add_class_54;
  CoinDoubleArrayWithLength* tmp_add_class_55;
  CoinError* tmp_add_class_56;
  CoinFactorization* tmp_add_class_57;
  CoinFactorizationDoubleArrayWithLength* tmp_add_class_58;
  CoinFactorizationLongDoubleArrayWithLength* tmp_add_class_59;
  CoinIndexedVector* tmp_add_class_60;
  CoinIntArrayWithLength* tmp_add_class_61;
  CoinMessage* tmp_add_class_62;
  CoinMessageHandler* tmp_add_class_63;
  CoinMessages* tmp_add_class_64;
  CoinOneMessage* tmp_add_class_65;
  CoinOtherFactorization* tmp_add_class_66;
  CoinPackedMatrix* tmp_add_class_67;
  CoinPackedVectorBase* tmp_add_class_68;
  CoinPartitionedVector* tmp_add_class_69;
  CoinPostsolveMatrix* tmp_add_class_70;
  CoinPrePostsolveMatrix* tmp_add_class_71;
  CoinPresolveAction* tmp_add_class_72;
  CoinPresolveMatrix* tmp_add_class_73;
  CoinShallowPackedVector* tmp_add_class_74;
  CoinThreadRandom* tmp_add_class_75;
  CoinTimer* tmp_add_class_76;
  CoinUnsignedIntArrayWithLength* tmp_add_class_77;
  CoinVoidStarArrayWithLength* tmp_add_class_78;
  CoinWarmStart* tmp_add_class_79;
  CoinWarmStartBasis* tmp_add_class_80;
  CoinWarmStartBasisDiff* tmp_add_class_81;
  CoinWarmStartDiff* tmp_add_class_82;
  Idiot* tmp_add_class_83;
  OsiClpDisasterHandler* tmp_add_class_84;
  OsiClpSolverInterface* tmp_add_class_85;
  OsiSolverInterface* tmp_add_class_86;
  _G_fpos64_t* tmp_add_class_87;
  _G_fpos_t* tmp_add_class_88;
  _IO_cookie_io_functions_t* tmp_add_class_89;
  itimerval* tmp_add_class_90;
  presolvehlink* tmp_add_class_91;
  rlimit64* tmp_add_class_92;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/wkLEs8fGyW/dump1.h"

