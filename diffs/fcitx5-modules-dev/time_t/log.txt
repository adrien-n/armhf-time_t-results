Temporary header file '/tmp/phU9rfNGSr/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/Fcitx5/Module/fcitx-module/clipboard/clipboard_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/dbus/dbus_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/emoji/emoji_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/notificationitem/notificationitem_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/notifications/notifications_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/quickphrase/quickphrase_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/spell/spell_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/testfrontend/testfrontend_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/testim/testim_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/unicode/unicode_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/wayland/wayland_public.h"
  #include "/usr/include/Fcitx5/Module/fcitx-module/xcb/xcb_public.h"

  // add namespaces
  namespace fcitx{typedef int tmp_add_type_1;}
  fcitx::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace fcitx{namespace dbus{typedef int tmp_add_type_2;}}
  fcitx::dbus::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace fcitx{namespace fs{typedef int tmp_add_type_3;}}
  fcitx::fs::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace fcitx{namespace IClipboard{typedef int tmp_add_type_4;}}
  fcitx::IClipboard::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace fcitx{namespace IDBusModule{typedef int tmp_add_type_5;}}
  fcitx::IDBusModule::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace fcitx{namespace IEmoji{typedef int tmp_add_type_6;}}
  fcitx::IEmoji::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace fcitx{namespace INotificationItem{typedef int tmp_add_type_7;}}
  fcitx::INotificationItem::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace fcitx{namespace INotifications{typedef int tmp_add_type_8;}}
  fcitx::INotifications::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace fcitx{namespace IQuickPhrase{typedef int tmp_add_type_9;}}
  fcitx::IQuickPhrase::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace fcitx{namespace ISpell{typedef int tmp_add_type_10;}}
  fcitx::ISpell::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace fcitx{namespace ITestFrontend{typedef int tmp_add_type_11;}}
  fcitx::ITestFrontend::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace fcitx{namespace ITestIM{typedef int tmp_add_type_12;}}
  fcitx::ITestIM::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace fcitx{namespace IUnicode{typedef int tmp_add_type_13;}}
  fcitx::IUnicode::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace fcitx{namespace IWaylandModule{typedef int tmp_add_type_14;}}
  fcitx::IWaylandModule::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace fcitx{namespace IXCBModule{typedef int tmp_add_type_15;}}
  fcitx::IXCBModule::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace fcitx{namespace stringutils{typedef int tmp_add_type_16;}}
  fcitx::stringutils::tmp_add_type_16 tmp_add_func_16(){return 0;};
  namespace fcitx{namespace stringutils{namespace details{typedef int tmp_add_type_17;}}}
  fcitx::stringutils::details::tmp_add_type_17 tmp_add_func_17(){return 0;};

  // add classes
  _G_fpos64_t* tmp_add_class_0;
  _G_fpos_t* tmp_add_class_1;
  _IO_cookie_io_functions_t* tmp_add_class_2;
  iovec* tmp_add_class_3;
  wl_argument* tmp_add_class_4;
  wl_array* tmp_add_class_5;
  wl_buffer_listener* tmp_add_class_6;
  wl_callback_listener* tmp_add_class_7;
  wl_data_device_listener* tmp_add_class_8;
  wl_data_offer_listener* tmp_add_class_9;
  wl_data_source_listener* tmp_add_class_10;
  wl_display_listener* tmp_add_class_11;
  wl_interface* tmp_add_class_12;
  wl_keyboard_listener* tmp_add_class_13;
  wl_list* tmp_add_class_14;
  wl_message* tmp_add_class_15;
  wl_output_listener* tmp_add_class_16;
  wl_pointer_listener* tmp_add_class_17;
  wl_registry_listener* tmp_add_class_18;
  wl_seat_listener* tmp_add_class_19;
  wl_shell_surface_listener* tmp_add_class_20;
  wl_shm_listener* tmp_add_class_21;
  wl_surface_listener* tmp_add_class_22;
  wl_touch_listener* tmp_add_class_23;
  xcb_alloc_color_cells_cookie_t* tmp_add_class_24;
  xcb_alloc_color_cells_reply_t* tmp_add_class_25;
  xcb_alloc_color_cells_request_t* tmp_add_class_26;
  xcb_alloc_color_cookie_t* tmp_add_class_27;
  xcb_alloc_color_planes_cookie_t* tmp_add_class_28;
  xcb_alloc_color_planes_reply_t* tmp_add_class_29;
  xcb_alloc_color_planes_request_t* tmp_add_class_30;
  xcb_alloc_color_reply_t* tmp_add_class_31;
  xcb_alloc_color_request_t* tmp_add_class_32;
  xcb_alloc_named_color_cookie_t* tmp_add_class_33;
  xcb_alloc_named_color_reply_t* tmp_add_class_34;
  xcb_alloc_named_color_request_t* tmp_add_class_35;
  xcb_allow_events_request_t* tmp_add_class_36;
  xcb_arc_iterator_t* tmp_add_class_37;
  xcb_arc_t* tmp_add_class_38;
  xcb_atom_iterator_t* tmp_add_class_39;
  xcb_auth_info_t* tmp_add_class_40;
  xcb_bell_request_t* tmp_add_class_41;
  xcb_bool32_iterator_t* tmp_add_class_42;
  xcb_button_iterator_t* tmp_add_class_43;
  xcb_button_press_event_t* tmp_add_class_44;
  xcb_change_active_pointer_grab_request_t* tmp_add_class_45;
  xcb_change_gc_request_t* tmp_add_class_46;
  xcb_change_gc_value_list_t* tmp_add_class_47;
  xcb_change_hosts_request_t* tmp_add_class_48;
  xcb_change_keyboard_control_request_t* tmp_add_class_49;
  xcb_change_keyboard_control_value_list_t* tmp_add_class_50;
  xcb_change_keyboard_mapping_request_t* tmp_add_class_51;
  xcb_change_pointer_control_request_t* tmp_add_class_52;
  xcb_change_property_request_t* tmp_add_class_53;
  xcb_change_save_set_request_t* tmp_add_class_54;
  xcb_change_window_attributes_request_t* tmp_add_class_55;
  xcb_change_window_attributes_value_list_t* tmp_add_class_56;
  xcb_char2b_iterator_t* tmp_add_class_57;
  xcb_char2b_t* tmp_add_class_58;
  xcb_charinfo_iterator_t* tmp_add_class_59;
  xcb_charinfo_t* tmp_add_class_60;
  xcb_circulate_notify_event_t* tmp_add_class_61;
  xcb_circulate_window_request_t* tmp_add_class_62;
  xcb_clear_area_request_t* tmp_add_class_63;
  xcb_client_message_data_iterator_t* tmp_add_class_64;
  xcb_client_message_data_t* tmp_add_class_65;
  xcb_client_message_event_t* tmp_add_class_66;
  xcb_close_font_request_t* tmp_add_class_67;
  xcb_coloritem_iterator_t* tmp_add_class_68;
  xcb_coloritem_t* tmp_add_class_69;
  xcb_colormap_iterator_t* tmp_add_class_70;
  xcb_colormap_notify_event_t* tmp_add_class_71;
  xcb_configure_notify_event_t* tmp_add_class_72;
  xcb_configure_request_event_t* tmp_add_class_73;
  xcb_configure_window_request_t* tmp_add_class_74;
  xcb_configure_window_value_list_t* tmp_add_class_75;
  xcb_convert_selection_request_t* tmp_add_class_76;
  xcb_copy_area_request_t* tmp_add_class_77;
  xcb_copy_colormap_and_free_request_t* tmp_add_class_78;
  xcb_copy_gc_request_t* tmp_add_class_79;
  xcb_copy_plane_request_t* tmp_add_class_80;
  xcb_create_colormap_request_t* tmp_add_class_81;
  xcb_create_cursor_request_t* tmp_add_class_82;
  xcb_create_gc_request_t* tmp_add_class_83;
  xcb_create_gc_value_list_t* tmp_add_class_84;
  xcb_create_glyph_cursor_request_t* tmp_add_class_85;
  xcb_create_notify_event_t* tmp_add_class_86;
  xcb_create_pixmap_request_t* tmp_add_class_87;
  xcb_create_window_request_t* tmp_add_class_88;
  xcb_create_window_value_list_t* tmp_add_class_89;
  xcb_cursor_iterator_t* tmp_add_class_90;
  xcb_delete_property_request_t* tmp_add_class_91;
  xcb_depth_iterator_t* tmp_add_class_92;
  xcb_depth_t* tmp_add_class_93;
  xcb_destroy_notify_event_t* tmp_add_class_94;
  xcb_destroy_subwindows_request_t* tmp_add_class_95;
  xcb_destroy_window_request_t* tmp_add_class_96;
  xcb_drawable_iterator_t* tmp_add_class_97;
  xcb_enter_notify_event_t* tmp_add_class_98;
  xcb_expose_event_t* tmp_add_class_99;
  xcb_fill_poly_request_t* tmp_add_class_100;
  xcb_focus_in_event_t* tmp_add_class_101;
  xcb_font_iterator_t* tmp_add_class_102;
  xcb_fontable_iterator_t* tmp_add_class_103;
  xcb_fontprop_iterator_t* tmp_add_class_104;
  xcb_fontprop_t* tmp_add_class_105;
  xcb_force_screen_saver_request_t* tmp_add_class_106;
  xcb_format_iterator_t* tmp_add_class_107;
  xcb_format_t* tmp_add_class_108;
  xcb_free_colormap_request_t* tmp_add_class_109;
  xcb_free_colors_request_t* tmp_add_class_110;
  xcb_free_cursor_request_t* tmp_add_class_111;
  xcb_free_gc_request_t* tmp_add_class_112;
  xcb_free_pixmap_request_t* tmp_add_class_113;
  xcb_gcontext_iterator_t* tmp_add_class_114;
  xcb_ge_generic_event_t* tmp_add_class_115;
  xcb_get_atom_name_cookie_t* tmp_add_class_116;
  xcb_get_atom_name_reply_t* tmp_add_class_117;
  xcb_get_atom_name_request_t* tmp_add_class_118;
  xcb_get_font_path_cookie_t* tmp_add_class_119;
  xcb_get_font_path_reply_t* tmp_add_class_120;
  xcb_get_font_path_request_t* tmp_add_class_121;
  xcb_get_geometry_cookie_t* tmp_add_class_122;
  xcb_get_geometry_reply_t* tmp_add_class_123;
  xcb_get_geometry_request_t* tmp_add_class_124;
  xcb_get_image_cookie_t* tmp_add_class_125;
  xcb_get_image_reply_t* tmp_add_class_126;
  xcb_get_image_request_t* tmp_add_class_127;
  xcb_get_input_focus_cookie_t* tmp_add_class_128;
  xcb_get_input_focus_reply_t* tmp_add_class_129;
  xcb_get_input_focus_request_t* tmp_add_class_130;
  xcb_get_keyboard_control_cookie_t* tmp_add_class_131;
  xcb_get_keyboard_control_reply_t* tmp_add_class_132;
  xcb_get_keyboard_control_request_t* tmp_add_class_133;
  xcb_get_keyboard_mapping_cookie_t* tmp_add_class_134;
  xcb_get_keyboard_mapping_reply_t* tmp_add_class_135;
  xcb_get_keyboard_mapping_request_t* tmp_add_class_136;
  xcb_get_modifier_mapping_cookie_t* tmp_add_class_137;
  xcb_get_modifier_mapping_reply_t* tmp_add_class_138;
  xcb_get_modifier_mapping_request_t* tmp_add_class_139;
  xcb_get_motion_events_cookie_t* tmp_add_class_140;
  xcb_get_motion_events_reply_t* tmp_add_class_141;
  xcb_get_motion_events_request_t* tmp_add_class_142;
  xcb_get_pointer_control_cookie_t* tmp_add_class_143;
  xcb_get_pointer_control_reply_t* tmp_add_class_144;
  xcb_get_pointer_control_request_t* tmp_add_class_145;
  xcb_get_pointer_mapping_cookie_t* tmp_add_class_146;
  xcb_get_pointer_mapping_reply_t* tmp_add_class_147;
  xcb_get_pointer_mapping_request_t* tmp_add_class_148;
  xcb_get_property_cookie_t* tmp_add_class_149;
  xcb_get_property_reply_t* tmp_add_class_150;
  xcb_get_property_request_t* tmp_add_class_151;
  xcb_get_screen_saver_cookie_t* tmp_add_class_152;
  xcb_get_screen_saver_reply_t* tmp_add_class_153;
  xcb_get_screen_saver_request_t* tmp_add_class_154;
  xcb_get_selection_owner_cookie_t* tmp_add_class_155;
  xcb_get_selection_owner_reply_t* tmp_add_class_156;
  xcb_get_selection_owner_request_t* tmp_add_class_157;
  xcb_get_window_attributes_cookie_t* tmp_add_class_158;
  xcb_get_window_attributes_reply_t* tmp_add_class_159;
  xcb_get_window_attributes_request_t* tmp_add_class_160;
  xcb_grab_button_request_t* tmp_add_class_161;
  xcb_grab_key_request_t* tmp_add_class_162;
  xcb_grab_keyboard_cookie_t* tmp_add_class_163;
  xcb_grab_keyboard_reply_t* tmp_add_class_164;
  xcb_grab_keyboard_request_t* tmp_add_class_165;
  xcb_grab_pointer_cookie_t* tmp_add_class_166;
  xcb_grab_pointer_reply_t* tmp_add_class_167;
  xcb_grab_pointer_request_t* tmp_add_class_168;
  xcb_grab_server_request_t* tmp_add_class_169;
  xcb_graphics_exposure_event_t* tmp_add_class_170;
  xcb_gravity_notify_event_t* tmp_add_class_171;
  xcb_host_iterator_t* tmp_add_class_172;
  xcb_host_t* tmp_add_class_173;
  xcb_image_text_16_request_t* tmp_add_class_174;
  xcb_image_text_8_request_t* tmp_add_class_175;
  xcb_install_colormap_request_t* tmp_add_class_176;
  xcb_intern_atom_cookie_t* tmp_add_class_177;
  xcb_intern_atom_reply_t* tmp_add_class_178;
  xcb_intern_atom_request_t* tmp_add_class_179;
  xcb_key_press_event_t* tmp_add_class_180;
  xcb_keycode32_iterator_t* tmp_add_class_181;
  xcb_keycode_iterator_t* tmp_add_class_182;
  xcb_keymap_notify_event_t* tmp_add_class_183;
  xcb_keysym_iterator_t* tmp_add_class_184;
  xcb_kill_client_request_t* tmp_add_class_185;
  xcb_list_extensions_cookie_t* tmp_add_class_186;
  xcb_list_extensions_reply_t* tmp_add_class_187;
  xcb_list_extensions_request_t* tmp_add_class_188;
  xcb_list_fonts_cookie_t* tmp_add_class_189;
  xcb_list_fonts_reply_t* tmp_add_class_190;
  xcb_list_fonts_request_t* tmp_add_class_191;
  xcb_list_fonts_with_info_cookie_t* tmp_add_class_192;
  xcb_list_fonts_with_info_reply_t* tmp_add_class_193;
  xcb_list_fonts_with_info_request_t* tmp_add_class_194;
  xcb_list_hosts_cookie_t* tmp_add_class_195;
  xcb_list_hosts_reply_t* tmp_add_class_196;
  xcb_list_hosts_request_t* tmp_add_class_197;
  xcb_list_installed_colormaps_cookie_t* tmp_add_class_198;
  xcb_list_installed_colormaps_reply_t* tmp_add_class_199;
  xcb_list_installed_colormaps_request_t* tmp_add_class_200;
  xcb_list_properties_cookie_t* tmp_add_class_201;
  xcb_list_properties_reply_t* tmp_add_class_202;
  xcb_list_properties_request_t* tmp_add_class_203;
  xcb_lookup_color_cookie_t* tmp_add_class_204;
  xcb_lookup_color_reply_t* tmp_add_class_205;
  xcb_lookup_color_request_t* tmp_add_class_206;
  xcb_map_notify_event_t* tmp_add_class_207;
  xcb_map_request_event_t* tmp_add_class_208;
  xcb_map_subwindows_request_t* tmp_add_class_209;
  xcb_map_window_request_t* tmp_add_class_210;
  xcb_mapping_notify_event_t* tmp_add_class_211;
  xcb_motion_notify_event_t* tmp_add_class_212;
  xcb_no_exposure_event_t* tmp_add_class_213;
  xcb_no_operation_request_t* tmp_add_class_214;
  xcb_open_font_request_t* tmp_add_class_215;
  xcb_pixmap_iterator_t* tmp_add_class_216;
  xcb_point_iterator_t* tmp_add_class_217;
  xcb_point_t* tmp_add_class_218;
  xcb_poly_arc_request_t* tmp_add_class_219;
  xcb_poly_fill_arc_request_t* tmp_add_class_220;
  xcb_poly_fill_rectangle_request_t* tmp_add_class_221;
  xcb_poly_line_request_t* tmp_add_class_222;
  xcb_poly_point_request_t* tmp_add_class_223;
  xcb_poly_rectangle_request_t* tmp_add_class_224;
  xcb_poly_segment_request_t* tmp_add_class_225;
  xcb_poly_text_16_request_t* tmp_add_class_226;
  xcb_poly_text_8_request_t* tmp_add_class_227;
  xcb_property_notify_event_t* tmp_add_class_228;
  xcb_put_image_request_t* tmp_add_class_229;
  xcb_query_best_size_cookie_t* tmp_add_class_230;
  xcb_query_best_size_reply_t* tmp_add_class_231;
  xcb_query_best_size_request_t* tmp_add_class_232;
  xcb_query_colors_cookie_t* tmp_add_class_233;
  xcb_query_colors_reply_t* tmp_add_class_234;
  xcb_query_colors_request_t* tmp_add_class_235;
  xcb_query_extension_cookie_t* tmp_add_class_236;
  xcb_query_extension_reply_t* tmp_add_class_237;
  xcb_query_extension_request_t* tmp_add_class_238;
  xcb_query_font_cookie_t* tmp_add_class_239;
  xcb_query_font_reply_t* tmp_add_class_240;
  xcb_query_font_request_t* tmp_add_class_241;
  xcb_query_keymap_cookie_t* tmp_add_class_242;
  xcb_query_keymap_reply_t* tmp_add_class_243;
  xcb_query_keymap_request_t* tmp_add_class_244;
  xcb_query_pointer_cookie_t* tmp_add_class_245;
  xcb_query_pointer_reply_t* tmp_add_class_246;
  xcb_query_pointer_request_t* tmp_add_class_247;
  xcb_query_text_extents_cookie_t* tmp_add_class_248;
  xcb_query_text_extents_reply_t* tmp_add_class_249;
  xcb_query_text_extents_request_t* tmp_add_class_250;
  xcb_query_tree_cookie_t* tmp_add_class_251;
  xcb_query_tree_reply_t* tmp_add_class_252;
  xcb_query_tree_request_t* tmp_add_class_253;
  xcb_recolor_cursor_request_t* tmp_add_class_254;
  xcb_rectangle_iterator_t* tmp_add_class_255;
  xcb_rectangle_t* tmp_add_class_256;
  xcb_reparent_notify_event_t* tmp_add_class_257;
  xcb_reparent_window_request_t* tmp_add_class_258;
  xcb_request_error_t* tmp_add_class_259;
  xcb_resize_request_event_t* tmp_add_class_260;
  xcb_rgb_iterator_t* tmp_add_class_261;
  xcb_rgb_t* tmp_add_class_262;
  xcb_rotate_properties_request_t* tmp_add_class_263;
  xcb_screen_iterator_t* tmp_add_class_264;
  xcb_screen_t* tmp_add_class_265;
  xcb_segment_iterator_t* tmp_add_class_266;
  xcb_segment_t* tmp_add_class_267;
  xcb_selection_clear_event_t* tmp_add_class_268;
  xcb_selection_notify_event_t* tmp_add_class_269;
  xcb_selection_request_event_t* tmp_add_class_270;
  xcb_send_event_request_t* tmp_add_class_271;
  xcb_set_access_control_request_t* tmp_add_class_272;
  xcb_set_clip_rectangles_request_t* tmp_add_class_273;
  xcb_set_close_down_mode_request_t* tmp_add_class_274;
  xcb_set_dashes_request_t* tmp_add_class_275;
  xcb_set_font_path_request_t* tmp_add_class_276;
  xcb_set_input_focus_request_t* tmp_add_class_277;
  xcb_set_modifier_mapping_cookie_t* tmp_add_class_278;
  xcb_set_modifier_mapping_reply_t* tmp_add_class_279;
  xcb_set_modifier_mapping_request_t* tmp_add_class_280;
  xcb_set_pointer_mapping_cookie_t* tmp_add_class_281;
  xcb_set_pointer_mapping_reply_t* tmp_add_class_282;
  xcb_set_pointer_mapping_request_t* tmp_add_class_283;
  xcb_set_screen_saver_request_t* tmp_add_class_284;
  xcb_set_selection_owner_request_t* tmp_add_class_285;
  xcb_setup_authenticate_iterator_t* tmp_add_class_286;
  xcb_setup_authenticate_t* tmp_add_class_287;
  xcb_setup_failed_iterator_t* tmp_add_class_288;
  xcb_setup_failed_t* tmp_add_class_289;
  xcb_setup_iterator_t* tmp_add_class_290;
  xcb_setup_request_iterator_t* tmp_add_class_291;
  xcb_setup_request_t* tmp_add_class_292;
  xcb_setup_t* tmp_add_class_293;
  xcb_store_colors_request_t* tmp_add_class_294;
  xcb_store_named_color_request_t* tmp_add_class_295;
  xcb_str_iterator_t* tmp_add_class_296;
  xcb_str_t* tmp_add_class_297;
  xcb_timecoord_iterator_t* tmp_add_class_298;
  xcb_timecoord_t* tmp_add_class_299;
  xcb_timestamp_iterator_t* tmp_add_class_300;
  xcb_translate_coordinates_cookie_t* tmp_add_class_301;
  xcb_translate_coordinates_reply_t* tmp_add_class_302;
  xcb_translate_coordinates_request_t* tmp_add_class_303;
  xcb_ungrab_button_request_t* tmp_add_class_304;
  xcb_ungrab_key_request_t* tmp_add_class_305;
  xcb_ungrab_keyboard_request_t* tmp_add_class_306;
  xcb_ungrab_pointer_request_t* tmp_add_class_307;
  xcb_ungrab_server_request_t* tmp_add_class_308;
  xcb_uninstall_colormap_request_t* tmp_add_class_309;
  xcb_unmap_notify_event_t* tmp_add_class_310;
  xcb_unmap_subwindows_request_t* tmp_add_class_311;
  xcb_unmap_window_request_t* tmp_add_class_312;
  xcb_value_error_t* tmp_add_class_313;
  xcb_visibility_notify_event_t* tmp_add_class_314;
  xcb_visualid_iterator_t* tmp_add_class_315;
  xcb_visualtype_iterator_t* tmp_add_class_316;
  xcb_visualtype_t* tmp_add_class_317;
  xcb_warp_pointer_request_t* tmp_add_class_318;
  xcb_window_iterator_t* tmp_add_class_319;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/phU9rfNGSr/dump1.h"  -I/usr/include/Fcitx5/Utils -I/usr/include/Fcitx5/Core -I/usr/include/Fcitx5/Config

