Temporary header file '/tmp/e17vbJ_O2A/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/dbus-1.0/dbus/dbus.h"
  #include "/usr/include/fcitx/addon.h"
  #include "/usr/include/fcitx/candidate.h"
  #include "/usr/include/fcitx/configfile.h"
  #include "/usr/include/fcitx/context.h"
  #include "/usr/include/fcitx/fcitx.h"
  #include "/usr/include/fcitx/frontend.h"
  #include "/usr/include/fcitx/hook.h"
  #include "/usr/include/fcitx/ime.h"
  #include "/usr/include/fcitx/instance.h"
  #include "/usr/include/fcitx/keys.h"
  #include "/usr/include/fcitx/module/chttrans/chttrans.h"
  #include "/usr/include/fcitx/module/chttrans/fcitx-chttrans.h"
  #include "/usr/include/fcitx/module/classicui/classicuiinterface.h"
  #include "/usr/include/fcitx/module/classicui/fcitx-classicui.h"
  #include "/usr/include/fcitx/module/clipboard/clipboard.h"
  #include "/usr/include/fcitx/module/clipboard/fcitx-clipboard.h"
  #include "/usr/include/fcitx/module/dbus/dbusstuff.h"
  #include "/usr/include/fcitx/module/dbus/fcitx-dbus.h"
  #include "/usr/include/fcitx/module/freedesktop-notify/fcitx-freedesktop-notify.h"
  #include "/usr/include/fcitx/module/freedesktop-notify/freedesktop-notify.h"
  #include "/usr/include/fcitx/module/ipc/ipc.h"
  #include "/usr/include/fcitx/module/lua/fcitx-lua.h"
  #include "/usr/include/fcitx/module/lua/luamod.h"
  #include "/usr/include/fcitx/module/notificationitem/fcitx-notificationitem.h"
  #include "/usr/include/fcitx/module/notificationitem/notificationitem.h"
  #include "/usr/include/fcitx/module/pinyin/fcitx-pinyin.h"
  #include "/usr/include/fcitx/module/pinyin/pydef.h"
  #include "/usr/include/fcitx/module/pinyin-enhance/fcitx-pinyin-enhance.h"
  #include "/usr/include/fcitx/module/pinyin-enhance/pinyin-enhance.h"
  #include "/usr/include/fcitx/module/punc/fcitx-punc.h"
  #include "/usr/include/fcitx/module/punc/punc.h"
  #include "/usr/include/fcitx/module/quickphrase/fcitx-quickphrase.h"
  #include "/usr/include/fcitx/module/quickphrase/quickphrase.h"
  #include "/usr/include/fcitx/module/spell/fcitx-spell.h"
  #include "/usr/include/fcitx/module/spell/spell.h"
  #include "/usr/include/fcitx/module/x11/fcitx-x11.h"
  #include "/usr/include/fcitx/module/x11/x11stuff.h"
  #include "/usr/include/fcitx/module/xkb/fcitx-xkb.h"
  #include "/usr/include/fcitx/module/xkb/xkb.h"
  #include "/usr/include/fcitx/module/xkbdbus/fcitx-xkbdbus.h"
  #include "/usr/include/fcitx/module.h"
  #include "/usr/include/fcitx/profile.h"
  #include "/usr/include/fcitx/ui.h"
  #include "/usr/include/fcitx-config/fcitx-config.h"
  #include "/usr/include/fcitx-config/hotkey.h"
  #include "/usr/include/fcitx-config/xdg.h"
  #include "/usr/include/fcitx-gclient/fcitxclient.h"
  #include "/usr/include/fcitx-gclient/fcitxconnection.h"
  #include "/usr/include/fcitx-gclient/fcitxinputmethod.h"
  #include "/usr/include/fcitx-gclient/fcitxkbd.h"
  #include "/usr/include/fcitx-utils/bitset.h"
  #include "/usr/include/fcitx-utils/desktop-parse.h"
  #include "/usr/include/fcitx-utils/handler-table.h"
  #include "/usr/include/fcitx-utils/keysym.h"
  #include "/usr/include/fcitx-utils/keysymgen.h"
  #include "/usr/include/fcitx-utils/log.h"
  #include "/usr/include/fcitx-utils/memory.h"
  #include "/usr/include/fcitx-utils/objpool.h"
  #include "/usr/include/fcitx-utils/stringmap.h"
  #include "/usr/include/fcitx-utils/utarray.h"
  #include "/usr/include/fcitx-utils/utf8.h"
  #include "/usr/include/fcitx-utils/uthash.h"
  #include "/usr/include/fcitx-utils/utils.h"

  // add classes
  DBusError* tmp_add_class_0;
  DBusMessageIter* tmp_add_class_1;
  DBusObjectPathVTable* tmp_add_class_2;
  UT_hash_bucket* tmp_add_class_3;
  UT_hash_handle* tmp_add_class_4;
  UT_hash_table* tmp_add_class_5;
  _FcitxAddon* tmp_add_class_6;
  _FcitxBitSet* tmp_add_class_7;
  _FcitxCandidateWord* tmp_add_class_8;
  _FcitxClient* tmp_add_class_9;
  _FcitxClientClass* tmp_add_class_10;
  _FcitxConfig* tmp_add_class_11;
  _FcitxConfigColor* tmp_add_class_12;
  _FcitxConfigConstrain* tmp_add_class_13;
  _FcitxConfigEnum* tmp_add_class_14;
  _FcitxConfigFile* tmp_add_class_15;
  _FcitxConfigFileDesc* tmp_add_class_16;
  _FcitxConfigGroup* tmp_add_class_17;
  _FcitxConfigGroupDesc* tmp_add_class_18;
  _FcitxConfigOption* tmp_add_class_19;
  _FcitxConfigOptionDesc* tmp_add_class_20;
  _FcitxConfigOptionDesc2* tmp_add_class_21;
  _FcitxConfigOptionSubkey* tmp_add_class_22;
  _FcitxConfigValueType* tmp_add_class_23;
  _FcitxConnection* tmp_add_class_24;
  _FcitxConnectionClass* tmp_add_class_25;
  _FcitxDesktopEntry* tmp_add_class_26;
  _FcitxDesktopFile* tmp_add_class_27;
  _FcitxDesktopGroup* tmp_add_class_28;
  _FcitxFrontend* tmp_add_class_29;
  _FcitxGenericConfig* tmp_add_class_30;
  _FcitxHotkey* tmp_add_class_31;
  _FcitxHotkeyHook* tmp_add_class_32;
  _FcitxHotkeys* tmp_add_class_33;
  _FcitxICEventHook* tmp_add_class_34;
  _FcitxIM* tmp_add_class_35;
  _FcitxIMClass* tmp_add_class_36;
  _FcitxIMClass2* tmp_add_class_37;
  _FcitxIMEventHook* tmp_add_class_38;
  _FcitxIMIFace* tmp_add_class_39;
  _FcitxIMItem* tmp_add_class_40;
  _FcitxInputContext* tmp_add_class_41;
  _FcitxInputContext2* tmp_add_class_42;
  _FcitxInputMethod* tmp_add_class_43;
  _FcitxInputMethodClass* tmp_add_class_44;
  _FcitxKbd* tmp_add_class_45;
  _FcitxKbdClass* tmp_add_class_46;
  _FcitxKeyFilterHook* tmp_add_class_47;
  _FcitxLayoutItem* tmp_add_class_48;
  _FcitxMenuItem* tmp_add_class_49;
  _FcitxModule* tmp_add_class_50;
  _FcitxModuleFunctionArg* tmp_add_class_51;
  _FcitxObjPool* tmp_add_class_52;
  _FcitxPreeditItem* tmp_add_class_53;
  _FcitxProfile* tmp_add_class_54;
  _FcitxRect* tmp_add_class_55;
  _FcitxStringFilterHook* tmp_add_class_56;
  _FcitxStringHashSet* tmp_add_class_57;
  _FcitxUI* tmp_add_class_58;
  _FcitxUIComplexStatus* tmp_add_class_59;
  _FcitxUIMenu* tmp_add_class_60;
  _FcitxUIStatus* tmp_add_class_61;
  _FcitxUIStatusHook* tmp_add_class_62;
  _FcitxXkbLayoutInfo* tmp_add_class_63;
  _FcitxXkbModelInfo* tmp_add_class_64;
  _FcitxXkbOptionGroupInfo* tmp_add_class_65;
  _FcitxXkbOptionInfo* tmp_add_class_66;
  _FcitxXkbRules* tmp_add_class_67;
  _FcitxXkbRulesHandler* tmp_add_class_68;
  _FcitxXkbVariantInfo* tmp_add_class_69;
  _GActionEntry* tmp_add_class_70;
  _GActionGroupInterface* tmp_add_class_71;
  _GActionInterface* tmp_add_class_72;
  _GActionMapInterface* tmp_add_class_73;
  _GAppInfoIface* tmp_add_class_74;
  _GAppLaunchContext* tmp_add_class_75;
  _GAppLaunchContextClass* tmp_add_class_76;
  _GApplication* tmp_add_class_77;
  _GApplicationClass* tmp_add_class_78;
  _GApplicationCommandLine* tmp_add_class_79;
  _GApplicationCommandLineClass* tmp_add_class_80;
  _GArray* tmp_add_class_81;
  _GAsyncInitableIface* tmp_add_class_82;
  _GAsyncResultIface* tmp_add_class_83;
  _GBufferedInputStream* tmp_add_class_84;
  _GBufferedInputStreamClass* tmp_add_class_85;
  _GBufferedOutputStream* tmp_add_class_86;
  _GBufferedOutputStreamClass* tmp_add_class_87;
  _GByteArray* tmp_add_class_88;
  _GCClosure* tmp_add_class_89;
  _GCancellable* tmp_add_class_90;
  _GCancellableClass* tmp_add_class_91;
  _GCharsetConverterClass* tmp_add_class_92;
  _GClosure* tmp_add_class_93;
  _GClosureNotifyData* tmp_add_class_94;
  _GCompletion* tmp_add_class_95;
  _GCond* tmp_add_class_96;
  _GConverterIface* tmp_add_class_97;
  _GConverterInputStream* tmp_add_class_98;
  _GConverterInputStreamClass* tmp_add_class_99;
  _GConverterOutputStream* tmp_add_class_100;
  _GConverterOutputStreamClass* tmp_add_class_101;
  _GDBusAnnotationInfo* tmp_add_class_102;
  _GDBusArgInfo* tmp_add_class_103;
  _GDBusErrorEntry* tmp_add_class_104;
  _GDBusInterfaceIface* tmp_add_class_105;
  _GDBusInterfaceInfo* tmp_add_class_106;
  _GDBusInterfaceSkeleton* tmp_add_class_107;
  _GDBusInterfaceSkeletonClass* tmp_add_class_108;
  _GDBusInterfaceVTable* tmp_add_class_109;
  _GDBusMethodInfo* tmp_add_class_110;
  _GDBusNodeInfo* tmp_add_class_111;
  _GDBusObjectIface* tmp_add_class_112;
  _GDBusObjectManagerClient* tmp_add_class_113;
  _GDBusObjectManagerClientClass* tmp_add_class_114;
  _GDBusObjectManagerIface* tmp_add_class_115;
  _GDBusObjectManagerServer* tmp_add_class_116;
  _GDBusObjectManagerServerClass* tmp_add_class_117;
  _GDBusObjectProxy* tmp_add_class_118;
  _GDBusObjectProxyClass* tmp_add_class_119;
  _GDBusObjectSkeleton* tmp_add_class_120;
  _GDBusObjectSkeletonClass* tmp_add_class_121;
  _GDBusPropertyInfo* tmp_add_class_122;
  _GDBusProxy* tmp_add_class_123;
  _GDBusProxyClass* tmp_add_class_124;
  _GDBusSignalInfo* tmp_add_class_125;
  _GDBusSubtreeVTable* tmp_add_class_126;
  _GDataInputStream* tmp_add_class_127;
  _GDataInputStreamClass* tmp_add_class_128;
  _GDataOutputStream* tmp_add_class_129;
  _GDataOutputStreamClass* tmp_add_class_130;
  _GDatagramBasedInterface* tmp_add_class_131;
  _GDate* tmp_add_class_132;
  _GDebugControllerDBus* tmp_add_class_133;
  _GDebugControllerDBusClass* tmp_add_class_134;
  _GDebugControllerInterface* tmp_add_class_135;
  _GDebugKey* tmp_add_class_136;
  _GDoubleIEEE754* tmp_add_class_137;
  _GDriveIface* tmp_add_class_138;
  _GDtlsClientConnectionInterface* tmp_add_class_139;
  _GDtlsConnectionInterface* tmp_add_class_140;
  _GDtlsServerConnectionInterface* tmp_add_class_141;
  _GEmblemedIcon* tmp_add_class_142;
  _GEmblemedIconClass* tmp_add_class_143;
  _GEnumClass* tmp_add_class_144;
  _GEnumValue* tmp_add_class_145;
  _GError* tmp_add_class_146;
  _GFileAttributeInfo* tmp_add_class_147;
  _GFileAttributeInfoList* tmp_add_class_148;
  _GFileEnumerator* tmp_add_class_149;
  _GFileEnumeratorClass* tmp_add_class_150;
  _GFileIOStream* tmp_add_class_151;
  _GFileIOStreamClass* tmp_add_class_152;
  _GFileIface* tmp_add_class_153;
  _GFileInputStream* tmp_add_class_154;
  _GFileInputStreamClass* tmp_add_class_155;
  _GFileMonitor* tmp_add_class_156;
  _GFileMonitorClass* tmp_add_class_157;
  _GFileOutputStream* tmp_add_class_158;
  _GFileOutputStreamClass* tmp_add_class_159;
  _GFilenameCompleterClass* tmp_add_class_160;
  _GFilterInputStream* tmp_add_class_161;
  _GFilterInputStreamClass* tmp_add_class_162;
  _GFilterOutputStream* tmp_add_class_163;
  _GFilterOutputStreamClass* tmp_add_class_164;
  _GFlagsClass* tmp_add_class_165;
  _GFlagsValue* tmp_add_class_166;
  _GFloatIEEE754* tmp_add_class_167;
  _GHashTableIter* tmp_add_class_168;
  _GHook* tmp_add_class_169;
  _GHookList* tmp_add_class_170;
  _GIOChannel* tmp_add_class_171;
  _GIOFuncs* tmp_add_class_172;
  _GIOStream* tmp_add_class_173;
  _GIOStreamClass* tmp_add_class_174;
  _GIconIface* tmp_add_class_175;
  _GInetAddress* tmp_add_class_176;
  _GInetAddressClass* tmp_add_class_177;
  _GInetAddressMask* tmp_add_class_178;
  _GInetAddressMaskClass* tmp_add_class_179;
  _GInetSocketAddress* tmp_add_class_180;
  _GInetSocketAddressClass* tmp_add_class_181;
  _GInitableIface* tmp_add_class_182;
  _GInputMessage* tmp_add_class_183;
  _GInputStream* tmp_add_class_184;
  _GInputStreamClass* tmp_add_class_185;
  _GInputVector* tmp_add_class_186;
  _GInterfaceInfo* tmp_add_class_187;
  _GList* tmp_add_class_188;
  _GListModelInterface* tmp_add_class_189;
  _GLoadableIconIface* tmp_add_class_190;
  _GLogField* tmp_add_class_191;
  _GMarkupParser* tmp_add_class_192;
  _GMemVTable* tmp_add_class_193;
  _GMemoryInputStream* tmp_add_class_194;
  _GMemoryInputStreamClass* tmp_add_class_195;
  _GMemoryMonitorInterface* tmp_add_class_196;
  _GMemoryOutputStream* tmp_add_class_197;
  _GMemoryOutputStreamClass* tmp_add_class_198;
  _GMenuAttributeIter* tmp_add_class_199;
  _GMenuAttributeIterClass* tmp_add_class_200;
  _GMenuLinkIter* tmp_add_class_201;
  _GMenuLinkIterClass* tmp_add_class_202;
  _GMenuModel* tmp_add_class_203;
  _GMenuModelClass* tmp_add_class_204;
  _GMountIface* tmp_add_class_205;
  _GMountOperation* tmp_add_class_206;
  _GMountOperationClass* tmp_add_class_207;
  _GMutex* tmp_add_class_208;
  _GNativeSocketAddress* tmp_add_class_209;
  _GNativeSocketAddressClass* tmp_add_class_210;
  _GNativeVolumeMonitor* tmp_add_class_211;
  _GNativeVolumeMonitorClass* tmp_add_class_212;
  _GNetworkAddress* tmp_add_class_213;
  _GNetworkAddressClass* tmp_add_class_214;
  _GNetworkMonitorInterface* tmp_add_class_215;
  _GNetworkService* tmp_add_class_216;
  _GNetworkServiceClass* tmp_add_class_217;
  _GNode* tmp_add_class_218;
  _GObject* tmp_add_class_219;
  _GObjectClass* tmp_add_class_220;
  _GObjectConstructParam* tmp_add_class_221;
  _GOnce* tmp_add_class_222;
  _GOptionEntry* tmp_add_class_223;
  _GOutputMessage* tmp_add_class_224;
  _GOutputStream* tmp_add_class_225;
  _GOutputStreamClass* tmp_add_class_226;
  _GOutputVector* tmp_add_class_227;
  _GParamSpec* tmp_add_class_228;
  _GParamSpecBoolean* tmp_add_class_229;
  _GParamSpecBoxed* tmp_add_class_230;
  _GParamSpecChar* tmp_add_class_231;
  _GParamSpecClass* tmp_add_class_232;
  _GParamSpecDouble* tmp_add_class_233;
  _GParamSpecEnum* tmp_add_class_234;
  _GParamSpecFlags* tmp_add_class_235;
  _GParamSpecFloat* tmp_add_class_236;
  _GParamSpecGType* tmp_add_class_237;
  _GParamSpecInt* tmp_add_class_238;
  _GParamSpecInt64* tmp_add_class_239;
  _GParamSpecLong* tmp_add_class_240;
  _GParamSpecObject* tmp_add_class_241;
  _GParamSpecOverride* tmp_add_class_242;
  _GParamSpecParam* tmp_add_class_243;
  _GParamSpecPointer* tmp_add_class_244;
  _GParamSpecString* tmp_add_class_245;
  _GParamSpecTypeInfo* tmp_add_class_246;
  _GParamSpecUChar* tmp_add_class_247;
  _GParamSpecUInt* tmp_add_class_248;
  _GParamSpecUInt64* tmp_add_class_249;
  _GParamSpecULong* tmp_add_class_250;
  _GParamSpecUnichar* tmp_add_class_251;
  _GParamSpecValueArray* tmp_add_class_252;
  _GParamSpecVariant* tmp_add_class_253;
  _GParameter* tmp_add_class_254;
  _GPathBuf* tmp_add_class_255;
  _GPermission* tmp_add_class_256;
  _GPermissionClass* tmp_add_class_257;
  _GPollFD* tmp_add_class_258;
  _GPollableInputStreamInterface* tmp_add_class_259;
  _GPollableOutputStreamInterface* tmp_add_class_260;
  _GPowerProfileMonitorInterface* tmp_add_class_261;
  _GPrivate* tmp_add_class_262;
  _GProxyAddress* tmp_add_class_263;
  _GProxyAddressClass* tmp_add_class_264;
  _GProxyAddressEnumerator* tmp_add_class_265;
  _GProxyAddressEnumeratorClass* tmp_add_class_266;
  _GProxyInterface* tmp_add_class_267;
  _GProxyResolverInterface* tmp_add_class_268;
  _GPtrArray* tmp_add_class_269;
  _GQueue* tmp_add_class_270;
  _GRWLock* tmp_add_class_271;
  _GRecMutex* tmp_add_class_272;
  _GRemoteActionGroupInterface* tmp_add_class_273;
  _GResolver* tmp_add_class_274;
  _GResolverClass* tmp_add_class_275;
  _GSList* tmp_add_class_276;
  _GScanner* tmp_add_class_277;
  _GScannerConfig* tmp_add_class_278;
  _GSeekableIface* tmp_add_class_279;
  _GSettings* tmp_add_class_280;
  _GSettingsClass* tmp_add_class_281;
  _GSignalInvocationHint* tmp_add_class_282;
  _GSignalQuery* tmp_add_class_283;
  _GSimpleActionGroup* tmp_add_class_284;
  _GSimpleActionGroupClass* tmp_add_class_285;
  _GSimpleProxyResolver* tmp_add_class_286;
  _GSimpleProxyResolverClass* tmp_add_class_287;
  _GSocket* tmp_add_class_288;
  _GSocketAddress* tmp_add_class_289;
  _GSocketAddressClass* tmp_add_class_290;
  _GSocketAddressEnumerator* tmp_add_class_291;
  _GSocketAddressEnumeratorClass* tmp_add_class_292;
  _GSocketClass* tmp_add_class_293;
  _GSocketClient* tmp_add_class_294;
  _GSocketClientClass* tmp_add_class_295;
  _GSocketConnectableIface* tmp_add_class_296;
  _GSocketConnection* tmp_add_class_297;
  _GSocketConnectionClass* tmp_add_class_298;
  _GSocketControlMessage* tmp_add_class_299;
  _GSocketControlMessageClass* tmp_add_class_300;
  _GSocketListener* tmp_add_class_301;
  _GSocketListenerClass* tmp_add_class_302;
  _GSocketService* tmp_add_class_303;
  _GSocketServiceClass* tmp_add_class_304;
  _GSource* tmp_add_class_305;
  _GSourceCallbackFuncs* tmp_add_class_306;
  _GSourceFuncs* tmp_add_class_307;
  _GStaticPrivate* tmp_add_class_308;
  _GStaticRWLock* tmp_add_class_309;
  _GStaticRecMutex* tmp_add_class_310;
  _GStaticResource* tmp_add_class_311;
  _GString* tmp_add_class_312;
  _GTcpConnection* tmp_add_class_313;
  _GTcpConnectionClass* tmp_add_class_314;
  _GTcpWrapperConnection* tmp_add_class_315;
  _GTcpWrapperConnectionClass* tmp_add_class_316;
  _GThread* tmp_add_class_317;
  _GThreadFunctions* tmp_add_class_318;
  _GThreadPool* tmp_add_class_319;
  _GThreadedSocketService* tmp_add_class_320;
  _GThreadedSocketServiceClass* tmp_add_class_321;
  _GTimeVal* tmp_add_class_322;
  _GTlsBackendInterface* tmp_add_class_323;
  _GTlsCertificate* tmp_add_class_324;
  _GTlsCertificateClass* tmp_add_class_325;
  _GTlsClientConnectionInterface* tmp_add_class_326;
  _GTlsConnection* tmp_add_class_327;
  _GTlsConnectionClass* tmp_add_class_328;
  _GTlsDatabase* tmp_add_class_329;
  _GTlsDatabaseClass* tmp_add_class_330;
  _GTlsFileDatabaseInterface* tmp_add_class_331;
  _GTlsInteraction* tmp_add_class_332;
  _GTlsInteractionClass* tmp_add_class_333;
  _GTlsPassword* tmp_add_class_334;
  _GTlsPasswordClass* tmp_add_class_335;
  _GTlsServerConnectionInterface* tmp_add_class_336;
  _GTokenValue* tmp_add_class_337;
  _GTrashStack* tmp_add_class_338;
  _GTuples* tmp_add_class_339;
  _GTypeClass* tmp_add_class_340;
  _GTypeFundamentalInfo* tmp_add_class_341;
  _GTypeInfo* tmp_add_class_342;
  _GTypeInstance* tmp_add_class_343;
  _GTypeInterface* tmp_add_class_344;
  _GTypeModule* tmp_add_class_345;
  _GTypeModuleClass* tmp_add_class_346;
  _GTypePluginClass* tmp_add_class_347;
  _GTypeQuery* tmp_add_class_348;
  _GTypeValueTable* tmp_add_class_349;
  _GUnixConnection* tmp_add_class_350;
  _GUnixConnectionClass* tmp_add_class_351;
  _GUnixCredentialsMessage* tmp_add_class_352;
  _GUnixCredentialsMessageClass* tmp_add_class_353;
  _GUnixFDList* tmp_add_class_354;
  _GUnixFDListClass* tmp_add_class_355;
  _GUnixSocketAddress* tmp_add_class_356;
  _GUnixSocketAddressClass* tmp_add_class_357;
  _GUriParamsIter* tmp_add_class_358;
  _GValue* tmp_add_class_359;
  _GValueArray* tmp_add_class_360;
  _GVariantBuilder* tmp_add_class_361;
  _GVariantDict* tmp_add_class_362;
  _GVariantIter* tmp_add_class_363;
  _GVfs* tmp_add_class_364;
  _GVfsClass* tmp_add_class_365;
  _GVolumeIface* tmp_add_class_366;
  _GVolumeMonitor* tmp_add_class_367;
  _GVolumeMonitorClass* tmp_add_class_368;
  _GZlibCompressorClass* tmp_add_class_369;
  _GZlibDecompressorClass* tmp_add_class_370;
  _G_fpos64_t* tmp_add_class_371;
  _G_fpos_t* tmp_add_class_372;
  _IO_cookie_io_functions_t* tmp_add_class_373;
  _XEvent* tmp_add_class_374;
  _XExtData* tmp_add_class_375;
  _XIMHotKeyTrigger* tmp_add_class_376;
  _XIMHotKeyTriggers* tmp_add_class_377;
  _XIMPreeditCaretCallbackStruct* tmp_add_class_378;
  _XIMPreeditDrawCallbackStruct* tmp_add_class_379;
  _XIMPreeditStateNotifyCallbackStruct* tmp_add_class_380;
  _XIMStatusDrawCallbackStruct* tmp_add_class_381;
  _XIMStringConversionCallbackStruct* tmp_add_class_382;
  _XIMStringConversionText* tmp_add_class_383;
  _XIMText* tmp_add_class_384;
  _XImage* tmp_add_class_385;
  _cairo_matrix* tmp_add_class_386;
  _cairo_path_data_t* tmp_add_class_387;
  _cairo_rectangle* tmp_add_class_388;
  _cairo_rectangle_int* tmp_add_class_389;
  _cairo_rectangle_list* tmp_add_class_390;
  _cairo_user_data_key* tmp_add_class_391;
  cairo_path* tmp_add_class_392;
  statx_timestamp* tmp_add_class_393;
  ucontext_t* tmp_add_class_394;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/e17vbJ_O2A/dump1.h"  -I/usr/include/cairo -I/usr/include/glib-2.0 -I/usr/include/dbus-1.0 -I/lib/arm-linux-gnueabihf/glib-2.0/include -I/lib/arm-linux-gnueabihf/dbus-1.0/include

