Temporary header file '/tmp/14cyAPcuWe/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/AdwaitaQt6/adwaita.h"
  #include "/usr/include/AdwaitaQt6/adwaitacolors.h"
  #include "/usr/include/AdwaitaQt6/adwaitaqt_export.h"
  #include "/usr/include/AdwaitaQt6/adwaitarenderer.h"

  // add namespaces
  namespace Adwaita{typedef int tmp_add_type_1;}
  Adwaita::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace Adwaita{namespace Config{typedef int tmp_add_type_2;}}
  Adwaita::Config::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace Adwaita{namespace PropertyNames{typedef int tmp_add_type_3;}}
  Adwaita::PropertyNames::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace Adwaita{namespace Settings{typedef int tmp_add_type_4;}}
  Adwaita::Settings::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_5;}
  QAlgorithmsPrivate::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_6;}
  QColorConstants::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_7;}}
  QColorConstants::Svg::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QContainerInfo{typedef int tmp_add_type_8;}
  QContainerInfo::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QHashPrivate{typedef int tmp_add_type_9;}
  QHashPrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace Qt{typedef int tmp_add_type_10;}
  Qt::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace Qt{namespace Literals{typedef int tmp_add_type_11;}}
  Qt::Literals::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace Qt{namespace Literals{namespace StringLiterals{typedef int tmp_add_type_12;}}}
  Qt::Literals::StringLiterals::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_13;}
  QtGlobalStatic::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtLiterals{typedef int tmp_add_type_14;}
  QtLiterals::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace QtMetaContainerPrivate{typedef int tmp_add_type_15;}
  QtMetaContainerPrivate::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_16;}
  QtMetaTypePrivate::tmp_add_type_16 tmp_add_func_16(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_17;}
  QtPrivate::tmp_add_type_17 tmp_add_func_17(){return 0;};
  namespace QtPrivate{namespace detail{typedef int tmp_add_type_18;}}
  QtPrivate::detail::tmp_add_type_18 tmp_add_func_18(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_19;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_19 tmp_add_func_19(){return 0;};
  namespace QtPrivate{namespace Tok{typedef int tmp_add_type_20;}}
  QtPrivate::Tok::tmp_add_type_20 tmp_add_func_20(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_21;}
  QtSharedPointer::tmp_add_type_21 tmp_add_func_21(){return 0;};
  namespace QTypeTraits{typedef int tmp_add_type_22;}
  QTypeTraits::tmp_add_type_22 tmp_add_func_22(){return 0;};
  namespace QTypeTraits{namespace detail{typedef int tmp_add_type_23;}}
  QTypeTraits::detail::tmp_add_type_23 tmp_add_func_23(){return 0;};
  namespace q_has_char8_t{typedef int tmp_add_type_24;}
  q_has_char8_t::tmp_add_type_24 tmp_add_func_24(){return 0;};
  namespace q_no_char8_t{typedef int tmp_add_type_25;}
  q_no_char8_t::tmp_add_type_25 tmp_add_func_25(){return 0;};

  // add classes
  QAdoptSharedDataTag* tmp_add_class_0;
  QAnyStringView* tmp_add_class_1;
  QArrayData* tmp_add_class_2;
  QAtomicInt* tmp_add_class_3;
  QBindingStatus* tmp_add_class_4;
  QBindingStorage* tmp_add_class_5;
  QBrush* tmp_add_class_6;
  QBrushData* tmp_add_class_7;
  QBrushDataPointerDeleter* tmp_add_class_8;
  QByteArray* tmp_add_class_9;
  QByteArrayView* tmp_add_class_10;
  QChar* tmp_add_class_11;
  QColor* tmp_add_class_12;
  QConicalGradient* tmp_add_class_13;
  QDataStream* tmp_add_class_14;
  QFlag* tmp_add_class_15;
  QGenericArgument* tmp_add_class_16;
  QGenericReturnArgument* tmp_add_class_17;
  QGradient* tmp_add_class_18;
  QHashSeed* tmp_add_class_19;
  QIODeviceBase* tmp_add_class_20;
  QIcon* tmp_add_class_21;
  QImage* tmp_add_class_22;
  QIncompatibleFlag* tmp_add_class_23;
  QInternal* tmp_add_class_24;
  QKeyCombination* tmp_add_class_25;
  QLatin1Char* tmp_add_class_26;
  QLatin1String* tmp_add_class_27;
  QLine* tmp_add_class_28;
  QLineF* tmp_add_class_29;
  QLinearGradient* tmp_add_class_30;
  QMargins* tmp_add_class_31;
  QMarginsF* tmp_add_class_32;
  QMessageLogContext* tmp_add_class_33;
  QMessageLogger* tmp_add_class_34;
  QMetaAssociation* tmp_add_class_35;
  QMetaContainer* tmp_add_class_36;
  QMetaObject* tmp_add_class_37;
  QMetaSequence* tmp_add_class_38;
  QMetaType* tmp_add_class_39;
  QMethodRawArguments* tmp_add_class_40;
  QObject* tmp_add_class_41;
  QObjectData* tmp_add_class_42;
  QPaintDevice* tmp_add_class_43;
  QPalette* tmp_add_class_44;
  QPartialOrdering* tmp_add_class_45;
  QPixelFormat* tmp_add_class_46;
  QPixmap* tmp_add_class_47;
  QPoint* tmp_add_class_48;
  QPointF* tmp_add_class_49;
  QPolygon* tmp_add_class_50;
  QPolygonF* tmp_add_class_51;
  QRadialGradient* tmp_add_class_52;
  QRect* tmp_add_class_53;
  QRectF* tmp_add_class_54;
  QRegion* tmp_add_class_55;
  QRgba64* tmp_add_class_56;
  QScopedPointerPodDeleter* tmp_add_class_57;
  QSharedData* tmp_add_class_58;
  QSignalBlocker* tmp_add_class_59;
  QSize* tmp_add_class_60;
  QSizeF* tmp_add_class_61;
  QSizePolicy* tmp_add_class_62;
  QString* tmp_add_class_63;
  QStringMatcher* tmp_add_class_64;
  QStringTokenizerBaseBase* tmp_add_class_65;
  QStringView* tmp_add_class_66;
  QStyle* tmp_add_class_67;
  QSysInfo* tmp_add_class_68;
  QTransform* tmp_add_class_69;
  _G_fpos64_t* tmp_add_class_70;
  _G_fpos_t* tmp_add_class_71;
  _IO_cookie_io_functions_t* tmp_add_class_72;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/14cyAPcuWe/dump1.h"  -I/usr/include/arm-linux-gnueabihf/qt6 -I/usr/include/arm-linux-gnueabihf/qt6/QtWidgets -I/usr/include/arm-linux-gnueabihf/qt6/QtGui -I/usr/include/arm-linux-gnueabihf/qt6/QtCore

