Temporary header file '/tmp/iTNpUfjoAr/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/fftw.h"
  #include "/usr/include/fftw_mpi.h"
  #include "/usr/include/fftw_threads.h"
  #include "/usr/include/rfftw.h"
  #include "/usr/include/rfftw_mpi.h"
  #include "/usr/include/rfftw_threads.h"

  // add namespaces
  namespace MPI{typedef int tmp_add_type_1;}
  MPI::tmp_add_type_1 tmp_add_func_1(){return 0;};

  // add classes
  _G_fpos64_t* tmp_add_class_0;
  _G_fpos_t* tmp_add_class_1;
  _IO_cookie_io_functions_t* tmp_add_class_2;
  fftw_mpi_plan_struct* tmp_add_class_3;
  fftw_mpi_twiddle_struct* tmp_add_class_4;
  fftw_plan_node_struct* tmp_add_class_5;
  fftw_plan_struct* tmp_add_class_6;
  fftw_rader_data_struct* tmp_add_class_7;
  fftw_twiddle_struct* tmp_add_class_8;
  ompi_status_public_t* tmp_add_class_9;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/iTNpUfjoAr/dump1.h"  -I/usr/include/arm-linux-gnueabihf/mpi -I/usr/lib/arm-linux-gnueabihf/openmpi/include -I/usr/lib/arm-linux-gnueabihf/openmpi/include/openmpi

