Temporary header file '/tmp/MZW_M94iBY/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/ace/INet/AuthenticationBase.h"
  #include "/usr/include/ace/INet/BidirStreamBuffer.h"
  #include "/usr/include/ace/INet/BufferedStreamBuffer.h"
  #include "/usr/include/ace/INet/ClientRequestHandler.h"
  #include "/usr/include/ace/INet/ConnectionCache.h"
  #include "/usr/include/ace/INet/FTP_ClientRequestHandler.h"
  #include "/usr/include/ace/INet/FTP_IOStream.h"
  #include "/usr/include/ace/INet/FTP_Request.h"
  #include "/usr/include/ace/INet/FTP_Response.h"
  #include "/usr/include/ace/INet/FTP_Session.h"
  #include "/usr/include/ace/INet/FTP_URL.h"
  #include "/usr/include/ace/INet/HTTP_BasicAuthentication.h"
  #include "/usr/include/ace/INet/HTTP_ClientRequestHandler.h"
  #include "/usr/include/ace/INet/HTTP_Header.h"
  #include "/usr/include/ace/INet/HTTP_IOStream.h"
  #include "/usr/include/ace/INet/HTTP_Request.h"
  #include "/usr/include/ace/INet/HTTP_Response.h"
  #include "/usr/include/ace/INet/HTTP_Session.h"
  #include "/usr/include/ace/INet/HTTP_SessionBase.h"
  #include "/usr/include/ace/INet/HTTP_Status.h"
  #include "/usr/include/ace/INet/HTTP_StreamPolicy.h"
  #include "/usr/include/ace/INet/HTTP_StreamPolicyBase.h"
  #include "/usr/include/ace/INet/HTTP_URL.h"
  #include "/usr/include/ace/INet/HeaderBase.h"
  #include "/usr/include/ace/INet/INet_Export.h"
  #include "/usr/include/ace/INet/INet_Log.h"
  #include "/usr/include/ace/INet/IOS_util.h"
  #include "/usr/include/ace/INet/Request.h"
  #include "/usr/include/ace/INet/RequestHandler.h"
  #include "/usr/include/ace/INet/Response.h"
  #include "/usr/include/ace/INet/Sock_IOStream.h"
  #include "/usr/include/ace/INet/StreamHandler.h"
  #include "/usr/include/ace/INet/StreamInterceptor.h"
  #include "/usr/include/ace/INet/String_IOStream.h"
  #include "/usr/include/ace/INet/URLBase.h"

  // add namespaces
  namespace ACE{typedef int tmp_add_type_1;}
  ACE::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace ACE{namespace FTP{typedef int tmp_add_type_2;}}
  ACE::FTP::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace ACE{namespace HTTP{typedef int tmp_add_type_3;}}
  ACE::HTTP::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace ACE{namespace INet{typedef int tmp_add_type_4;}}
  ACE::INet::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace ACE{namespace IOS{typedef int tmp_add_type_5;}}
  ACE::IOS::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace ACE_Acquire_Method{typedef int tmp_add_type_6;}
  ACE_Acquire_Method::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace ACE_OS{typedef int tmp_add_type_7;}
  ACE_OS::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace ACE_Task_Flags{typedef int tmp_add_type_8;}
  ACE_Task_Flags::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace ACE_Utils{typedef int tmp_add_type_9;}
  ACE_Utils::tmp_add_type_9 tmp_add_func_9(){return 0;};

  // add classes
  ACE_Accept_QoS_Params* tmp_add_class_0;
  ACE_Adaptive_Lock* tmp_add_class_1;
  ACE_Addr* tmp_add_class_2;
  ACE_Allocator* tmp_add_class_3;
  ACE_Ascii_To_Wide* tmp_add_class_4;
  ACE_At_Thread_Exit* tmp_add_class_5;
  ACE_At_Thread_Exit_Func* tmp_add_class_6;
  ACE_Auto_String_Free* tmp_add_class_7;
  ACE_Base_Thread_Adapter* tmp_add_class_8;
  ACE_Cleanup* tmp_add_class_9;
  ACE_Cleanup_Info_Node* tmp_add_class_10;
  ACE_Command_Base* tmp_add_class_11;
  ACE_Condition_Attributes* tmp_add_class_12;
  ACE_Connection_Recycling_Strategy* tmp_add_class_13;
  ACE_Copy_Disabled* tmp_add_class_14;
  ACE_DLL* tmp_add_class_15;
  ACE_DLList_Node* tmp_add_class_16;
  ACE_Data_Block* tmp_add_class_17;
  ACE_Deadline_Message_Strategy* tmp_add_class_18;
  ACE_Delegating_Time_Policy* tmp_add_class_19;
  ACE_Dynamic* tmp_add_class_20;
  ACE_Dynamic_Message_Strategy* tmp_add_class_21;
  ACE_Dynamic_Time_Policy_Base* tmp_add_class_22;
  ACE_Errno_Guard* tmp_add_class_23;
  ACE_Event_Handler* tmp_add_class_24;
  ACE_Event_Handler_var* tmp_add_class_25;
  ACE_FPointer_Time_Policy* tmp_add_class_26;
  ACE_Flow_Spec* tmp_add_class_27;
  ACE_Framework_Component* tmp_add_class_28;
  ACE_Framework_Repository* tmp_add_class_29;
  ACE_HR_Time_Policy* tmp_add_class_30;
  ACE_Hashable* tmp_add_class_31;
  ACE_High_Res_Timer* tmp_add_class_32;
  ACE_INET_Addr* tmp_add_class_33;
  ACE_IO_Cntl_Msg* tmp_add_class_34;
  ACE_IPC_SAP* tmp_add_class_35;
  ACE_Laxity_Message_Strategy* tmp_add_class_36;
  ACE_Lock* tmp_add_class_37;
  ACE_Log_Category* tmp_add_class_38;
  ACE_Log_Category_TSS* tmp_add_class_39;
  ACE_Log_Msg* tmp_add_class_40;
  ACE_MT_SYNCH* tmp_add_class_41;
  ACE_Message_Block* tmp_add_class_42;
  ACE_Message_Queue_Base* tmp_add_class_43;
  ACE_Module_Base* tmp_add_class_44;
  ACE_Module_Type* tmp_add_class_45;
  ACE_NS_WString* tmp_add_class_46;
  ACE_NULL_SYNCH* tmp_add_class_47;
  ACE_Noop_Command* tmp_add_class_48;
  ACE_Notification_Buffer* tmp_add_class_49;
  ACE_Notification_Strategy* tmp_add_class_50;
  ACE_Null_Mutex* tmp_add_class_51;
  ACE_OS_Exit_Info* tmp_add_class_52;
  ACE_OS_Log_Msg_Attributes* tmp_add_class_53;
  ACE_OS_Object_Manager* tmp_add_class_54;
  ACE_OS_Recursive_Thread_Mutex_Guard* tmp_add_class_55;
  ACE_OS_Thread_Descriptor* tmp_add_class_56;
  ACE_OS_Thread_Mutex_Guard* tmp_add_class_57;
  ACE_OVERLAPPED* tmp_add_class_58;
  ACE_Object_Manager* tmp_add_class_59;
  ACE_Object_Manager_Base* tmp_add_class_60;
  ACE_Protocol_Info* tmp_add_class_61;
  ACE_QoS* tmp_add_class_62;
  ACE_QoS_Params* tmp_add_class_63;
  ACE_RW_Mutex* tmp_add_class_64;
  ACE_RW_Thread_Mutex* tmp_add_class_65;
  ACE_Reactor* tmp_add_class_66;
  ACE_Reactor_Impl* tmp_add_class_67;
  ACE_Reactor_Notification_Strategy* tmp_add_class_68;
  ACE_Reactor_Notify* tmp_add_class_69;
  ACE_Reactor_Timer_Interface* tmp_add_class_70;
  ACE_Recursive_Thread_Mutex* tmp_add_class_71;
  ACE_Recyclable* tmp_add_class_72;
  ACE_SOCK* tmp_add_class_73;
  ACE_SOCK_Connector* tmp_add_class_74;
  ACE_SOCK_IO* tmp_add_class_75;
  ACE_SOCK_Stream* tmp_add_class_76;
  ACE_SString* tmp_add_class_77;
  ACE_Service_Gestalt* tmp_add_class_78;
  ACE_Service_Object* tmp_add_class_79;
  ACE_Service_Object_Ptr* tmp_add_class_80;
  ACE_Service_Object_Type* tmp_add_class_81;
  ACE_Service_Repository* tmp_add_class_82;
  ACE_Service_Repository_Iterator* tmp_add_class_83;
  ACE_Service_Type* tmp_add_class_84;
  ACE_Service_Type_Dynamic_Guard* tmp_add_class_85;
  ACE_Service_Type_Impl* tmp_add_class_86;
  ACE_Shared_Object* tmp_add_class_87;
  ACE_Static_Object_Lock* tmp_add_class_88;
  ACE_Str_Buf* tmp_add_class_89;
  ACE_Stream_Type* tmp_add_class_90;
  ACE_String_Base_Const* tmp_add_class_91;
  ACE_Synch_Options* tmp_add_class_92;
  ACE_System_Time_Policy* tmp_add_class_93;
  ACE_Task_Base* tmp_add_class_94;
  ACE_Thread* tmp_add_class_95;
  ACE_Thread_Adapter* tmp_add_class_96;
  ACE_Thread_Control* tmp_add_class_97;
  ACE_Thread_Descriptor* tmp_add_class_98;
  ACE_Thread_Descriptor_Base* tmp_add_class_99;
  ACE_Thread_Exit* tmp_add_class_100;
  ACE_Thread_Exit_Maybe* tmp_add_class_101;
  ACE_Thread_ID* tmp_add_class_102;
  ACE_Thread_Manager* tmp_add_class_103;
  ACE_Thread_Mutex* tmp_add_class_104;
  ACE_Time_Value* tmp_add_class_105;
  ACE_Trace* tmp_add_class_106;
  ACE_Wide_To_Ascii* tmp_add_class_107;
  ACE_event_t* tmp_add_class_108;
  ACE_eventdata_t* tmp_add_class_109;
  _G_fpos64_t* tmp_add_class_110;
  _G_fpos_t* tmp_add_class_111;
  _IO_cookie_io_functions_t* tmp_add_class_112;
  cancel_state* tmp_add_class_113;
  cmsghdr* tmp_add_class_114;
  dl_find_object* tmp_add_class_115;
  entry* tmp_add_class_116;
  f_owner_ex* tmp_add_class_117;
  file_handle* tmp_add_class_118;
  flock64* tmp_add_class_119;
  group_filter* tmp_add_class_120;
  group_req* tmp_add_class_121;
  group_source_req* tmp_add_class_122;
  hsearch_data* tmp_add_class_123;
  ifaddr* tmp_add_class_124;
  ifconf* tmp_add_class_125;
  ifmap* tmp_add_class_126;
  ifreq* tmp_add_class_127;
  in6_addr* tmp_add_class_128;
  in6_pktinfo* tmp_add_class_129;
  in_addr* tmp_add_class_130;
  in_pktinfo* tmp_add_class_131;
  iovec* tmp_add_class_132;
  ip6_mtuinfo* tmp_add_class_133;
  ip_mreq* tmp_add_class_134;
  ip_mreq_source* tmp_add_class_135;
  ip_mreqn* tmp_add_class_136;
  ip_msfilter* tmp_add_class_137;
  ip_opts* tmp_add_class_138;
  ipc_perm* tmp_add_class_139;
  ipv6_mreq* tmp_add_class_140;
  itimerval* tmp_add_class_141;
  linger* tmp_add_class_142;
  mmsghdr* tmp_add_class_143;
  msghdr* tmp_add_class_144;
  nl_mmap_hdr* tmp_add_class_145;
  nl_mmap_req* tmp_add_class_146;
  nl_pktinfo* tmp_add_class_147;
  nla_bitfield32* tmp_add_class_148;
  nlattr* tmp_add_class_149;
  nlmsgerr* tmp_add_class_150;
  nlmsghdr* tmp_add_class_151;
  osockaddr* tmp_add_class_152;
  passwd* tmp_add_class_153;
  qelem* tmp_add_class_154;
  rlimit64* tmp_add_class_155;
  sembuf* tmp_add_class_156;
  semid_ds* tmp_add_class_157;
  seminfo* tmp_add_class_158;
  semun* tmp_add_class_159;
  sockaddr* tmp_add_class_160;
  sockaddr_in* tmp_add_class_161;
  sockaddr_in6* tmp_add_class_162;
  sockaddr_nl* tmp_add_class_163;
  sockaddr_storage* tmp_add_class_164;
  statx_timestamp* tmp_add_class_165;
  strbuf* tmp_add_class_166;
  strrecvfd* tmp_add_class_167;
  termio* tmp_add_class_168;
  ucontext_t* tmp_add_class_169;
  ucred* tmp_add_class_170;
  utsname* tmp_add_class_171;
  winsize* tmp_add_class_172;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/MZW_M94iBY/dump1.h"

