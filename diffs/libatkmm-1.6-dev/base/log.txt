Temporary header file '/tmp/EgMTdo2Nbw/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/atkmm-1.6/atkmm/action.h"
  #include "/usr/include/atkmm-1.6/atkmm/component.h"
  #include "/usr/include/atkmm-1.6/atkmm/document.h"
  #include "/usr/include/atkmm-1.6/atkmm/editabletext.h"
  #include "/usr/include/atkmm-1.6/atkmm/hyperlink.h"
  #include "/usr/include/atkmm-1.6/atkmm/hypertext.h"
  #include "/usr/include/atkmm-1.6/atkmm/image.h"
  #include "/usr/include/atkmm-1.6/atkmm/implementor.h"
  #include "/usr/include/atkmm-1.6/atkmm/init.h"
  #include "/usr/include/atkmm-1.6/atkmm/noopobject.h"
  #include "/usr/include/atkmm-1.6/atkmm/object.h"
  #include "/usr/include/atkmm-1.6/atkmm/objectaccessible.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/action_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/component_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/document_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/editabletext_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/hyperlink_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/hypertext_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/image_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/implementor_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/noopobject_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/object_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/objectaccessible_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/range_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/relation_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/relationset_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/selection_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/stateset_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/table_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/text_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/private/value_p.h"
  #include "/usr/include/atkmm-1.6/atkmm/range.h"
  #include "/usr/include/atkmm-1.6/atkmm/relation.h"
  #include "/usr/include/atkmm-1.6/atkmm/relationset.h"
  #include "/usr/include/atkmm-1.6/atkmm/selection.h"
  #include "/usr/include/atkmm-1.6/atkmm/stateset.h"
  #include "/usr/include/atkmm-1.6/atkmm/streamablecontent.h"
  #include "/usr/include/atkmm-1.6/atkmm/table.h"
  #include "/usr/include/atkmm-1.6/atkmm/text.h"
  #include "/usr/include/atkmm-1.6/atkmm/value.h"
  #include "/usr/include/atkmm-1.6/atkmm/wrap_init.h"
  #include "/usr/include/atkmm-1.6/atkmm.h"
  #include "/usr/lib/arm-linux-gnueabihf/atkmm-1.6/include/atkmmconfig.h"

  // add namespaces
  namespace Atk{typedef int tmp_add_type_1;}
  Atk::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace Glib{typedef int tmp_add_type_2;}
  Glib::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace Glib{namespace Ascii{typedef int tmp_add_type_3;}}
  Glib::Ascii::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace Glib{namespace detail{typedef int tmp_add_type_4;}}
  Glib::detail::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace Glib{namespace Unicode{typedef int tmp_add_type_5;}}
  Glib::Unicode::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace sigc{typedef int tmp_add_type_6;}
  sigc::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace sigc{namespace internal{typedef int tmp_add_type_7;}}
  sigc::internal::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace Glib{namespace Container_Helpers{typedef int tmp_add_type_8;}}
  Glib::Container_Helpers::tmp_add_type_8 tmp_add_func_8(){return 0;};

  // add classes
  _AtkActionIface* tmp_add_class_0;
  _AtkAttribute* tmp_add_class_1;
  _AtkComponentIface* tmp_add_class_2;
  _AtkDocumentIface* tmp_add_class_3;
  _AtkEditableTextIface* tmp_add_class_4;
  _AtkGObjectAccessible* tmp_add_class_5;
  _AtkGObjectAccessibleClass* tmp_add_class_6;
  _AtkHyperlink* tmp_add_class_7;
  _AtkHyperlinkClass* tmp_add_class_8;
  _AtkHyperlinkImplIface* tmp_add_class_9;
  _AtkHypertextIface* tmp_add_class_10;
  _AtkImageIface* tmp_add_class_11;
  _AtkImplementorIface* tmp_add_class_12;
  _AtkKeyEventStruct* tmp_add_class_13;
  _AtkMisc* tmp_add_class_14;
  _AtkMiscClass* tmp_add_class_15;
  _AtkNoOpObject* tmp_add_class_16;
  _AtkNoOpObjectClass* tmp_add_class_17;
  _AtkNoOpObjectFactory* tmp_add_class_18;
  _AtkNoOpObjectFactoryClass* tmp_add_class_19;
  _AtkObject* tmp_add_class_20;
  _AtkObjectClass* tmp_add_class_21;
  _AtkObjectFactory* tmp_add_class_22;
  _AtkObjectFactoryClass* tmp_add_class_23;
  _AtkPlug* tmp_add_class_24;
  _AtkPlugClass* tmp_add_class_25;
  _AtkPropertyValues* tmp_add_class_26;
  _AtkRectangle* tmp_add_class_27;
  _AtkRegistry* tmp_add_class_28;
  _AtkRegistryClass* tmp_add_class_29;
  _AtkRelation* tmp_add_class_30;
  _AtkRelationClass* tmp_add_class_31;
  _AtkRelationSet* tmp_add_class_32;
  _AtkRelationSetClass* tmp_add_class_33;
  _AtkSelectionIface* tmp_add_class_34;
  _AtkSocket* tmp_add_class_35;
  _AtkSocketClass* tmp_add_class_36;
  _AtkStateSet* tmp_add_class_37;
  _AtkStateSetClass* tmp_add_class_38;
  _AtkStreamableContentIface* tmp_add_class_39;
  _AtkTableCellIface* tmp_add_class_40;
  _AtkTableIface* tmp_add_class_41;
  _AtkTextIface* tmp_add_class_42;
  _AtkTextRange* tmp_add_class_43;
  _AtkTextRectangle* tmp_add_class_44;
  _AtkUtil* tmp_add_class_45;
  _AtkUtilClass* tmp_add_class_46;
  _AtkValueIface* tmp_add_class_47;
  _AtkWindowIface* tmp_add_class_48;
  _GArray* tmp_add_class_49;
  _GByteArray* tmp_add_class_50;
  _GCClosure* tmp_add_class_51;
  _GClosure* tmp_add_class_52;
  _GClosureNotifyData* tmp_add_class_53;
  _GCompletion* tmp_add_class_54;
  _GCond* tmp_add_class_55;
  _GDate* tmp_add_class_56;
  _GDebugKey* tmp_add_class_57;
  _GDoubleIEEE754* tmp_add_class_58;
  _GEnumClass* tmp_add_class_59;
  _GEnumValue* tmp_add_class_60;
  _GError* tmp_add_class_61;
  _GFlagsClass* tmp_add_class_62;
  _GFlagsValue* tmp_add_class_63;
  _GFloatIEEE754* tmp_add_class_64;
  _GHashTableIter* tmp_add_class_65;
  _GHook* tmp_add_class_66;
  _GHookList* tmp_add_class_67;
  _GIOChannel* tmp_add_class_68;
  _GIOFuncs* tmp_add_class_69;
  _GInterfaceInfo* tmp_add_class_70;
  _GList* tmp_add_class_71;
  _GLogField* tmp_add_class_72;
  _GMarkupParser* tmp_add_class_73;
  _GMemVTable* tmp_add_class_74;
  _GMutex* tmp_add_class_75;
  _GNode* tmp_add_class_76;
  _GObject* tmp_add_class_77;
  _GObjectClass* tmp_add_class_78;
  _GObjectConstructParam* tmp_add_class_79;
  _GOnce* tmp_add_class_80;
  _GOptionEntry* tmp_add_class_81;
  _GParamSpec* tmp_add_class_82;
  _GParamSpecBoolean* tmp_add_class_83;
  _GParamSpecBoxed* tmp_add_class_84;
  _GParamSpecChar* tmp_add_class_85;
  _GParamSpecClass* tmp_add_class_86;
  _GParamSpecDouble* tmp_add_class_87;
  _GParamSpecEnum* tmp_add_class_88;
  _GParamSpecFlags* tmp_add_class_89;
  _GParamSpecFloat* tmp_add_class_90;
  _GParamSpecGType* tmp_add_class_91;
  _GParamSpecInt* tmp_add_class_92;
  _GParamSpecInt64* tmp_add_class_93;
  _GParamSpecLong* tmp_add_class_94;
  _GParamSpecObject* tmp_add_class_95;
  _GParamSpecOverride* tmp_add_class_96;
  _GParamSpecParam* tmp_add_class_97;
  _GParamSpecPointer* tmp_add_class_98;
  _GParamSpecString* tmp_add_class_99;
  _GParamSpecTypeInfo* tmp_add_class_100;
  _GParamSpecUChar* tmp_add_class_101;
  _GParamSpecUInt* tmp_add_class_102;
  _GParamSpecUInt64* tmp_add_class_103;
  _GParamSpecULong* tmp_add_class_104;
  _GParamSpecUnichar* tmp_add_class_105;
  _GParamSpecValueArray* tmp_add_class_106;
  _GParamSpecVariant* tmp_add_class_107;
  _GParameter* tmp_add_class_108;
  _GPathBuf* tmp_add_class_109;
  _GPollFD* tmp_add_class_110;
  _GPrivate* tmp_add_class_111;
  _GPtrArray* tmp_add_class_112;
  _GQueue* tmp_add_class_113;
  _GRWLock* tmp_add_class_114;
  _GRecMutex* tmp_add_class_115;
  _GSList* tmp_add_class_116;
  _GScanner* tmp_add_class_117;
  _GScannerConfig* tmp_add_class_118;
  _GSignalInvocationHint* tmp_add_class_119;
  _GSignalQuery* tmp_add_class_120;
  _GSource* tmp_add_class_121;
  _GSourceCallbackFuncs* tmp_add_class_122;
  _GSourceFuncs* tmp_add_class_123;
  _GStaticPrivate* tmp_add_class_124;
  _GStaticRWLock* tmp_add_class_125;
  _GStaticRecMutex* tmp_add_class_126;
  _GString* tmp_add_class_127;
  _GThread* tmp_add_class_128;
  _GThreadFunctions* tmp_add_class_129;
  _GThreadPool* tmp_add_class_130;
  _GTimeVal* tmp_add_class_131;
  _GTokenValue* tmp_add_class_132;
  _GTrashStack* tmp_add_class_133;
  _GTuples* tmp_add_class_134;
  _GTypeClass* tmp_add_class_135;
  _GTypeFundamentalInfo* tmp_add_class_136;
  _GTypeInfo* tmp_add_class_137;
  _GTypeInstance* tmp_add_class_138;
  _GTypeInterface* tmp_add_class_139;
  _GTypeModule* tmp_add_class_140;
  _GTypeModuleClass* tmp_add_class_141;
  _GTypePluginClass* tmp_add_class_142;
  _GTypeQuery* tmp_add_class_143;
  _GTypeValueTable* tmp_add_class_144;
  _GUriParamsIter* tmp_add_class_145;
  _GValue* tmp_add_class_146;
  _GValueArray* tmp_add_class_147;
  _GVariantBuilder* tmp_add_class_148;
  _GVariantDict* tmp_add_class_149;
  _GVariantIter* tmp_add_class_150;
  _G_fpos64_t* tmp_add_class_151;
  _G_fpos_t* tmp_add_class_152;
  _IO_cookie_io_functions_t* tmp_add_class_153;
  ucontext_t* tmp_add_class_154;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/EgMTdo2Nbw/dump1.h"  -I/usr/include/atkmm-1.6 -I/usr/lib/arm-linux-gnueabihf/atkmm-1.6/include -I/usr/include/sigc\+\+-2.0 -I/usr/include/glibmm-2.4 -I/usr/include/glib-2.0 -I/usr/include/atk-1.0 -I/lib/arm-linux-gnueabihf/sigc\+\+-2.0/include -I/lib/arm-linux-gnueabihf/glibmm-2.4/include -I/lib/arm-linux-gnueabihf/glib-2.0/include

