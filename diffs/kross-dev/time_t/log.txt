Temporary header file '/tmp/GaiWsTx03j/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/KF5/KrossCore/kross/core/action.h"
  #include "/usr/include/KF5/KrossCore/kross/core/actioncollection.h"
  #include "/usr/include/KF5/KrossCore/kross/core/childreninterface.h"
  #include "/usr/include/KF5/KrossCore/kross/core/errorinterface.h"
  #include "/usr/include/KF5/KrossCore/kross/core/interpreter.h"
  #include "/usr/include/KF5/KrossCore/kross/core/krossconfig.h"
  #include "/usr/include/KF5/KrossCore/kross/core/krosscore_export.h"
  #include "/usr/include/KF5/KrossCore/kross/core/manager.h"
  #include "/usr/include/KF5/KrossCore/kross/core/metafunction.h"
  #include "/usr/include/KF5/KrossCore/kross/core/metatype.h"
  #include "/usr/include/KF5/KrossCore/kross/core/object.h"
  #include "/usr/include/KF5/KrossCore/kross/core/script.h"
  #include "/usr/include/KF5/KrossCore/kross/core/wrapperinterface.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/actioncollectionmodel.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/actioncollectionview.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/krossui_export.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/model.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/plugin.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/scriptingplugin.h"
  #include "/usr/include/KF5/KrossUi/kross/ui/view.h"
  #include "/usr/include/KF5/kross_version.h"

  // add namespaces
  namespace KDEPrivate{typedef int tmp_add_type_1;}
  KDEPrivate::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace KParts{typedef int tmp_add_type_2;}
  KParts::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace Kross{typedef int tmp_add_type_3;}
  Kross::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_4;}
  QAlgorithmsPrivate::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_5;}
  QColorConstants::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_6;}}
  QColorConstants::Svg::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace Qt{typedef int tmp_add_type_7;}
  Qt::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_8;}
  QtGlobalStatic::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_9;}
  QtMetaTypePrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_10;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_11;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_12;}
  QtPrivate::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_13;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_14;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_15;}
  QtSharedPointer::tmp_add_type_15 tmp_add_func_15(){return 0;};

  // add classes
  KXMLGUIClient* tmp_add_class_0;
  QAbstractItemDelegate* tmp_add_class_1;
  QAbstractItemModel* tmp_add_class_2;
  QAbstractItemView* tmp_add_class_3;
  QAbstractListModel* tmp_add_class_4;
  QAbstractProxyModel* tmp_add_class_5;
  QAbstractScrollArea* tmp_add_class_6;
  QAbstractSlider* tmp_add_class_7;
  QAbstractSpinBox* tmp_add_class_8;
  QAbstractTableModel* tmp_add_class_9;
  QAction* tmp_add_class_10;
  QActionGroup* tmp_add_class_11;
  QArrayData* tmp_add_class_12;
  QAssociativeIterable* tmp_add_class_13;
  QAtomicInt* tmp_add_class_14;
  QBrush* tmp_add_class_15;
  QBrushData* tmp_add_class_16;
  QByteArray* tmp_add_class_17;
  QByteArrayDataPtr* tmp_add_class_18;
  QByteRef* tmp_add_class_19;
  QChar* tmp_add_class_20;
  QCharRef* tmp_add_class_21;
  QColor* tmp_add_class_22;
  QConicalGradient* tmp_add_class_23;
  QCursor* tmp_add_class_24;
  QDataStream* tmp_add_class_25;
  QDir* tmp_add_class_26;
  QDomAttr* tmp_add_class_27;
  QDomCDATASection* tmp_add_class_28;
  QDomCharacterData* tmp_add_class_29;
  QDomComment* tmp_add_class_30;
  QDomDocument* tmp_add_class_31;
  QDomDocumentFragment* tmp_add_class_32;
  QDomDocumentType* tmp_add_class_33;
  QDomElement* tmp_add_class_34;
  QDomEntity* tmp_add_class_35;
  QDomEntityReference* tmp_add_class_36;
  QDomImplementation* tmp_add_class_37;
  QDomNamedNodeMap* tmp_add_class_38;
  QDomNode* tmp_add_class_39;
  QDomNodeList* tmp_add_class_40;
  QDomNotation* tmp_add_class_41;
  QDomProcessingInstruction* tmp_add_class_42;
  QDomText* tmp_add_class_43;
  QDoubleValidator* tmp_add_class_44;
  QFile* tmp_add_class_45;
  QFileDevice* tmp_add_class_46;
  QFileInfo* tmp_add_class_47;
  QFlag* tmp_add_class_48;
  QFont* tmp_add_class_49;
  QFontInfo* tmp_add_class_50;
  QFontMetrics* tmp_add_class_51;
  QFontMetricsF* tmp_add_class_52;
  QFrame* tmp_add_class_53;
  QGenericArgument* tmp_add_class_54;
  QGenericReturnArgument* tmp_add_class_55;
  QGradient* tmp_add_class_56;
  QHashData* tmp_add_class_57;
  QHashDummyValue* tmp_add_class_58;
  QIODevice* tmp_add_class_59;
  QIcon* tmp_add_class_60;
  QImage* tmp_add_class_61;
  QIncompatibleFlag* tmp_add_class_62;
  QIntValidator* tmp_add_class_63;
  QInternal* tmp_add_class_64;
  QItemSelection* tmp_add_class_65;
  QItemSelectionModel* tmp_add_class_66;
  QItemSelectionRange* tmp_add_class_67;
  QKeySequence* tmp_add_class_68;
  QLatin1Char* tmp_add_class_69;
  QLatin1String* tmp_add_class_70;
  QLine* tmp_add_class_71;
  QLineF* tmp_add_class_72;
  QLinearGradient* tmp_add_class_73;
  QListData* tmp_add_class_74;
  QLocale* tmp_add_class_75;
  QMapDataBase* tmp_add_class_76;
  QMapNodeBase* tmp_add_class_77;
  QMargins* tmp_add_class_78;
  QMarginsF* tmp_add_class_79;
  QMatrix* tmp_add_class_80;
  QMessageLogContext* tmp_add_class_81;
  QMessageLogger* tmp_add_class_82;
  QMetaObject* tmp_add_class_83;
  QMetaType* tmp_add_class_84;
  QModelIndex* tmp_add_class_85;
  QObject* tmp_add_class_86;
  QObjectData* tmp_add_class_87;
  QObjectUserData* tmp_add_class_88;
  QPaintDevice* tmp_add_class_89;
  QPalette* tmp_add_class_90;
  QPersistentModelIndex* tmp_add_class_91;
  QPixelFormat* tmp_add_class_92;
  QPixmap* tmp_add_class_93;
  QPoint* tmp_add_class_94;
  QPointF* tmp_add_class_95;
  QPolygon* tmp_add_class_96;
  QPolygonF* tmp_add_class_97;
  QRadialGradient* tmp_add_class_98;
  QRect* tmp_add_class_99;
  QRectF* tmp_add_class_100;
  QRegExp* tmp_add_class_101;
  QRegExpValidator* tmp_add_class_102;
  QRegion* tmp_add_class_103;
  QRegularExpression* tmp_add_class_104;
  QRegularExpressionMatch* tmp_add_class_105;
  QRegularExpressionMatchIterator* tmp_add_class_106;
  QRegularExpressionValidator* tmp_add_class_107;
  QRgba64* tmp_add_class_108;
  QRubberBand* tmp_add_class_109;
  QScopedPointerPodDeleter* tmp_add_class_110;
  QScriptable* tmp_add_class_111;
  QSequentialIterable* tmp_add_class_112;
  QSharedData* tmp_add_class_113;
  QSignalBlocker* tmp_add_class_114;
  QSize* tmp_add_class_115;
  QSizeF* tmp_add_class_116;
  QSizePolicy* tmp_add_class_117;
  QSlider* tmp_add_class_118;
  QSortFilterProxyModel* tmp_add_class_119;
  QString* tmp_add_class_120;
  QStringDataPtr* tmp_add_class_121;
  QStringList* tmp_add_class_122;
  QStringMatcher* tmp_add_class_123;
  QStringRef* tmp_add_class_124;
  QStringView* tmp_add_class_125;
  QStyle* tmp_add_class_126;
  QStyleHintReturn* tmp_add_class_127;
  QStyleHintReturnMask* tmp_add_class_128;
  QStyleHintReturnVariant* tmp_add_class_129;
  QStyleOption* tmp_add_class_130;
  QStyleOptionButton* tmp_add_class_131;
  QStyleOptionComboBox* tmp_add_class_132;
  QStyleOptionComplex* tmp_add_class_133;
  QStyleOptionDockWidget* tmp_add_class_134;
  QStyleOptionFocusRect* tmp_add_class_135;
  QStyleOptionFrame* tmp_add_class_136;
  QStyleOptionGraphicsItem* tmp_add_class_137;
  QStyleOptionGroupBox* tmp_add_class_138;
  QStyleOptionHeader* tmp_add_class_139;
  QStyleOptionMenuItem* tmp_add_class_140;
  QStyleOptionProgressBar* tmp_add_class_141;
  QStyleOptionRubberBand* tmp_add_class_142;
  QStyleOptionSizeGrip* tmp_add_class_143;
  QStyleOptionSlider* tmp_add_class_144;
  QStyleOptionSpinBox* tmp_add_class_145;
  QStyleOptionTab* tmp_add_class_146;
  QStyleOptionTabBarBase* tmp_add_class_147;
  QStyleOptionTabV4* tmp_add_class_148;
  QStyleOptionTabWidgetFrame* tmp_add_class_149;
  QStyleOptionTitleBar* tmp_add_class_150;
  QStyleOptionToolBar* tmp_add_class_151;
  QStyleOptionToolBox* tmp_add_class_152;
  QStyleOptionToolButton* tmp_add_class_153;
  QStyleOptionViewItem* tmp_add_class_154;
  QSysInfo* tmp_add_class_155;
  QTabBar* tmp_add_class_156;
  QTabWidget* tmp_add_class_157;
  QTransform* tmp_add_class_158;
  QTreeView* tmp_add_class_159;
  QUrl* tmp_add_class_160;
  QValidator* tmp_add_class_161;
  QVariant* tmp_add_class_162;
  QVariantComparisonHelper* tmp_add_class_163;
  QWidget* tmp_add_class_164;
  QWidgetData* tmp_add_class_165;
  _G_fpos64_t* tmp_add_class_166;
  _G_fpos_t* tmp_add_class_167;
  _IO_cookie_io_functions_t* tmp_add_class_168;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/GaiWsTx03j/dump1.h"  -I/usr/include/KF5/KrossUi -I/usr/include/KF5/KrossCore -I/usr/include/KF5/KXmlGui -I/usr/include/KF5/KParts -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtXml -I/usr/include/arm-linux-gnueabihf/qt5/QtWidgets -I/usr/include/arm-linux-gnueabihf/qt5/QtScript -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

