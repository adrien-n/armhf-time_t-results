Temporary header file '/tmp/tr8CrbCGHj/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/agg2/agg_renderer_base.h"
  #include "/usr/include/agg2/agg_alpha_mask_u8.h"
  #include "/usr/include/agg2/agg_arc.h"
  #include "/usr/include/agg2/agg_array.h"
  #include "/usr/include/agg2/agg_arrowhead.h"
  #include "/usr/include/agg2/agg_basics.h"
  #include "/usr/include/agg2/agg_bezier_arc.h"
  #include "/usr/include/agg2/agg_bitset_iterator.h"
  #include "/usr/include/agg2/agg_blur.h"
  #include "/usr/include/agg2/agg_bounding_rect.h"
  #include "/usr/include/agg2/agg_bspline.h"
  #include "/usr/include/agg2/agg_clip_liang_barsky.h"
  #include "/usr/include/agg2/agg_color_gray.h"
  #include "/usr/include/agg2/agg_color_rgba.h"
  #include "/usr/include/agg2/agg_config.h"
  #include "/usr/include/agg2/agg_conv_adaptor_vcgen.h"
  #include "/usr/include/agg2/agg_conv_adaptor_vpgen.h"
  #include "/usr/include/agg2/agg_conv_bspline.h"
  #include "/usr/include/agg2/agg_conv_clip_polygon.h"
  #include "/usr/include/agg2/agg_conv_clip_polyline.h"
  #include "/usr/include/agg2/agg_conv_close_polygon.h"
  #include "/usr/include/agg2/agg_conv_concat.h"
  #include "/usr/include/agg2/agg_conv_contour.h"
  #include "/usr/include/agg2/agg_conv_curve.h"
  #include "/usr/include/agg2/agg_conv_dash.h"
  #include "/usr/include/agg2/agg_conv_marker.h"
  #include "/usr/include/agg2/agg_conv_marker_adaptor.h"
  #include "/usr/include/agg2/agg_conv_segmentator.h"
  #include "/usr/include/agg2/agg_conv_shorten_path.h"
  #include "/usr/include/agg2/agg_conv_smooth_poly1.h"
  #include "/usr/include/agg2/agg_conv_stroke.h"
  #include "/usr/include/agg2/agg_conv_transform.h"
  #include "/usr/include/agg2/agg_conv_unclose_polygon.h"
  #include "/usr/include/agg2/agg_curves.h"
  #include "/usr/include/agg2/agg_dda_line.h"
  #include "/usr/include/agg2/agg_ellipse.h"
  #include "/usr/include/agg2/agg_ellipse_bresenham.h"
  #include "/usr/include/agg2/agg_embedded_raster_fonts.h"
  #include "/usr/include/agg2/agg_font_cache_manager.h"
  #include "/usr/include/agg2/agg_font_cache_manager2.h"
  #include "/usr/include/agg2/agg_font_freetype.h"
  #include "/usr/include/agg2/agg_gamma_functions.h"
  #include "/usr/include/agg2/agg_gamma_lut.h"
  #include "/usr/include/agg2/agg_glyph_raster_bin.h"
  #include "/usr/include/agg2/agg_gradient_lut.h"
  #include "/usr/include/agg2/agg_gsv_text.h"
  #include "/usr/include/agg2/agg_image_accessors.h"
  #include "/usr/include/agg2/agg_image_filters.h"
  #include "/usr/include/agg2/agg_line_aa_basics.h"
  #include "/usr/include/agg2/agg_math.h"
  #include "/usr/include/agg2/agg_math_stroke.h"
  #include "/usr/include/agg2/agg_path_length.h"
  #include "/usr/include/agg2/agg_path_storage.h"
  #include "/usr/include/agg2/agg_path_storage_integer.h"
  #include "/usr/include/agg2/agg_pattern_filters_rgba.h"
  #include "/usr/include/agg2/agg_pixfmt_amask_adaptor.h"
  #include "/usr/include/agg2/agg_pixfmt_base.h"
  #include "/usr/include/agg2/agg_pixfmt_gray.h"
  #include "/usr/include/agg2/agg_pixfmt_rgb.h"
  #include "/usr/include/agg2/agg_pixfmt_rgb_packed.h"
  #include "/usr/include/agg2/agg_pixfmt_rgba.h"
  #include "/usr/include/agg2/agg_pixfmt_transposer.h"
  #include "/usr/include/agg2/agg_rasterizer_cells_aa.h"
  #include "/usr/include/agg2/agg_rasterizer_compound_aa.h"
  #include "/usr/include/agg2/agg_rasterizer_outline.h"
  #include "/usr/include/agg2/agg_rasterizer_outline_aa.h"
  #include "/usr/include/agg2/agg_rasterizer_scanline_aa.h"
  #include "/usr/include/agg2/agg_rasterizer_scanline_aa_nogamma.h"
  #include "/usr/include/agg2/agg_rasterizer_sl_clip.h"
  #include "/usr/include/agg2/agg_renderer_markers.h"
  #include "/usr/include/agg2/agg_renderer_mclip.h"
  #include "/usr/include/agg2/agg_renderer_outline_aa.h"
  #include "/usr/include/agg2/agg_renderer_outline_image.h"
  #include "/usr/include/agg2/agg_renderer_primitives.h"
  #include "/usr/include/agg2/agg_renderer_raster_text.h"
  #include "/usr/include/agg2/agg_renderer_scanline.h"
  #include "/usr/include/agg2/agg_rendering_buffer.h"
  #include "/usr/include/agg2/agg_rendering_buffer_dynarow.h"
  #include "/usr/include/agg2/agg_rounded_rect.h"
  #include "/usr/include/agg2/agg_scanline_bin.h"
  #include "/usr/include/agg2/agg_scanline_boolean_algebra.h"
  #include "/usr/include/agg2/agg_scanline_p.h"
  #include "/usr/include/agg2/agg_scanline_storage_aa.h"
  #include "/usr/include/agg2/agg_scanline_storage_bin.h"
  #include "/usr/include/agg2/agg_scanline_u.h"
  #include "/usr/include/agg2/agg_shorten_path.h"
  #include "/usr/include/agg2/agg_simul_eq.h"
  #include "/usr/include/agg2/agg_span_allocator.h"
  #include "/usr/include/agg2/agg_span_converter.h"
  #include "/usr/include/agg2/agg_span_gouraud.h"
  #include "/usr/include/agg2/agg_span_gouraud_gray.h"
  #include "/usr/include/agg2/agg_span_gouraud_rgba.h"
  #include "/usr/include/agg2/agg_span_gradient.h"
  #include "/usr/include/agg2/agg_span_gradient_alpha.h"
  #include "/usr/include/agg2/agg_span_gradient_contour.h"
  #include "/usr/include/agg2/agg_span_gradient_image.h"
  #include "/usr/include/agg2/agg_span_image_filter.h"
  #include "/usr/include/agg2/agg_span_image_filter_gray.h"
  #include "/usr/include/agg2/agg_span_image_filter_rgb.h"
  #include "/usr/include/agg2/agg_span_image_filter_rgba.h"
  #include "/usr/include/agg2/agg_span_interpolator_adaptor.h"
  #include "/usr/include/agg2/agg_span_interpolator_linear.h"
  #include "/usr/include/agg2/agg_span_interpolator_persp.h"
  #include "/usr/include/agg2/agg_span_interpolator_trans.h"
  #include "/usr/include/agg2/agg_span_pattern_gray.h"
  #include "/usr/include/agg2/agg_span_pattern_rgb.h"
  #include "/usr/include/agg2/agg_span_pattern_rgba.h"
  #include "/usr/include/agg2/agg_span_solid.h"
  #include "/usr/include/agg2/agg_span_subdiv_adaptor.h"
  #include "/usr/include/agg2/agg_trans_affine.h"
  #include "/usr/include/agg2/agg_trans_bilinear.h"
  #include "/usr/include/agg2/agg_trans_double_path.h"
  #include "/usr/include/agg2/agg_trans_perspective.h"
  #include "/usr/include/agg2/agg_trans_single_path.h"
  #include "/usr/include/agg2/agg_trans_viewport.h"
  #include "/usr/include/agg2/agg_trans_warp_magnifier.h"
  #include "/usr/include/agg2/agg_vcgen_bspline.h"
  #include "/usr/include/agg2/agg_vcgen_contour.h"
  #include "/usr/include/agg2/agg_vcgen_dash.h"
  #include "/usr/include/agg2/agg_vcgen_markers_term.h"
  #include "/usr/include/agg2/agg_vcgen_smooth_poly1.h"
  #include "/usr/include/agg2/agg_vcgen_stroke.h"
  #include "/usr/include/agg2/agg_vcgen_vertex_sequence.h"
  #include "/usr/include/agg2/agg_vertex_sequence.h"
  #include "/usr/include/agg2/agg_vpgen_clip_polygon.h"
  #include "/usr/include/agg2/agg_vpgen_clip_polyline.h"
  #include "/usr/include/agg2/agg_vpgen_segmentator.h"
  #include "/usr/include/agg2/ctrl/agg_bezier_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_cbox_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_gamma_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_gamma_spline.h"
  #include "/usr/include/agg2/ctrl/agg_polygon_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_rbox_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_scale_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_slider_ctrl.h"
  #include "/usr/include/agg2/ctrl/agg_spline_ctrl.h"
  #include "/usr/include/agg2/platform/agg_platform_support.h"
  #include "/usr/include/agg2/util/agg_color_conv.h"
  #include "/usr/include/agg2/util/agg_color_conv_rgb16.h"
  #include "/usr/include/agg2/util/agg_color_conv_rgb8.h"

  // add namespaces
  namespace agg{typedef int tmp_add_type_1;}
  agg::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace agg{namespace fman{typedef int tmp_add_type_2;}}
  agg::fman::tmp_add_type_2 tmp_add_func_2(){return 0;};

  // add classes
  FT_BBox_* tmp_add_class_0;
  FT_Bitmap_* tmp_add_class_1;
  FT_Bitmap_Size_* tmp_add_class_2;
  FT_CharMapRec_* tmp_add_class_3;
  FT_Data_* tmp_add_class_4;
  FT_FaceRec_* tmp_add_class_5;
  FT_Generic_* tmp_add_class_6;
  FT_GlyphSlotRec_* tmp_add_class_7;
  FT_Glyph_Metrics_* tmp_add_class_8;
  FT_ListNodeRec_* tmp_add_class_9;
  FT_ListRec_* tmp_add_class_10;
  FT_Matrix_* tmp_add_class_11;
  FT_MemoryRec_* tmp_add_class_12;
  FT_Open_Args_* tmp_add_class_13;
  FT_Outline_* tmp_add_class_14;
  FT_Outline_Funcs_* tmp_add_class_15;
  FT_Parameter_* tmp_add_class_16;
  FT_Raster_Funcs_* tmp_add_class_17;
  FT_Raster_Params_* tmp_add_class_18;
  FT_SizeRec_* tmp_add_class_19;
  FT_Size_Metrics_* tmp_add_class_20;
  FT_Size_RequestRec_* tmp_add_class_21;
  FT_Span_* tmp_add_class_22;
  FT_StreamDesc_* tmp_add_class_23;
  FT_StreamRec_* tmp_add_class_24;
  FT_UnitVector_* tmp_add_class_25;
  FT_Vector_* tmp_add_class_26;
  _G_fpos64_t* tmp_add_class_27;
  _G_fpos_t* tmp_add_class_28;
  _IO_cookie_io_functions_t* tmp_add_class_29;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/tr8CrbCGHj/dump1.h"  -I/usr/include/agg2 -I/usr/include/freetype2

