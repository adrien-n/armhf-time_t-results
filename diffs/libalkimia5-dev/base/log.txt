Temporary header file '/tmp/sTo5DWa5MP/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/alkimia/Qt5/alkimia/alk_export.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkcompany.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkonlinequote.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkonlinequotesource.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkonlinequotesprofile.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkonlinequotesprofilemanager.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkonlinequoteswidget.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkvalue.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkversion.h"
  #include "/usr/include/alkimia/Qt5/alkimia/alkwebpage.h"

  // add namespaces
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_1;}
  QAlgorithmsPrivate::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_2;}
  QColorConstants::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_3;}}
  QColorConstants::Svg::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace Qt{typedef int tmp_add_type_4;}
  Qt::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_5;}
  QtGlobalStatic::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_6;}
  QtMetaTypePrivate::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_7;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_8;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_9;}
  QtPrivate::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_10;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_11;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_12;}
  QtSharedPointer::tmp_add_type_12 tmp_add_func_12(){return 0;};

  // add classes
  AlkCompany* tmp_add_class_0;
  AlkOnlineQuote* tmp_add_class_1;
  AlkOnlineQuoteSource* tmp_add_class_2;
  AlkOnlineQuotesProfile* tmp_add_class_3;
  AlkOnlineQuotesProfileManager* tmp_add_class_4;
  AlkOnlineQuotesWidget* tmp_add_class_5;
  AlkValue* tmp_add_class_6;
  AlkWebPage* tmp_add_class_7;
  QAbstractScrollArea* tmp_add_class_8;
  QAbstractUndoItem* tmp_add_class_9;
  QArrayData* tmp_add_class_10;
  QAssociativeIterable* tmp_add_class_11;
  QAtomicInt* tmp_add_class_12;
  QBrush* tmp_add_class_13;
  QBrushData* tmp_add_class_14;
  QByteArray* tmp_add_class_15;
  QByteArrayDataPtr* tmp_add_class_16;
  QByteRef* tmp_add_class_17;
  QChar* tmp_add_class_18;
  QCharRef* tmp_add_class_19;
  QColor* tmp_add_class_20;
  QConicalGradient* tmp_add_class_21;
  QCursor* tmp_add_class_22;
  QDBusArgument* tmp_add_class_23;
  QDBusObjectPath* tmp_add_class_24;
  QDBusSignature* tmp_add_class_25;
  QDBusVariant* tmp_add_class_26;
  QDataStream* tmp_add_class_27;
  QDate* tmp_add_class_28;
  QDateTime* tmp_add_class_29;
  QFlag* tmp_add_class_30;
  QFont* tmp_add_class_31;
  QFontInfo* tmp_add_class_32;
  QFontMetrics* tmp_add_class_33;
  QFontMetricsF* tmp_add_class_34;
  QFrame* tmp_add_class_35;
  QGenericArgument* tmp_add_class_36;
  QGenericReturnArgument* tmp_add_class_37;
  QGradient* tmp_add_class_38;
  QHashData* tmp_add_class_39;
  QHashDummyValue* tmp_add_class_40;
  QIODevice* tmp_add_class_41;
  QImage* tmp_add_class_42;
  QIncompatibleFlag* tmp_add_class_43;
  QInternal* tmp_add_class_44;
  QKeySequence* tmp_add_class_45;
  QLatin1Char* tmp_add_class_46;
  QLatin1String* tmp_add_class_47;
  QLine* tmp_add_class_48;
  QLineF* tmp_add_class_49;
  QLinearGradient* tmp_add_class_50;
  QListData* tmp_add_class_51;
  QMapDataBase* tmp_add_class_52;
  QMapNodeBase* tmp_add_class_53;
  QMargins* tmp_add_class_54;
  QMarginsF* tmp_add_class_55;
  QMatrix* tmp_add_class_56;
  QMessageLogContext* tmp_add_class_57;
  QMessageLogger* tmp_add_class_58;
  QMetaObject* tmp_add_class_59;
  QMetaType* tmp_add_class_60;
  QObject* tmp_add_class_61;
  QObjectData* tmp_add_class_62;
  QObjectUserData* tmp_add_class_63;
  QPaintDevice* tmp_add_class_64;
  QPalette* tmp_add_class_65;
  QPen* tmp_add_class_66;
  QPixelFormat* tmp_add_class_67;
  QPixmap* tmp_add_class_68;
  QPoint* tmp_add_class_69;
  QPointF* tmp_add_class_70;
  QPolygon* tmp_add_class_71;
  QPolygonF* tmp_add_class_72;
  QRadialGradient* tmp_add_class_73;
  QRect* tmp_add_class_74;
  QRectF* tmp_add_class_75;
  QRegExp* tmp_add_class_76;
  QRegion* tmp_add_class_77;
  QRgba64* tmp_add_class_78;
  QScopedPointerPodDeleter* tmp_add_class_79;
  QSequentialIterable* tmp_add_class_80;
  QSharedData* tmp_add_class_81;
  QSignalBlocker* tmp_add_class_82;
  QSize* tmp_add_class_83;
  QSizeF* tmp_add_class_84;
  QSizePolicy* tmp_add_class_85;
  QString* tmp_add_class_86;
  QStringDataPtr* tmp_add_class_87;
  QStringList* tmp_add_class_88;
  QStringMatcher* tmp_add_class_89;
  QStringRef* tmp_add_class_90;
  QStringView* tmp_add_class_91;
  QSysInfo* tmp_add_class_92;
  QTextBlockFormat* tmp_add_class_93;
  QTextBrowser* tmp_add_class_94;
  QTextCharFormat* tmp_add_class_95;
  QTextCursor* tmp_add_class_96;
  QTextDocument* tmp_add_class_97;
  QTextEdit* tmp_add_class_98;
  QTextFormat* tmp_add_class_99;
  QTextFrameFormat* tmp_add_class_100;
  QTextImageFormat* tmp_add_class_101;
  QTextLength* tmp_add_class_102;
  QTextListFormat* tmp_add_class_103;
  QTextOption* tmp_add_class_104;
  QTextTableCellFormat* tmp_add_class_105;
  QTextTableFormat* tmp_add_class_106;
  QTime* tmp_add_class_107;
  QTransform* tmp_add_class_108;
  QUrl* tmp_add_class_109;
  QVariant* tmp_add_class_110;
  QVariantComparisonHelper* tmp_add_class_111;
  QWidget* tmp_add_class_112;
  QWidgetData* tmp_add_class_113;
  _G_fpos64_t* tmp_add_class_114;
  _G_fpos_t* tmp_add_class_115;
  _IO_cookie_io_functions_t* tmp_add_class_116;
  gmp_randclass* tmp_add_class_117;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/sTo5DWa5MP/dump1.h"  -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/alkimia/Qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtWidgets -I/usr/include/arm-linux-gnueabihf/qt5/QtDBus -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

