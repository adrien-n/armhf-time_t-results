Temporary header file '/tmp/XdVzJf9mGw/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/astro/astr2lib.h"
  #include "/usr/include/astro/astro_version.h"
  #include "/usr/include/astro/astrolib.h"
  #include "/usr/include/astro/astrolib_export.h"
  #include "/usr/include/astro/attlib.h"
  #include "/usr/include/astro/eclsolar.h"
  #include "/usr/include/astro/planetarySats.h"
  #include "/usr/include/astro/solarsystem.h"

  // add classes
  EclSolar* tmp_add_class_0;
  Eclipse* tmp_add_class_1;
  Mat3* tmp_add_class_2;
  Moon200* tmp_add_class_3;
  Plan200* tmp_add_class_4;
  PlanetarySats* tmp_add_class_5;
  SolarSystem* tmp_add_class_6;
  Sun200* tmp_add_class_7;
  Vec3* tmp_add_class_8;
  _G_fpos64_t* tmp_add_class_9;
  _G_fpos_t* tmp_add_class_10;
  _IO_cookie_io_functions_t* tmp_add_class_11;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/XdVzJf9mGw/dump1.h"

