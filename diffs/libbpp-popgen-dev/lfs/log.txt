Temporary header file '/tmp/MjGAlo33EF/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/Bpp/PopGen/AlleleInfo.h"
  #include "/usr/include/Bpp/PopGen/BasicAlleleInfo.h"
  #include "/usr/include/Bpp/PopGen/BiAlleleMonolocusGenotype.h"
  #include "/usr/include/Bpp/PopGen/DataSet/AnalyzedLoci.h"
  #include "/usr/include/Bpp/PopGen/DataSet/AnalyzedSequences.h"
  #include "/usr/include/Bpp/PopGen/DataSet/DataSet.h"
  #include "/usr/include/Bpp/PopGen/DataSet/DataSetTools.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Date.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Group.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Individual.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/AbstractIDataSet.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/AbstractODataSet.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/Darwin/DarwinDon.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/Darwin/DarwinVarSingle.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/GeneMapper/GeneMapperCsvExport.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/Genepop/Genepop.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/Genetix/Genetix.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/IDataSet.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/IODataSet.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/ODataSet.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Io/PopgenlibIO.h"
  #include "/usr/include/Bpp/PopGen/DataSet/Locality.h"
  #include "/usr/include/Bpp/PopGen/DataSet/MultiSeqIndividual.h"
  #include "/usr/include/Bpp/PopGen/GeneralExceptions.h"
  #include "/usr/include/Bpp/PopGen/LocusInfo.h"
  #include "/usr/include/Bpp/PopGen/MonoAlleleMonolocusGenotype.h"
  #include "/usr/include/Bpp/PopGen/MonolocusGenotype.h"
  #include "/usr/include/Bpp/PopGen/MonolocusGenotypeTools.h"
  #include "/usr/include/Bpp/PopGen/MultiAlleleMonolocusGenotype.h"
  #include "/usr/include/Bpp/PopGen/MultilocusGenotype.h"
  #include "/usr/include/Bpp/PopGen/MultilocusGenotypeStatistics.h"
  #include "/usr/include/Bpp/PopGen/PolymorphismMultiGContainer.h"
  #include "/usr/include/Bpp/PopGen/PolymorphismMultiGContainerTools.h"
  #include "/usr/include/Bpp/PopGen/PolymorphismSequenceContainer.h"
  #include "/usr/include/Bpp/PopGen/PolymorphismSequenceContainerTools.h"
  #include "/usr/include/Bpp/PopGen/SequenceStatistics.h"

  // add namespaces
  namespace bpp{typedef int tmp_add_type_1;}
  bpp::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace bpp{namespace TextTools{typedef int tmp_add_type_2;}}
  bpp::TextTools::tmp_add_type_2 tmp_add_func_2(){return 0;};

  // add classes
  _G_fpos64_t* tmp_add_class_0;
  _G_fpos_t* tmp_add_class_1;
  _IO_cookie_io_functions_t* tmp_add_class_2;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/MjGAlo33EF/dump1.h"

