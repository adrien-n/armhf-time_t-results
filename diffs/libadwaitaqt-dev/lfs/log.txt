Temporary header file '/tmp/gPC7e8CK0y/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/AdwaitaQt/adwaita.h"
  #include "/usr/include/AdwaitaQt/adwaitacolors.h"
  #include "/usr/include/AdwaitaQt/adwaitaqt_export.h"
  #include "/usr/include/AdwaitaQt/adwaitarenderer.h"

  // add namespaces
  namespace Adwaita{typedef int tmp_add_type_1;}
  Adwaita::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace Adwaita{namespace Config{typedef int tmp_add_type_2;}}
  Adwaita::Config::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace Adwaita{namespace PropertyNames{typedef int tmp_add_type_3;}}
  Adwaita::PropertyNames::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace Adwaita{namespace Settings{typedef int tmp_add_type_4;}}
  Adwaita::Settings::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_5;}
  QAlgorithmsPrivate::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QColorConstants{typedef int tmp_add_type_6;}
  QColorConstants::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QColorConstants{namespace Svg{typedef int tmp_add_type_7;}}
  QColorConstants::Svg::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace Qt{typedef int tmp_add_type_8;}
  Qt::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_9;}
  QtGlobalStatic::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_10;}
  QtMetaTypePrivate::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_11;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_11 tmp_add_func_11(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_12;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_12 tmp_add_func_12(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_13;}
  QtPrivate::tmp_add_type_13 tmp_add_func_13(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_14;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_14 tmp_add_func_14(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_15;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_15 tmp_add_func_15(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_16;}
  QtSharedPointer::tmp_add_type_16 tmp_add_func_16(){return 0;};

  // add classes
  QArrayData* tmp_add_class_0;
  QAtomicInt* tmp_add_class_1;
  QBrush* tmp_add_class_2;
  QBrushData* tmp_add_class_3;
  QByteArray* tmp_add_class_4;
  QByteArrayDataPtr* tmp_add_class_5;
  QByteRef* tmp_add_class_6;
  QChar* tmp_add_class_7;
  QCharRef* tmp_add_class_8;
  QColor* tmp_add_class_9;
  QConicalGradient* tmp_add_class_10;
  QDataStream* tmp_add_class_11;
  QFlag* tmp_add_class_12;
  QGenericArgument* tmp_add_class_13;
  QGenericReturnArgument* tmp_add_class_14;
  QGradient* tmp_add_class_15;
  QHashData* tmp_add_class_16;
  QHashDummyValue* tmp_add_class_17;
  QIODevice* tmp_add_class_18;
  QIcon* tmp_add_class_19;
  QImage* tmp_add_class_20;
  QIncompatibleFlag* tmp_add_class_21;
  QInternal* tmp_add_class_22;
  QLatin1Char* tmp_add_class_23;
  QLatin1String* tmp_add_class_24;
  QLine* tmp_add_class_25;
  QLineF* tmp_add_class_26;
  QLinearGradient* tmp_add_class_27;
  QListData* tmp_add_class_28;
  QMargins* tmp_add_class_29;
  QMarginsF* tmp_add_class_30;
  QMatrix* tmp_add_class_31;
  QMessageLogContext* tmp_add_class_32;
  QMessageLogger* tmp_add_class_33;
  QMetaObject* tmp_add_class_34;
  QMetaType* tmp_add_class_35;
  QObject* tmp_add_class_36;
  QObjectData* tmp_add_class_37;
  QObjectUserData* tmp_add_class_38;
  QPaintDevice* tmp_add_class_39;
  QPalette* tmp_add_class_40;
  QPixelFormat* tmp_add_class_41;
  QPixmap* tmp_add_class_42;
  QPoint* tmp_add_class_43;
  QPointF* tmp_add_class_44;
  QPolygon* tmp_add_class_45;
  QPolygonF* tmp_add_class_46;
  QRadialGradient* tmp_add_class_47;
  QRect* tmp_add_class_48;
  QRectF* tmp_add_class_49;
  QRegExp* tmp_add_class_50;
  QRegion* tmp_add_class_51;
  QRgba64* tmp_add_class_52;
  QScopedPointerPodDeleter* tmp_add_class_53;
  QSharedData* tmp_add_class_54;
  QSignalBlocker* tmp_add_class_55;
  QSize* tmp_add_class_56;
  QSizeF* tmp_add_class_57;
  QSizePolicy* tmp_add_class_58;
  QString* tmp_add_class_59;
  QStringDataPtr* tmp_add_class_60;
  QStringList* tmp_add_class_61;
  QStringMatcher* tmp_add_class_62;
  QStringRef* tmp_add_class_63;
  QStringView* tmp_add_class_64;
  QStyle* tmp_add_class_65;
  QSysInfo* tmp_add_class_66;
  QTransform* tmp_add_class_67;
  _G_fpos64_t* tmp_add_class_68;
  _G_fpos_t* tmp_add_class_69;
  _IO_cookie_io_functions_t* tmp_add_class_70;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/gPC7e8CK0y/dump1.h"  -I/usr/include/arm-linux-gnueabihf/qt5 -I/usr/include/arm-linux-gnueabihf/qt5/QtWidgets -I/usr/include/arm-linux-gnueabihf/qt5/QtGui -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

