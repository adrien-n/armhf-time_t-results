Temporary header file '/tmp/L0hcl0S1Hp/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d) 
#define HAVE_CSTDDEF

  // add includes
  #include "/usr/include/coin/HSLLoader.h"
  #include "/usr/include/coin/IpAlgTypes.hpp"
  #include "/usr/include/coin/IpBlas.hpp"
  #include "/usr/include/coin/IpCachedResults.hpp"
  #include "/usr/include/coin/IpCompoundVector.hpp"
  #include "/usr/include/coin/IpDebug.hpp"
  #include "/usr/include/coin/IpDenseVector.hpp"
  #include "/usr/include/coin/IpException.hpp"
  #include "/usr/include/coin/IpExpansionMatrix.hpp"
  #include "/usr/include/coin/IpIpoptApplication.hpp"
  #include "/usr/include/coin/IpIpoptCalculatedQuantities.hpp"
  #include "/usr/include/coin/IpIpoptData.hpp"
  #include "/usr/include/coin/IpIpoptNLP.hpp"
  #include "/usr/include/coin/IpIteratesVector.hpp"
  #include "/usr/include/coin/IpJournalist.hpp"
  #include "/usr/include/coin/IpLapack.hpp"
  #include "/usr/include/coin/IpMatrix.hpp"
  #include "/usr/include/coin/IpNLP.hpp"
  #include "/usr/include/coin/IpNLPScaling.hpp"
  #include "/usr/include/coin/IpObserver.hpp"
  #include "/usr/include/coin/IpOptionsList.hpp"
  #include "/usr/include/coin/IpOrigIpoptNLP.hpp"
  #include "/usr/include/coin/IpReferenced.hpp"
  #include "/usr/include/coin/IpRegOptions.hpp"
  #include "/usr/include/coin/IpReturnCodes.h"
  #include "/usr/include/coin/IpReturnCodes.hpp"
  #include "/usr/include/coin/IpSmartPtr.hpp"
  #include "/usr/include/coin/IpSolveStatistics.hpp"
  #include "/usr/include/coin/IpStdCInterface.h"
  #include "/usr/include/coin/IpSymMatrix.hpp"
  #include "/usr/include/coin/IpTNLP.hpp"
  #include "/usr/include/coin/IpTNLPAdapter.hpp"
  #include "/usr/include/coin/IpTNLPReducer.hpp"
  #include "/usr/include/coin/IpTaggedObject.hpp"
  #include "/usr/include/coin/IpTimedTask.hpp"
  #include "/usr/include/coin/IpTimingStatistics.hpp"
  #include "/usr/include/coin/IpTypes.hpp"
  #include "/usr/include/coin/IpUtils.hpp"
  #include "/usr/include/coin/IpVector.hpp"
  #include "/usr/include/coin/IpoptConfig.h"
  #include "/usr/include/coin/PardisoLoader.h"

  // add namespaces
  namespace Ipopt{typedef int tmp_add_type_1;}
  Ipopt::tmp_add_type_1 tmp_add_func_1(){return 0;};

  // add classes
  _G_fpos64_t* tmp_add_class_0;
  _G_fpos_t* tmp_add_class_1;
  _IO_cookie_io_functions_t* tmp_add_class_2;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/L0hcl0S1Hp/dump1.h"  -I/usr/lib/arm-linux-gnueabihf/openmpi/include

