Temporary header file '/tmp/2OJZpcnjGZ/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/dune/uggrid/domain/domain.h"
  #include "/usr/include/dune/uggrid/domain/std_domain.h"
  #include "/usr/include/dune/uggrid/gm/algebra.h"
  #include "/usr/include/dune/uggrid/gm/cw.h"
  #include "/usr/include/dune/uggrid/gm/dlmgr.h"
  #include "/usr/include/dune/uggrid/gm/elements.h"
  #include "/usr/include/dune/uggrid/gm/evm.h"
  #include "/usr/include/dune/uggrid/gm/gm.h"
  #include "/usr/include/dune/uggrid/gm/pargm.h"
  #include "/usr/include/dune/uggrid/gm/refine.h"
  #include "/usr/include/dune/uggrid/gm/rm-write2file.h"
  #include "/usr/include/dune/uggrid/gm/rm.h"
  #include "/usr/include/dune/uggrid/gm/shapes.h"
  #include "/usr/include/dune/uggrid/gm/ugm.h"
  #include "/usr/include/dune/uggrid/initug.h"
  #include "/usr/include/dune/uggrid/low/debug.h"
  #include "/usr/include/dune/uggrid/low/dimension.h"
  #include "/usr/include/dune/uggrid/low/fileopen.h"
  #include "/usr/include/dune/uggrid/low/heaps.h"
  #include "/usr/include/dune/uggrid/low/misc.h"
  #include "/usr/include/dune/uggrid/low/namespace.h"
  #include "/usr/include/dune/uggrid/low/ugenv.h"
  #include "/usr/include/dune/uggrid/low/ugstruct.h"
  #include "/usr/include/dune/uggrid/low/ugtimer.h"
  #include "/usr/include/dune/uggrid/low/ugtypes.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/basic/lowcomm.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/basic/notify.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/basic/oopp.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/basic/ooppcc.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/ctrl/stat.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/dddconstants.hh"
  #include "/usr/include/dune/uggrid/parallel/ddd/dddcontext.hh"
  #include "/usr/include/dune/uggrid/parallel/ddd/dddtypes.hh"
  #include "/usr/include/dune/uggrid/parallel/ddd/dddtypes_impl.hh"
  #include "/usr/include/dune/uggrid/parallel/ddd/if/if.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/include/ddd.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/join/join.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/xfer/sll.h"
  #include "/usr/include/dune/uggrid/parallel/ddd/xfer/xfer.h"
  #include "/usr/include/dune/uggrid/parallel/dddif/parallel.h"
  #include "/usr/include/dune/uggrid/parallel/ppif/ppif.h"
  #include "/usr/include/dune/uggrid/parallel/ppif/ppifcontext.hh"
  #include "/usr/include/dune/uggrid/parallel/ppif/ppiftypes.hh"
  #include "/usr/include/dune/uggrid/ugdevices.h"

  // add namespaces
  namespace DDD{typedef int tmp_add_type_1;}
  DDD::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace DDD{namespace Basic{typedef int tmp_add_type_2;}}
  DDD::Basic::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace DDD{namespace Ident{typedef int tmp_add_type_3;}}
  DDD::Ident::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace DDD{namespace If{typedef int tmp_add_type_4;}}
  DDD::If::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace DDD{namespace Join{typedef int tmp_add_type_5;}}
  DDD::Join::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace DDD{namespace Mgr{typedef int tmp_add_type_6;}}
  DDD::Mgr::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace DDD{namespace Prio{typedef int tmp_add_type_7;}}
  DDD::Prio::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace DDD{namespace Xfer{typedef int tmp_add_type_8;}}
  DDD::Xfer::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace PPIF{typedef int tmp_add_type_9;}
  PPIF::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace UG{typedef int tmp_add_type_10;}
  UG::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace UG{namespace D2{typedef int tmp_add_type_11;}}
  UG::D2::tmp_add_type_11 tmp_add_func_11(){return 0;};

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/2OJZpcnjGZ/dump1.h"  -I/usr/lib/arm-linux-gnueabihf/openmpi/include -I/usr/lib/arm-linux-gnueabihf/openmpi/include/openmpi

In file included from /usr/include/dune/uggrid/domain/std_domain.h:46,
                 from /tmp/2OJZpcnjGZ/dump1.h:17:
/usr/include/dune/uggrid/low/dimension.h:31:2: error: #error **** define at least dimension two OR three ****
   31 | #error ****    define at least dimension two OR three        ****
      |  ^~~~~
In file included from /tmp/2OJZpcnjGZ/dump1.h:45:
/usr/include/dune/uggrid/parallel/ddd/ctrl/stat.h:35:10: fatal error: dune/uggrid/parallel/ddd/include/dddaddon.h: No such file or directory
   35 | #include <dune/uggrid/parallel/ddd/include/dddaddon.h>
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
compilation terminated.

