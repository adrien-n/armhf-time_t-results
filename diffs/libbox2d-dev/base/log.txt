Temporary header file '/tmp/8uVVcaRNQh/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/box2d/b2_api.h"
  #include "/usr/include/box2d/b2_block_allocator.h"
  #include "/usr/include/box2d/b2_body.h"
  #include "/usr/include/box2d/b2_broad_phase.h"
  #include "/usr/include/box2d/b2_chain_shape.h"
  #include "/usr/include/box2d/b2_circle_shape.h"
  #include "/usr/include/box2d/b2_collision.h"
  #include "/usr/include/box2d/b2_common.h"
  #include "/usr/include/box2d/b2_contact.h"
  #include "/usr/include/box2d/b2_contact_manager.h"
  #include "/usr/include/box2d/b2_distance.h"
  #include "/usr/include/box2d/b2_distance_joint.h"
  #include "/usr/include/box2d/b2_draw.h"
  #include "/usr/include/box2d/b2_dynamic_tree.h"
  #include "/usr/include/box2d/b2_edge_shape.h"
  #include "/usr/include/box2d/b2_fixture.h"
  #include "/usr/include/box2d/b2_friction_joint.h"
  #include "/usr/include/box2d/b2_gear_joint.h"
  #include "/usr/include/box2d/b2_growable_stack.h"
  #include "/usr/include/box2d/b2_joint.h"
  #include "/usr/include/box2d/b2_math.h"
  #include "/usr/include/box2d/b2_motor_joint.h"
  #include "/usr/include/box2d/b2_mouse_joint.h"
  #include "/usr/include/box2d/b2_polygon_shape.h"
  #include "/usr/include/box2d/b2_prismatic_joint.h"
  #include "/usr/include/box2d/b2_pulley_joint.h"
  #include "/usr/include/box2d/b2_revolute_joint.h"
  #include "/usr/include/box2d/b2_rope.h"
  #include "/usr/include/box2d/b2_settings.h"
  #include "/usr/include/box2d/b2_shape.h"
  #include "/usr/include/box2d/b2_stack_allocator.h"
  #include "/usr/include/box2d/b2_time_of_impact.h"
  #include "/usr/include/box2d/b2_time_step.h"
  #include "/usr/include/box2d/b2_timer.h"
  #include "/usr/include/box2d/b2_types.h"
  #include "/usr/include/box2d/b2_weld_joint.h"
  #include "/usr/include/box2d/b2_wheel_joint.h"
  #include "/usr/include/box2d/b2_world.h"
  #include "/usr/include/box2d/b2_world_callbacks.h"
  #include "/usr/include/box2d/box2d.h"

  // add classes
  b2AABB* tmp_add_class_0;
  b2BlockAllocator* tmp_add_class_1;
  b2Body* tmp_add_class_2;
  b2BodyDef* tmp_add_class_3;
  b2BodyUserData* tmp_add_class_4;
  b2BroadPhase* tmp_add_class_5;
  b2ChainShape* tmp_add_class_6;
  b2CircleShape* tmp_add_class_7;
  b2ClipVertex* tmp_add_class_8;
  b2Color* tmp_add_class_9;
  b2Contact* tmp_add_class_10;
  b2ContactEdge* tmp_add_class_11;
  b2ContactFeature* tmp_add_class_12;
  b2ContactFilter* tmp_add_class_13;
  b2ContactID* tmp_add_class_14;
  b2ContactImpulse* tmp_add_class_15;
  b2ContactListener* tmp_add_class_16;
  b2ContactManager* tmp_add_class_17;
  b2ContactRegister* tmp_add_class_18;
  b2DestructionListener* tmp_add_class_19;
  b2DistanceInput* tmp_add_class_20;
  b2DistanceJoint* tmp_add_class_21;
  b2DistanceJointDef* tmp_add_class_22;
  b2DistanceOutput* tmp_add_class_23;
  b2DistanceProxy* tmp_add_class_24;
  b2Draw* tmp_add_class_25;
  b2DynamicTree* tmp_add_class_26;
  b2EdgeShape* tmp_add_class_27;
  b2Filter* tmp_add_class_28;
  b2Fixture* tmp_add_class_29;
  b2FixtureDef* tmp_add_class_30;
  b2FixtureProxy* tmp_add_class_31;
  b2FixtureUserData* tmp_add_class_32;
  b2FrictionJoint* tmp_add_class_33;
  b2FrictionJointDef* tmp_add_class_34;
  b2GearJoint* tmp_add_class_35;
  b2GearJointDef* tmp_add_class_36;
  b2Jacobian* tmp_add_class_37;
  b2Joint* tmp_add_class_38;
  b2JointDef* tmp_add_class_39;
  b2JointEdge* tmp_add_class_40;
  b2JointUserData* tmp_add_class_41;
  b2Manifold* tmp_add_class_42;
  b2ManifoldPoint* tmp_add_class_43;
  b2MassData* tmp_add_class_44;
  b2Mat22* tmp_add_class_45;
  b2Mat33* tmp_add_class_46;
  b2MotorJoint* tmp_add_class_47;
  b2MotorJointDef* tmp_add_class_48;
  b2MouseJoint* tmp_add_class_49;
  b2MouseJointDef* tmp_add_class_50;
  b2Pair* tmp_add_class_51;
  b2PolygonShape* tmp_add_class_52;
  b2Position* tmp_add_class_53;
  b2PrismaticJoint* tmp_add_class_54;
  b2PrismaticJointDef* tmp_add_class_55;
  b2Profile* tmp_add_class_56;
  b2PulleyJoint* tmp_add_class_57;
  b2PulleyJointDef* tmp_add_class_58;
  b2QueryCallback* tmp_add_class_59;
  b2RayCastCallback* tmp_add_class_60;
  b2RayCastInput* tmp_add_class_61;
  b2RayCastOutput* tmp_add_class_62;
  b2RevoluteJoint* tmp_add_class_63;
  b2RevoluteJointDef* tmp_add_class_64;
  b2Rope* tmp_add_class_65;
  b2RopeDef* tmp_add_class_66;
  b2RopeTuning* tmp_add_class_67;
  b2Rot* tmp_add_class_68;
  b2Shape* tmp_add_class_69;
  b2ShapeCastInput* tmp_add_class_70;
  b2ShapeCastOutput* tmp_add_class_71;
  b2SimplexCache* tmp_add_class_72;
  b2SolverData* tmp_add_class_73;
  b2StackAllocator* tmp_add_class_74;
  b2StackEntry* tmp_add_class_75;
  b2Sweep* tmp_add_class_76;
  b2TOIInput* tmp_add_class_77;
  b2TOIOutput* tmp_add_class_78;
  b2TimeStep* tmp_add_class_79;
  b2Timer* tmp_add_class_80;
  b2Transform* tmp_add_class_81;
  b2TreeNode* tmp_add_class_82;
  b2Vec2* tmp_add_class_83;
  b2Vec3* tmp_add_class_84;
  b2Velocity* tmp_add_class_85;
  b2Version* tmp_add_class_86;
  b2WeldJoint* tmp_add_class_87;
  b2WeldJointDef* tmp_add_class_88;
  b2WheelJoint* tmp_add_class_89;
  b2WheelJointDef* tmp_add_class_90;
  b2World* tmp_add_class_91;
  b2WorldManifold* tmp_add_class_92;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/8uVVcaRNQh/dump1.h"

