Temporary header file '/tmp/oTuzME3ZZ2/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/ace/TMCast/Export.hpp"
  #include "/usr/include/ace/TMCast/FaultDetector.hpp"
  #include "/usr/include/ace/TMCast/Group.hpp"
  #include "/usr/include/ace/TMCast/GroupFwd.hpp"
  #include "/usr/include/ace/TMCast/LinkListener.hpp"
  #include "/usr/include/ace/TMCast/MTQueue.hpp"
  #include "/usr/include/ace/TMCast/Messaging.hpp"
  #include "/usr/include/ace/TMCast/Protocol.hpp"
  #include "/usr/include/ace/TMCast/TransactionController.hpp"

  // add namespaces
  namespace ACE{typedef int tmp_add_type_1;}
  ACE::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace ACE_OS{typedef int tmp_add_type_2;}
  ACE_OS::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace ACE_TMCast{typedef int tmp_add_type_3;}
  ACE_TMCast::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace ACE_TMCast{namespace Protocol{typedef int tmp_add_type_4;}}
  ACE_TMCast::Protocol::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace ACE_Utils{typedef int tmp_add_type_5;}
  ACE_Utils::tmp_add_type_5 tmp_add_func_5(){return 0;};

  // add classes
  ACE_Accept_QoS_Params* tmp_add_class_0;
  ACE_Adaptive_Lock* tmp_add_class_1;
  ACE_Addr* tmp_add_class_2;
  ACE_Allocator* tmp_add_class_3;
  ACE_Ascii_To_Wide* tmp_add_class_4;
  ACE_Auto_Event* tmp_add_class_5;
  ACE_Barrier* tmp_add_class_6;
  ACE_Base_Thread_Adapter* tmp_add_class_7;
  ACE_Cleanup* tmp_add_class_8;
  ACE_Cleanup_Info_Node* tmp_add_class_9;
  ACE_Condition_Attributes* tmp_add_class_10;
  ACE_Delegating_Time_Policy* tmp_add_class_11;
  ACE_Dynamic_Time_Policy_Base* tmp_add_class_12;
  ACE_Errno_Guard* tmp_add_class_13;
  ACE_Event_Base* tmp_add_class_14;
  ACE_FPointer_Time_Policy* tmp_add_class_15;
  ACE_Flow_Spec* tmp_add_class_16;
  ACE_HR_Time_Policy* tmp_add_class_17;
  ACE_High_Res_Timer* tmp_add_class_18;
  ACE_INET_Addr* tmp_add_class_19;
  ACE_IPC_SAP* tmp_add_class_20;
  ACE_Lock* tmp_add_class_21;
  ACE_Log_Category* tmp_add_class_22;
  ACE_Log_Category_TSS* tmp_add_class_23;
  ACE_Log_Msg* tmp_add_class_24;
  ACE_MT_SYNCH* tmp_add_class_25;
  ACE_Manual_Event* tmp_add_class_26;
  ACE_Mutex* tmp_add_class_27;
  ACE_NULL_SYNCH* tmp_add_class_28;
  ACE_Null_Barrier* tmp_add_class_29;
  ACE_Null_Mutex* tmp_add_class_30;
  ACE_Null_Semaphore* tmp_add_class_31;
  ACE_OS_Exit_Info* tmp_add_class_32;
  ACE_OS_Log_Msg_Attributes* tmp_add_class_33;
  ACE_OS_Object_Manager* tmp_add_class_34;
  ACE_OS_Recursive_Thread_Mutex_Guard* tmp_add_class_35;
  ACE_OS_Thread_Descriptor* tmp_add_class_36;
  ACE_OS_Thread_Mutex_Guard* tmp_add_class_37;
  ACE_OVERLAPPED* tmp_add_class_38;
  ACE_Object_Manager_Base* tmp_add_class_39;
  ACE_Protocol_Info* tmp_add_class_40;
  ACE_QoS* tmp_add_class_41;
  ACE_QoS_Params* tmp_add_class_42;
  ACE_RW_Mutex* tmp_add_class_43;
  ACE_RW_Thread_Mutex* tmp_add_class_44;
  ACE_Recursive_Thread_Mutex* tmp_add_class_45;
  ACE_SOCK* tmp_add_class_46;
  ACE_SOCK_Dgram* tmp_add_class_47;
  ACE_SOCK_Dgram_Mcast* tmp_add_class_48;
  ACE_Semaphore* tmp_add_class_49;
  ACE_Str_Buf* tmp_add_class_50;
  ACE_Sub_Barrier* tmp_add_class_51;
  ACE_System_Time_Policy* tmp_add_class_52;
  ACE_TSS_Adapter* tmp_add_class_53;
  ACE_Thread_Barrier* tmp_add_class_54;
  ACE_Thread_ID* tmp_add_class_55;
  ACE_Thread_Mutex* tmp_add_class_56;
  ACE_Thread_Semaphore* tmp_add_class_57;
  ACE_Time_Value* tmp_add_class_58;
  ACE_Wide_To_Ascii* tmp_add_class_59;
  ACE_event_t* tmp_add_class_60;
  ACE_eventdata_t* tmp_add_class_61;
  _G_fpos64_t* tmp_add_class_62;
  _G_fpos_t* tmp_add_class_63;
  _IO_cookie_io_functions_t* tmp_add_class_64;
  cmsghdr* tmp_add_class_65;
  entry* tmp_add_class_66;
  f_owner_ex* tmp_add_class_67;
  file_handle* tmp_add_class_68;
  flock64* tmp_add_class_69;
  group_filter* tmp_add_class_70;
  group_req* tmp_add_class_71;
  group_source_req* tmp_add_class_72;
  hsearch_data* tmp_add_class_73;
  ifaddr* tmp_add_class_74;
  ifconf* tmp_add_class_75;
  ifmap* tmp_add_class_76;
  ifreq* tmp_add_class_77;
  in6_addr* tmp_add_class_78;
  in6_pktinfo* tmp_add_class_79;
  in_addr* tmp_add_class_80;
  in_pktinfo* tmp_add_class_81;
  iovec* tmp_add_class_82;
  ip6_mtuinfo* tmp_add_class_83;
  ip_mreq* tmp_add_class_84;
  ip_mreq_source* tmp_add_class_85;
  ip_mreqn* tmp_add_class_86;
  ip_msfilter* tmp_add_class_87;
  ip_opts* tmp_add_class_88;
  ipc_perm* tmp_add_class_89;
  ipv6_mreq* tmp_add_class_90;
  itimerval* tmp_add_class_91;
  linger* tmp_add_class_92;
  mmsghdr* tmp_add_class_93;
  msghdr* tmp_add_class_94;
  nl_mmap_hdr* tmp_add_class_95;
  nl_mmap_req* tmp_add_class_96;
  nl_pktinfo* tmp_add_class_97;
  nla_bitfield32* tmp_add_class_98;
  nlattr* tmp_add_class_99;
  nlmsgerr* tmp_add_class_100;
  nlmsghdr* tmp_add_class_101;
  osockaddr* tmp_add_class_102;
  passwd* tmp_add_class_103;
  qelem* tmp_add_class_104;
  rlimit64* tmp_add_class_105;
  sembuf* tmp_add_class_106;
  semid_ds* tmp_add_class_107;
  seminfo* tmp_add_class_108;
  semun* tmp_add_class_109;
  sockaddr* tmp_add_class_110;
  sockaddr_in* tmp_add_class_111;
  sockaddr_in6* tmp_add_class_112;
  sockaddr_nl* tmp_add_class_113;
  sockaddr_storage* tmp_add_class_114;
  statx_timestamp* tmp_add_class_115;
  strbuf* tmp_add_class_116;
  strrecvfd* tmp_add_class_117;
  termio* tmp_add_class_118;
  ucontext_t* tmp_add_class_119;
  ucred* tmp_add_class_120;
  utsname* tmp_add_class_121;
  winsize* tmp_add_class_122;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/oTuzME3ZZ2/dump1.h"

