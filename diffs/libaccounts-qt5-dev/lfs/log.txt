Temporary header file '/tmp/2tJUB6nHYD/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/accounts-qt5/Accounts/account-service.h"
  #include "/usr/include/accounts-qt5/Accounts/account.h"
  #include "/usr/include/accounts-qt5/Accounts/accountscommon.h"
  #include "/usr/include/accounts-qt5/Accounts/application.h"
  #include "/usr/include/accounts-qt5/Accounts/auth-data.h"
  #include "/usr/include/accounts-qt5/Accounts/error.h"
  #include "/usr/include/accounts-qt5/Accounts/manager.h"
  #include "/usr/include/accounts-qt5/Accounts/manager_p.h"
  #include "/usr/include/accounts-qt5/Accounts/provider.h"
  #include "/usr/include/accounts-qt5/Accounts/service-type.h"
  #include "/usr/include/accounts-qt5/Accounts/service.h"
  #include "/usr/include/accounts-qt5/Accounts/utils.h"

  // add namespaces
  namespace Accounts{typedef int tmp_add_type_1;}
  Accounts::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace QAlgorithmsPrivate{typedef int tmp_add_type_2;}
  QAlgorithmsPrivate::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace Qt{typedef int tmp_add_type_3;}
  Qt::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace QtGlobalStatic{typedef int tmp_add_type_4;}
  QtGlobalStatic::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace QtMetaTypePrivate{typedef int tmp_add_type_5;}
  QtMetaTypePrivate::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{typedef int tmp_add_type_6;}}
  QtMetaTypePrivate::QtPrivate::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace QtMetaTypePrivate{namespace QtPrivate{namespace ContainerCapabilitiesMetaProgrammingHelper{typedef int tmp_add_type_7;}}}
  QtMetaTypePrivate::QtPrivate::ContainerCapabilitiesMetaProgrammingHelper::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace QtPrivate{typedef int tmp_add_type_8;}
  QtPrivate::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace QtPrivate{namespace DeprecatedRefClassBehavior{typedef int tmp_add_type_9;}}
  QtPrivate::DeprecatedRefClassBehavior::tmp_add_type_9 tmp_add_func_9(){return 0;};
  namespace QtPrivate{namespace SwapExceptionTester{typedef int tmp_add_type_10;}}
  QtPrivate::SwapExceptionTester::tmp_add_type_10 tmp_add_func_10(){return 0;};
  namespace QtSharedPointer{typedef int tmp_add_type_11;}
  QtSharedPointer::tmp_add_type_11 tmp_add_func_11(){return 0;};

  // add classes
  QArrayData* tmp_add_class_0;
  QAssociativeIterable* tmp_add_class_1;
  QAtomicInt* tmp_add_class_2;
  QByteArray* tmp_add_class_3;
  QByteArrayDataPtr* tmp_add_class_4;
  QByteRef* tmp_add_class_5;
  QChar* tmp_add_class_6;
  QCharRef* tmp_add_class_7;
  QDomAttr* tmp_add_class_8;
  QDomCDATASection* tmp_add_class_9;
  QDomCharacterData* tmp_add_class_10;
  QDomComment* tmp_add_class_11;
  QDomDocument* tmp_add_class_12;
  QDomDocumentFragment* tmp_add_class_13;
  QDomDocumentType* tmp_add_class_14;
  QDomElement* tmp_add_class_15;
  QDomEntity* tmp_add_class_16;
  QDomEntityReference* tmp_add_class_17;
  QDomImplementation* tmp_add_class_18;
  QDomNamedNodeMap* tmp_add_class_19;
  QDomNode* tmp_add_class_20;
  QDomNodeList* tmp_add_class_21;
  QDomNotation* tmp_add_class_22;
  QDomProcessingInstruction* tmp_add_class_23;
  QDomText* tmp_add_class_24;
  QFlag* tmp_add_class_25;
  QGenericArgument* tmp_add_class_26;
  QGenericReturnArgument* tmp_add_class_27;
  QHashData* tmp_add_class_28;
  QHashDummyValue* tmp_add_class_29;
  QIncompatibleFlag* tmp_add_class_30;
  QInternal* tmp_add_class_31;
  QLatin1Char* tmp_add_class_32;
  QLatin1String* tmp_add_class_33;
  QListData* tmp_add_class_34;
  QMapDataBase* tmp_add_class_35;
  QMapNodeBase* tmp_add_class_36;
  QMessageLogContext* tmp_add_class_37;
  QMessageLogger* tmp_add_class_38;
  QMetaObject* tmp_add_class_39;
  QMetaType* tmp_add_class_40;
  QObject* tmp_add_class_41;
  QObjectData* tmp_add_class_42;
  QObjectUserData* tmp_add_class_43;
  QRegExp* tmp_add_class_44;
  QScopedPointerPodDeleter* tmp_add_class_45;
  QSequentialIterable* tmp_add_class_46;
  QSettings* tmp_add_class_47;
  QSharedData* tmp_add_class_48;
  QSignalBlocker* tmp_add_class_49;
  QString* tmp_add_class_50;
  QStringDataPtr* tmp_add_class_51;
  QStringList* tmp_add_class_52;
  QStringMatcher* tmp_add_class_53;
  QStringRef* tmp_add_class_54;
  QStringView* tmp_add_class_55;
  QSysInfo* tmp_add_class_56;
  QVariant* tmp_add_class_57;
  QVariantComparisonHelper* tmp_add_class_58;
  _AgManager* tmp_add_class_59;
  _AgManagerClass* tmp_add_class_60;
  _GArray* tmp_add_class_61;
  _GByteArray* tmp_add_class_62;
  _GCClosure* tmp_add_class_63;
  _GClosure* tmp_add_class_64;
  _GClosureNotifyData* tmp_add_class_65;
  _GCompletion* tmp_add_class_66;
  _GCond* tmp_add_class_67;
  _GDate* tmp_add_class_68;
  _GDebugKey* tmp_add_class_69;
  _GDoubleIEEE754* tmp_add_class_70;
  _GEnumClass* tmp_add_class_71;
  _GEnumValue* tmp_add_class_72;
  _GError* tmp_add_class_73;
  _GFlagsClass* tmp_add_class_74;
  _GFlagsValue* tmp_add_class_75;
  _GFloatIEEE754* tmp_add_class_76;
  _GHashTableIter* tmp_add_class_77;
  _GHook* tmp_add_class_78;
  _GHookList* tmp_add_class_79;
  _GIOChannel* tmp_add_class_80;
  _GIOFuncs* tmp_add_class_81;
  _GInterfaceInfo* tmp_add_class_82;
  _GList* tmp_add_class_83;
  _GLogField* tmp_add_class_84;
  _GMarkupParser* tmp_add_class_85;
  _GMemVTable* tmp_add_class_86;
  _GMutex* tmp_add_class_87;
  _GNode* tmp_add_class_88;
  _GObject* tmp_add_class_89;
  _GObjectClass* tmp_add_class_90;
  _GObjectConstructParam* tmp_add_class_91;
  _GOnce* tmp_add_class_92;
  _GOptionEntry* tmp_add_class_93;
  _GParamSpec* tmp_add_class_94;
  _GParamSpecBoolean* tmp_add_class_95;
  _GParamSpecBoxed* tmp_add_class_96;
  _GParamSpecChar* tmp_add_class_97;
  _GParamSpecClass* tmp_add_class_98;
  _GParamSpecDouble* tmp_add_class_99;
  _GParamSpecEnum* tmp_add_class_100;
  _GParamSpecFlags* tmp_add_class_101;
  _GParamSpecFloat* tmp_add_class_102;
  _GParamSpecGType* tmp_add_class_103;
  _GParamSpecInt* tmp_add_class_104;
  _GParamSpecInt64* tmp_add_class_105;
  _GParamSpecLong* tmp_add_class_106;
  _GParamSpecObject* tmp_add_class_107;
  _GParamSpecOverride* tmp_add_class_108;
  _GParamSpecParam* tmp_add_class_109;
  _GParamSpecPointer* tmp_add_class_110;
  _GParamSpecString* tmp_add_class_111;
  _GParamSpecTypeInfo* tmp_add_class_112;
  _GParamSpecUChar* tmp_add_class_113;
  _GParamSpecUInt* tmp_add_class_114;
  _GParamSpecUInt64* tmp_add_class_115;
  _GParamSpecULong* tmp_add_class_116;
  _GParamSpecUnichar* tmp_add_class_117;
  _GParamSpecValueArray* tmp_add_class_118;
  _GParamSpecVariant* tmp_add_class_119;
  _GParameter* tmp_add_class_120;
  _GPathBuf* tmp_add_class_121;
  _GPollFD* tmp_add_class_122;
  _GPrivate* tmp_add_class_123;
  _GPtrArray* tmp_add_class_124;
  _GQueue* tmp_add_class_125;
  _GRWLock* tmp_add_class_126;
  _GRecMutex* tmp_add_class_127;
  _GSList* tmp_add_class_128;
  _GScanner* tmp_add_class_129;
  _GScannerConfig* tmp_add_class_130;
  _GSignalInvocationHint* tmp_add_class_131;
  _GSignalQuery* tmp_add_class_132;
  _GSource* tmp_add_class_133;
  _GSourceCallbackFuncs* tmp_add_class_134;
  _GSourceFuncs* tmp_add_class_135;
  _GStaticPrivate* tmp_add_class_136;
  _GStaticRWLock* tmp_add_class_137;
  _GStaticRecMutex* tmp_add_class_138;
  _GString* tmp_add_class_139;
  _GThread* tmp_add_class_140;
  _GThreadFunctions* tmp_add_class_141;
  _GThreadPool* tmp_add_class_142;
  _GTimeVal* tmp_add_class_143;
  _GTokenValue* tmp_add_class_144;
  _GTrashStack* tmp_add_class_145;
  _GTuples* tmp_add_class_146;
  _GTypeClass* tmp_add_class_147;
  _GTypeFundamentalInfo* tmp_add_class_148;
  _GTypeInfo* tmp_add_class_149;
  _GTypeInstance* tmp_add_class_150;
  _GTypeInterface* tmp_add_class_151;
  _GTypeModule* tmp_add_class_152;
  _GTypeModuleClass* tmp_add_class_153;
  _GTypePluginClass* tmp_add_class_154;
  _GTypeQuery* tmp_add_class_155;
  _GTypeValueTable* tmp_add_class_156;
  _GUriParamsIter* tmp_add_class_157;
  _GValue* tmp_add_class_158;
  _GValueArray* tmp_add_class_159;
  _GVariantBuilder* tmp_add_class_160;
  _GVariantDict* tmp_add_class_161;
  _GVariantIter* tmp_add_class_162;
  _G_fpos64_t* tmp_add_class_163;
  _G_fpos_t* tmp_add_class_164;
  _IO_cookie_io_functions_t* tmp_add_class_165;
  ucontext_t* tmp_add_class_166;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/2tJUB6nHYD/dump1.h"  -I/usr/include/glib-2.0 -I/usr/include/accounts-qt5 -I/usr/include/arm-linux-gnueabihf/qt5 -I/lib/arm-linux-gnueabihf/glib-2.0/include -I/usr/include/arm-linux-gnueabihf/qt5/QtXml -I/usr/include/arm-linux-gnueabihf/qt5/QtCore

