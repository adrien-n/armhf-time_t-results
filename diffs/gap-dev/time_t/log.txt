Temporary header file '/tmp/rmxq1gMJ8K/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _TIME_BITS 64
#define _FILE_OFFSET_BITS 64
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/gap/src/ariths.h"
  #include "/usr/include/gap/src/bits_intern.h"
  #include "/usr/include/gap/src/blister.h"
  #include "/usr/include/gap/src/bool.h"
  #include "/usr/include/gap/src/calls.h"
  #include "/usr/include/gap/src/code.h"
  #include "/usr/include/gap/src/collectors.h"
  #include "/usr/include/gap/src/common.h"
  #include "/usr/include/gap/src/compiled.h"
  #include "/usr/include/gap/src/compiler.h"
  #include "/usr/include/gap/src/compstat.h"
  #include "/usr/include/gap/src/costab.h"
  #include "/usr/include/gap/src/cyclotom.h"
  #include "/usr/include/gap/src/debug.h"
  #include "/usr/include/gap/src/dt.h"
  #include "/usr/include/gap/src/dteval.h"
  #include "/usr/include/gap/src/error.h"
  #include "/usr/include/gap/src/exprs.h"
  #include "/usr/include/gap/src/ffdata.h"
  #include "/usr/include/gap/src/fibhash.h"
  #include "/usr/include/gap/src/finfield.h"
  #include "/usr/include/gap/src/finfield_conway.h"
  #include "/usr/include/gap/src/funcs.h"
  #include "/usr/include/gap/src/gap.h"
  #include "/usr/include/gap/src/gap_all.h"
  #include "/usr/include/gap/src/gapstate.h"
  #include "/usr/include/gap/src/gaptime.h"
  #include "/usr/include/gap/src/gaputils.h"
  #include "/usr/include/gap/src/gasman.h"
  #include "/usr/include/gap/src/gasman_intern.h"
  #include "/usr/include/gap/src/gvars.h"
  #include "/usr/include/gap/src/hookintrprtr.h"
  #include "/usr/include/gap/src/info.h"
  #include "/usr/include/gap/src/integer.h"
  #include "/usr/include/gap/src/intfuncs.h"
  #include "/usr/include/gap/src/intobj.h"
  #include "/usr/include/gap/src/intrprtr.h"
  #include "/usr/include/gap/src/io.h"
  #include "/usr/include/gap/src/iostream.h"
  #include "/usr/include/gap/src/julia_gc.h"
  #include "/usr/include/gap/src/libgap-api.h"
  #include "/usr/include/gap/src/libgap_intern.h"
  #include "/usr/include/gap/src/listfunc.h"
  #include "/usr/include/gap/src/listoper.h"
  #include "/usr/include/gap/src/lists.h"
  #include "/usr/include/gap/src/macfloat.h"
  #include "/usr/include/gap/src/modules.h"
  #include "/usr/include/gap/src/modules_builtin.h"
  #include "/usr/include/gap/src/objccoll.h"
  #include "/usr/include/gap/src/objcftl.h"
  #include "/usr/include/gap/src/objects.h"
  #include "/usr/include/gap/src/objfgelm.h"
  #include "/usr/include/gap/src/objpcgel.h"
  #include "/usr/include/gap/src/objset.h"
  #include "/usr/include/gap/src/opers.h"
  #include "/usr/include/gap/src/permutat.h"
  #include "/usr/include/gap/src/plist.h"
  #include "/usr/include/gap/src/pperm.h"
  #include "/usr/include/gap/src/precord.h"
  #include "/usr/include/gap/src/profile.h"
  #include "/usr/include/gap/src/range.h"
  #include "/usr/include/gap/src/rational.h"
  #include "/usr/include/gap/src/read.h"
  #include "/usr/include/gap/src/records.h"
  #include "/usr/include/gap/src/saveload.h"
  #include "/usr/include/gap/src/scanner.h"
  #include "/usr/include/gap/src/sctable.h"
  #include "/usr/include/gap/src/set.h"
  #include "/usr/include/gap/src/sha256.h"
  #include "/usr/include/gap/src/stats.h"
  #include "/usr/include/gap/src/streams.h"
  #include "/usr/include/gap/src/stringobj.h"
  #include "/usr/include/gap/src/symbols.h"
  #include "/usr/include/gap/src/syntaxtree.h"
  #include "/usr/include/gap/src/sysenv.h"
  #include "/usr/include/gap/src/sysfiles.h"
  #include "/usr/include/gap/src/sysmem.h"
  #include "/usr/include/gap/src/sysopt.h"
  #include "/usr/include/gap/src/sysroots.h"
  #include "/usr/include/gap/src/sysstr.h"
  #include "/usr/include/gap/src/system.h"
  #include "/usr/include/gap/src/tietze.h"
  #include "/usr/include/gap/src/tracing.h"
  #include "/usr/include/gap/src/trans.h"
  #include "/usr/include/gap/src/trycatch.h"
  #include "/usr/include/gap/src/vars.h"
  #include "/usr/include/gap/src/vec8bit.h"
  #include "/usr/include/gap/src/vecffe.h"
  #include "/usr/include/gap/src/vecgf2.h"
  #include "/usr/include/gap/src/vector.h"
  #include "/usr/include/gap/src/version.h"
  #include "/usr/include/gap/src/weakptr.h"

  // add classes
  FieldInfo8Bit* tmp_add_class_0;
  GAPState* tmp_add_class_1;
  InterpreterHooks* tmp_add_class_2;
  IntrState* tmp_add_class_3;
  PrintHooks* tmp_add_class_4;
  SymbolTable* tmp_add_class_5;
  TypInputFile* tmp_add_class_6;
  TypOutputFile* tmp_add_class_7;
  _G_fpos64_t* tmp_add_class_8;
  _G_fpos_t* tmp_add_class_9;
  _IO_cookie_io_functions_t* tmp_add_class_10;
  init_info* tmp_add_class_11;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/rmxq1gMJ8K/dump1.h"  -I/usr/include/gap

