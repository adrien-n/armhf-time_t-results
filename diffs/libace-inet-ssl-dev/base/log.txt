Temporary header file '/tmp/S3tAWPQvXO/dump1.h' with the following content will be compiled to create GCC translation unit dump:

  // add defines
  #define __STDC_VERSION__ 201710L
#define _GXX_NULLPTR_T
#define CLUTTER_ENABLE_COMPOSITOR_API 1
#define _REGEX_NELTS(n)
#define _Atomic
#define PACKED
#ifdef __cplusplus
// C17 compat layer
#define _Alignof(type) alignof(type)
#endif
#define getaddrinfo_a(a,b,c,d)

  // add includes
  #include "/usr/include/ace/INet/HTTPS_Context.h"
  #include "/usr/include/ace/INet/HTTPS_Session.h"
  #include "/usr/include/ace/INet/HTTPS_SessionFactory.h"
  #include "/usr/include/ace/INet/HTTPS_URL.h"
  #include "/usr/include/ace/INet/INet_SSL_Export.h"
  #include "/usr/include/ace/INet/SSLSock_IOStream.h"
  #include "/usr/include/ace/INet/SSL_CallbackManager.h"
  #include "/usr/include/ace/INet/SSL_CertificateCallback.h"
  #include "/usr/include/ace/INet/SSL_PasswordCallback.h"
  #include "/usr/include/ace/INet/SSL_Proxy_Connector.h"
  #include "/usr/include/ace/INet/SSL_X509Cert.h"

  // add namespaces
  namespace ACE{typedef int tmp_add_type_1;}
  ACE::tmp_add_type_1 tmp_add_func_1(){return 0;};
  namespace ACE{namespace HTTP{typedef int tmp_add_type_2;}}
  ACE::HTTP::tmp_add_type_2 tmp_add_func_2(){return 0;};
  namespace ACE{namespace HTTPS{typedef int tmp_add_type_3;}}
  ACE::HTTPS::tmp_add_type_3 tmp_add_func_3(){return 0;};
  namespace ACE{namespace INet{typedef int tmp_add_type_4;}}
  ACE::INet::tmp_add_type_4 tmp_add_func_4(){return 0;};
  namespace ACE{namespace IOS{typedef int tmp_add_type_5;}}
  ACE::IOS::tmp_add_type_5 tmp_add_func_5(){return 0;};
  namespace ACE_Acquire_Method{typedef int tmp_add_type_6;}
  ACE_Acquire_Method::tmp_add_type_6 tmp_add_func_6(){return 0;};
  namespace ACE_OS{typedef int tmp_add_type_7;}
  ACE_OS::tmp_add_type_7 tmp_add_func_7(){return 0;};
  namespace ACE_Task_Flags{typedef int tmp_add_type_8;}
  ACE_Task_Flags::tmp_add_type_8 tmp_add_func_8(){return 0;};
  namespace ACE_Utils{typedef int tmp_add_type_9;}
  ACE_Utils::tmp_add_type_9 tmp_add_func_9(){return 0;};

  // add classes
  ACE_Accept_QoS_Params* tmp_add_class_0;
  ACE_Adaptive_Lock* tmp_add_class_1;
  ACE_Addr* tmp_add_class_2;
  ACE_Allocator* tmp_add_class_3;
  ACE_Ascii_To_Wide* tmp_add_class_4;
  ACE_At_Thread_Exit* tmp_add_class_5;
  ACE_At_Thread_Exit_Func* tmp_add_class_6;
  ACE_Auto_String_Free* tmp_add_class_7;
  ACE_Base_Thread_Adapter* tmp_add_class_8;
  ACE_Cleanup* tmp_add_class_9;
  ACE_Cleanup_Info_Node* tmp_add_class_10;
  ACE_Command_Base* tmp_add_class_11;
  ACE_Condition_Attributes* tmp_add_class_12;
  ACE_Connection_Recycling_Strategy* tmp_add_class_13;
  ACE_Copy_Disabled* tmp_add_class_14;
  ACE_DLL* tmp_add_class_15;
  ACE_DLList_Node* tmp_add_class_16;
  ACE_Data_Block* tmp_add_class_17;
  ACE_Deadline_Message_Strategy* tmp_add_class_18;
  ACE_Delegating_Time_Policy* tmp_add_class_19;
  ACE_Dynamic* tmp_add_class_20;
  ACE_Dynamic_Message_Strategy* tmp_add_class_21;
  ACE_Dynamic_Time_Policy_Base* tmp_add_class_22;
  ACE_Errno_Guard* tmp_add_class_23;
  ACE_Event_Handler* tmp_add_class_24;
  ACE_Event_Handler_var* tmp_add_class_25;
  ACE_FPointer_Time_Policy* tmp_add_class_26;
  ACE_Flow_Spec* tmp_add_class_27;
  ACE_Framework_Component* tmp_add_class_28;
  ACE_Framework_Repository* tmp_add_class_29;
  ACE_HR_Time_Policy* tmp_add_class_30;
  ACE_Hashable* tmp_add_class_31;
  ACE_High_Res_Timer* tmp_add_class_32;
  ACE_INET_Addr* tmp_add_class_33;
  ACE_IO_Cntl_Msg* tmp_add_class_34;
  ACE_IPC_SAP* tmp_add_class_35;
  ACE_Laxity_Message_Strategy* tmp_add_class_36;
  ACE_Lock* tmp_add_class_37;
  ACE_Log_Category* tmp_add_class_38;
  ACE_Log_Category_TSS* tmp_add_class_39;
  ACE_Log_Msg* tmp_add_class_40;
  ACE_MT_SYNCH* tmp_add_class_41;
  ACE_Message_Block* tmp_add_class_42;
  ACE_Message_Queue_Base* tmp_add_class_43;
  ACE_Module_Base* tmp_add_class_44;
  ACE_Module_Type* tmp_add_class_45;
  ACE_NS_WString* tmp_add_class_46;
  ACE_NULL_SYNCH* tmp_add_class_47;
  ACE_Noop_Command* tmp_add_class_48;
  ACE_Notification_Buffer* tmp_add_class_49;
  ACE_Notification_Strategy* tmp_add_class_50;
  ACE_Null_Mutex* tmp_add_class_51;
  ACE_OS_Exit_Info* tmp_add_class_52;
  ACE_OS_Log_Msg_Attributes* tmp_add_class_53;
  ACE_OS_Object_Manager* tmp_add_class_54;
  ACE_OS_Recursive_Thread_Mutex_Guard* tmp_add_class_55;
  ACE_OS_Thread_Descriptor* tmp_add_class_56;
  ACE_OS_Thread_Mutex_Guard* tmp_add_class_57;
  ACE_OVERLAPPED* tmp_add_class_58;
  ACE_Object_Manager* tmp_add_class_59;
  ACE_Object_Manager_Base* tmp_add_class_60;
  ACE_Protocol_Info* tmp_add_class_61;
  ACE_QoS* tmp_add_class_62;
  ACE_QoS_Params* tmp_add_class_63;
  ACE_RW_Mutex* tmp_add_class_64;
  ACE_RW_Thread_Mutex* tmp_add_class_65;
  ACE_Reactor* tmp_add_class_66;
  ACE_Reactor_Impl* tmp_add_class_67;
  ACE_Reactor_Notification_Strategy* tmp_add_class_68;
  ACE_Reactor_Notify* tmp_add_class_69;
  ACE_Reactor_Timer_Interface* tmp_add_class_70;
  ACE_Recursive_Thread_Mutex* tmp_add_class_71;
  ACE_Recyclable* tmp_add_class_72;
  ACE_SOCK* tmp_add_class_73;
  ACE_SOCK_Connector* tmp_add_class_74;
  ACE_SOCK_IO* tmp_add_class_75;
  ACE_SOCK_Stream* tmp_add_class_76;
  ACE_SSL_Context* tmp_add_class_77;
  ACE_SSL_Data_File* tmp_add_class_78;
  ACE_SSL_SOCK* tmp_add_class_79;
  ACE_SSL_SOCK_Connector* tmp_add_class_80;
  ACE_SSL_SOCK_Stream* tmp_add_class_81;
  ACE_SString* tmp_add_class_82;
  ACE_Service_Gestalt* tmp_add_class_83;
  ACE_Service_Object* tmp_add_class_84;
  ACE_Service_Object_Ptr* tmp_add_class_85;
  ACE_Service_Object_Type* tmp_add_class_86;
  ACE_Service_Repository* tmp_add_class_87;
  ACE_Service_Repository_Iterator* tmp_add_class_88;
  ACE_Service_Type* tmp_add_class_89;
  ACE_Service_Type_Dynamic_Guard* tmp_add_class_90;
  ACE_Service_Type_Impl* tmp_add_class_91;
  ACE_Shared_Object* tmp_add_class_92;
  ACE_Static_Object_Lock* tmp_add_class_93;
  ACE_Str_Buf* tmp_add_class_94;
  ACE_Stream_Type* tmp_add_class_95;
  ACE_String_Base_Const* tmp_add_class_96;
  ACE_Synch_Options* tmp_add_class_97;
  ACE_System_Time_Policy* tmp_add_class_98;
  ACE_Task_Base* tmp_add_class_99;
  ACE_Thread* tmp_add_class_100;
  ACE_Thread_Adapter* tmp_add_class_101;
  ACE_Thread_Control* tmp_add_class_102;
  ACE_Thread_Descriptor* tmp_add_class_103;
  ACE_Thread_Descriptor_Base* tmp_add_class_104;
  ACE_Thread_Exit* tmp_add_class_105;
  ACE_Thread_Exit_Maybe* tmp_add_class_106;
  ACE_Thread_ID* tmp_add_class_107;
  ACE_Thread_Manager* tmp_add_class_108;
  ACE_Thread_Mutex* tmp_add_class_109;
  ACE_Time_Value* tmp_add_class_110;
  ACE_Trace* tmp_add_class_111;
  ACE_Wide_To_Ascii* tmp_add_class_112;
  ACE_event_t* tmp_add_class_113;
  ACE_eventdata_t* tmp_add_class_114;
  ASN1_ENCODING_st* tmp_add_class_115;
  BIO_sock_info_u* tmp_add_class_116;
  BIT_STRING_BITNAME_st* tmp_add_class_117;
  ERR_string_data_st* tmp_add_class_118;
  Netscape_certificate_sequence* tmp_add_class_119;
  Netscape_spkac_st* tmp_add_class_120;
  Netscape_spki_st* tmp_add_class_121;
  PBE2PARAM_st* tmp_add_class_122;
  PBEPARAM_st* tmp_add_class_123;
  PBKDF2PARAM_st* tmp_add_class_124;
  PKCS7_CTX_st* tmp_add_class_125;
  SCRYPT_PARAMS_st* tmp_add_class_126;
  SHA256state_st* tmp_add_class_127;
  SHA512state_st* tmp_add_class_128;
  SHAstate_st* tmp_add_class_129;
  X509_algor_st* tmp_add_class_130;
  X509_info_st* tmp_add_class_131;
  X509_val_st* tmp_add_class_132;
  _G_fpos64_t* tmp_add_class_133;
  _G_fpos_t* tmp_add_class_134;
  _IO_cookie_io_functions_t* tmp_add_class_135;
  asn1_string_st* tmp_add_class_136;
  asn1_string_table_st* tmp_add_class_137;
  asn1_type_st* tmp_add_class_138;
  buf_mem_st* tmp_add_class_139;
  cancel_state* tmp_add_class_140;
  cmsghdr* tmp_add_class_141;
  conf_method_st* tmp_add_class_142;
  conf_st* tmp_add_class_143;
  crypto_ex_data_st* tmp_add_class_144;
  crypto_threadid_st* tmp_add_class_145;
  dl_find_object* tmp_add_class_146;
  entry* tmp_add_class_147;
  err_state_st* tmp_add_class_148;
  evp_cipher_info_st* tmp_add_class_149;
  f_owner_ex* tmp_add_class_150;
  file_handle* tmp_add_class_151;
  flock64* tmp_add_class_152;
  group_filter* tmp_add_class_153;
  group_req* tmp_add_class_154;
  group_source_req* tmp_add_class_155;
  hsearch_data* tmp_add_class_156;
  ifaddr* tmp_add_class_157;
  ifconf* tmp_add_class_158;
  ifmap* tmp_add_class_159;
  ifreq* tmp_add_class_160;
  in6_addr* tmp_add_class_161;
  in6_pktinfo* tmp_add_class_162;
  in_addr* tmp_add_class_163;
  in_pktinfo* tmp_add_class_164;
  iovec* tmp_add_class_165;
  ip6_mtuinfo* tmp_add_class_166;
  ip_mreq* tmp_add_class_167;
  ip_mreq_source* tmp_add_class_168;
  ip_mreqn* tmp_add_class_169;
  ip_msfilter* tmp_add_class_170;
  ip_opts* tmp_add_class_171;
  ipc_perm* tmp_add_class_172;
  ipv6_mreq* tmp_add_class_173;
  itimerval* tmp_add_class_174;
  lhash_st_CONF_VALUE* tmp_add_class_175;
  lhash_st_ERR_STRING_DATA* tmp_add_class_176;
  lhash_st_OPENSSL_CSTRING* tmp_add_class_177;
  lhash_st_OPENSSL_STRING* tmp_add_class_178;
  linger* tmp_add_class_179;
  mmsghdr* tmp_add_class_180;
  msghdr* tmp_add_class_181;
  nl_mmap_hdr* tmp_add_class_182;
  nl_mmap_req* tmp_add_class_183;
  nl_pktinfo* tmp_add_class_184;
  nla_bitfield32* tmp_add_class_185;
  nlattr* tmp_add_class_186;
  nlmsgerr* tmp_add_class_187;
  nlmsghdr* tmp_add_class_188;
  obj_name_st* tmp_add_class_189;
  osockaddr* tmp_add_class_190;
  ossl_algorithm_st* tmp_add_class_191;
  ossl_dispatch_st* tmp_add_class_192;
  ossl_item_st* tmp_add_class_193;
  ossl_param_st* tmp_add_class_194;
  passwd* tmp_add_class_195;
  pkcs7_digest_st* tmp_add_class_196;
  pkcs7_enc_content_st* tmp_add_class_197;
  pkcs7_encrypted_st* tmp_add_class_198;
  pkcs7_enveloped_st* tmp_add_class_199;
  pkcs7_issuer_and_serial_st* tmp_add_class_200;
  pkcs7_recip_info_st* tmp_add_class_201;
  pkcs7_signed_st* tmp_add_class_202;
  pkcs7_signedandenveloped_st* tmp_add_class_203;
  pkcs7_signer_info_st* tmp_add_class_204;
  pkcs7_st* tmp_add_class_205;
  private_key_st* tmp_add_class_206;
  qelem* tmp_add_class_207;
  rlimit64* tmp_add_class_208;
  rsa_oaep_params_st* tmp_add_class_209;
  rsa_pss_params_st* tmp_add_class_210;
  sembuf* tmp_add_class_211;
  semid_ds* tmp_add_class_212;
  seminfo* tmp_add_class_213;
  semun* tmp_add_class_214;
  sockaddr* tmp_add_class_215;
  sockaddr_in* tmp_add_class_216;
  sockaddr_in6* tmp_add_class_217;
  sockaddr_nl* tmp_add_class_218;
  sockaddr_storage* tmp_add_class_219;
  srtp_protection_profile_st* tmp_add_class_220;
  statx_timestamp* tmp_add_class_221;
  strbuf* tmp_add_class_222;
  strrecvfd* tmp_add_class_223;
  termio* tmp_add_class_224;
  tls_session_ticket_ext_st* tmp_add_class_225;
  ucontext_t* tmp_add_class_226;
  ucred* tmp_add_class_227;
  utsname* tmp_add_class_228;
  winsize* tmp_add_class_229;
  x509_trust_st* tmp_add_class_230;

The GCC parameters:
  gcc -fdump-lang-raw -fkeep-inline-functions -c -x c++ -fpermissive -w "/tmp/S3tAWPQvXO/dump1.h"

